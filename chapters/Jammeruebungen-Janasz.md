\BefehlVorKapitelDFDM{Janasz-n.png}{Janasz kocht für die Crew. Er hat vor nicht allzu langer Zeit angefangen, sich mit Jentel, der Mastkorb-Nixe, während des Essens zu unterhalten.}{Depressive Stimmung.}

Jammerübungen
=============

\Beitext{Janasz}

Ashne massierte ihm den Rücken. Sein ausgeschriebener Name war
Ashnekov, aber von Leuten, die er mochte, mochte er lieber
Ashne genannt werden. Mit einem 'e' wie in Scherben.

Janasz seufzte, als sich Ashnes Finger in seinen Rücken bohrten,
rieben, kreisten, aber so richtig kam Ashne nicht gegen
die Verspannungen an. Es waren Verspannungen aus
Frust, Angst, zu viel Arbeit, und nicht zuletzt kamen
sie davon, dass heute das Schiff besonders unruhig
durch die Wellen stampfte. Nicht, dass es gestürmt
hätte. Der Wind hatte in der Nacht gedreht, sodass
nun Dünung nicht zur Windrichtung passte. Erstere
brauchte immer ein bisschen, um sich einzupendeln. Janasz
spürte, wenn das nicht zusammenpasste. Sein
Körper reagierte darauf sensibel. Ashnes Hände
kamen da nicht gegen an. Schließlich ließ Janasz sich nach
hinten gegen ihn fallen und erhielt dafür eine Umarmung. Sie
würden nicht lange so verharren können. Es gab so viel
zu tun.

"Ich fühle mich ausgebrannt.", murmelte Janasz. "Als würde
ich die Arbeiten dreier Crewmitglieder ausführen, nicht
nur, was eine Person gut leisten kann." Er beschwerte sich selten
und fühlte sich schlecht dabei.

Ashne streichelte ihm fest über die Schultern und Arme. Er
hatte so eine unendliche Geduld mit ihm. Janasz mochte
keine zarten Berührungen und Ashne hatte sich so lange
Zeit genommen, Janasz zu fragen, bis Janasz sich
das erste Mal getraut hatte, über seine Bedürfnisse
zu reden. Jetzt übten sie es jeden Tag. Janasz sagte
jeden Tag einen Satz, in dem er sich überwand, etwas in
Worte zu kleiden, was sich für ihn ungemütlich anfühlte.

"Das Ausrauben.", zählte Ashne auf. "Das Kochen. Und
das Erfragen nach Vorlieben beim Essen."

"Das dritte zähle ich nicht, stattdessen das Ausräumen
und Buch führen.", korrigierte Janasz.

Wobei Rash beim Buchführen geholfen hatte. Janasz
konnte nicht schreiben. Nicht
so gut jedenfalls. Dafür hatte Janasz Rash beim Formulieren
von Briefen geholfen, was Rash wiederum schwerfiel. Es wirkte so
unrealistisch, dass Rash etwas schwerfiel. Manchmal
fragte sich Janasz, ob Rash das anfangs nur behauptet
hatte, um seine Gesellschaft zu haben und etwas gegen
sein Gewissen zu tun, weil Rash ihm ja bei einer Arbeit
half, die eigentlich seine Aufgabe war. Seit Kanta an
Bord war, war es anders. Kanta hatte auch Aufgaben
übernehmen sollen, die aber möglichst nicht zu viel
über ihre Vorgehensweisen verraten sollten. Wie
sie die Überfälle durchführten, war sehr wertvolles
Wissen, das nur sparsam geteilt wurde. Nun half sie
Rash stattdessen beim Formulieren, behauptete Rash
zumindest, und saß, während Janasz und Ashne ausluden und
Ladung sortierten, mit
in der kleinen Kombüse und später in den Vorratskammern, um
den Schreibanteil der Buchführung nach Diktat zu übernehmen. Ihre
Schrift war gut lesbar und schön. Sie sprach, ähnlich
wie die Kapitänin, allerlei Sprachen fließend. Janasz
hatte kein Sprachtalent. Manchmal fühlte er sich, als
hätte er überhaupt keine Talente.

Er fand Kanta durchaus nett. Mit seiner stillen
Art war er wohl gut in der Lage, ihr nichts zu verraten, was
sie nicht zwingend wissen musste. Von der Arbeitserleichterung
fühlte er jedoch wenig, im Gegenteil. Zum einen, weil eben Rash zuvor
schon geholfen hatte, und zum anderen, weil es
ein Routinebruch war.

"Strengt dich überhaupt nicht an, nach Essgewohnheiten und
-wünschen zu fragen?", fragte Ashne.

Er hatte recht. "Doch, schon." Aber es war trotzdem etwas
anderes. "Mir fällt reden schwer. Aber es lohnt sich. Es
macht Leute glücklich. Und es bringt mich zum Kennenlernen."

"Wieso zählst du dann kochen?", fragte Ashne. Er
versenkte sein Gesicht von oben in Janaszs festes, stabiles
Haar. Das musste doch pieksen oder kitzeln.

"Den Teil von Kochen, der glücklich macht, mag ich
sehr. Aber kochen ist auch heiß und eng und passiert
unter Zeitdruck.", erklärte Janasz. Er fühlte sich
wirklich nicht gut, sich ausgerechnet über Kochen zu
beschweren. "Reicht das für heute?"

Ashne gluckste. Janasz fühlte die Bewegung des
Bauchs und der Brust an seinem Rücken. "Wir haben ohnehin keine
Zeit mehr."

Ashne hatte gute Ohren. Er hatte Janasz gerade losgelassen, als
Kanta in der Kombüse für die Buchführung erschien. Sie
trug einen Rock, der so roch, als wäre er gerade noch
feucht gewesen, und darüber ein Oberhemd.

"Wir haben keine Kleider an Bord, die dir passen, schließe
ich.", sagte Ashne.

Das würde den Kleidungsstil erklären. Ashne war sehr gut darin,
mitzudenken. Janasz eher weniger.

Kanta nickte. "Rash hatte einen Rock, diesen, den Rash mir abgegeben
hat. Damit habe ich genau zwei Kleidungsstücke
mit Rock unten. Ich trage wirklich nicht gerne Hosen."

Hosen an Deck waren aber praktisch, dachte Janasz. Trotzdem
konnte er auf einer gewissen Ebene nachvollziehen, dass
selbst unpraktische Kleidung Vorzüge haben konnte, die
nicht jede Person gleich verstand.

Sie machten sich an die Arbeit. Säcke öffnen und wiegen,
Kisten und Fässer untersuchen, aufschreiben, was drin
war, und einen groben Plan darüber machen, was er daraus
kochen könnte und wie lange es reichen würde. Janasz hielt
sich nie an den so erstellten Kochplan. Das wussten alle. Es war ein Plan, der
nur dazu da war, einen Überblick abzuleiten, wie viele
Tage Janasz die Crew mit mehr oder weniger abwechslungsreichen
Gerichten versorgen könnte.

Das einzige, was Janasz an dieser Arbeit Spaß machte, waren
die Überraschungen. Manchmal gab es eine Kiste mit frischem
Gemüse. Einmal hatten sie ausversehen eine Kiste erwischt, in
der statt Lebensmittel Porzelangeschirr verstaut gewesen
war. Es war weich in Gras-artigem Material gepolstert gewesen, dass
sie für ein Lebensmittel gehalten hatten. Heute war die Ziege
dabei gewesen. Sie war vorhin aufgewacht und mähte nun
erstmal orientierungslos an Deck. Wobei sie unter Deck, bevor
er sie mit einem Blasrohrpfeil in Schlaf versetzt hatte, auch
nicht sehr orientiert gewirkt hatte. In einem Kartoffelsack
fanden sie außerdem ein großes Halstuch. Ashne fand, es passte
zu Kanta und reichte es ihr. Janasz hatte sie, seit sie
hier war, noch nie so gerührt gesehen. Und das war nun
immerhin mehrere Wochen her, oder war es schon länger als
ein Monat?

Sie strich sachte über den Stoff. "Ich darf etwas haben?", fragte sie.

"Wenn du es magst, schon, denke ich.", sagte Ashne
freundlich. "Ich habe den Eindruck, du könntest eine Freude
gebrauchen."

Kanta wirkte auf einmal sehr traurig. Vielleicht hätte Janasz
vorher auffallen sollen, dass ihr etwas fehlte.

"In der aktuellen Woche ist Marah mit Essgewohnheiten
dran.", sagte er. "Möchtest du danach dran sein?" Eigentlich
war er selber danach dran. Daher konnte er die Woche abgeben, ohne
eine Person enttäuschen zu müssen.

Ashne sah ihn sanft aber vielleicht auch eine Spur böse
an. Nur einen Moment. Er hatte Janaszs Plan wohl im
Kopf und wusste, was er vorhatte.

"Ach, ihr seid so lieb!", erwiderte Kanta.

Aber eine Antwort auf die Frage war das nicht. Er würde
wieder fragen, wenn es soweit war.

---

Janasz machte keine Pause nach der Buchführung, sondern
ging direkt zum Kochen über. Nixenküche, denn Marah
war dran. Nun, das war
vereinfacht gesagt, wie Janasz wusste, seit Jentel
endlich über saine Vorzüge geredet hatte. Bei den Essgewohnheiten
der Nixen gab es wohl genauso weitreichende
Unterschiede, wie bei denen aller anderen Völker
auch. Jentels Wünsche
machten Janasz Spaß, aber sie waren nicht einfach
zu erfüllen. Marah half ihm dabei, bei der Beschaffung
der Zutaten. Marah war ziemlich hilfsbereit aber auch
ein bisschen distanziert. Sie hatte allerdings einen guten
Draht zur Kapitänin und hätte vielleicht jeden Antrag
auf Ladungsaustausch mit der Schattenscholle
durchgekriegt. Sie setzte ohnehin etwa alle ein oder
zwei Monate einmal über.

Janasz musste an Jentel denken, als er Seetang mit in
die Nudelsuppe rührte. Das hieß bestimmt auch anders
auf Siren, irgendetwas ohne 'See-' davor.

Er würde as fragen, wenn er ihm das Essen brachte und
mit ihm im Mastkorb speiste. Er mochte mit Jentel
essen. Er konnte sich neben der zurückhaltenden Nixe
besser darauf konzentrieren, was er eigentlich aß. Und
Jentel hatte immer ein wenig interessanten Stoff zum
Nachdenken. Stoff, der ein bisschen weiter weg war vom
Alltag hier, der sich daher gut anfühlte.

Spät in der Nacht kippte Janasz dann endlich ins
Bett. Er hatte Kopfschmerzen. Er war im Mastkorb sogar
kurz eingedöst.

Er schlief mit Ashne zusammen in einer Schlafnische in
einem der beiden Schlafräume. Sie schliefen auf einer
Kiste, die gerade so breit genug für zwei war. Rash
schlief auf der zweiten Kiste im selben Raum. Sie durften nicht zu
laut knutschen, hatte Rash gesagt, was sie
ohnehin nicht taten. Sonst störte sich niemand daran,
das sie sich nachts aneinander schmiegten. Kanta
schlief mit der Kapitänin im zweiten, kleineren
Schlafraum. Kanta machte das nicht so glücklich, aber
die Kapitänin war immerhin ausschließlich zum Schlafen
dort. Auf diese Weise hatte Kanta eigentlich vergleichsweise
viel Privatsphäre.

Ashnes Arme legten sich fest um Janasz und hielten ihn
einfach. "Atme.", flüsterte er.

Janasz atmete und die Kopfschmerzen ließen eine Spur
nach. "Wie hältst du mich aus?", flüsterte er in Richtung
von Ashnes Ohr.

"Du bist das Gegenteil einer Last für mich.", versicherte
Ashne leise.

Janasz konnte sich das nicht so richtig vorstellen. Er
teilte seine Schwermütigkeit nur mit ihm. Und überhaupt
alles. Beziehungsweise noch nicht alles. Aber jeden Tag
ein bisschen mehr.
