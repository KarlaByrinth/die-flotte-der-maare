\BefehlVorKapitelDFDM{Katjenka-n.png}{Katjenka regiert das Süd-Ost-Maerdhische Zarenreich der Zwerge und wird von der Flotte der Maare als eines der kleineren Übel gesehen, wenn es darum geht, mit Landvölkern zu verhandeln.}{Tierleid, insbesondere Fischleid impliziert, Mord, Rassismus.}

Die Schattenscholle
===================

\Beitext{Katjenka}

Zarin Katjenka stand in den Tunnelgewölben des alten Tagebaus. Während
draußen Trockenheit und Hitze brütete, war es hier kühl und
sogar ein wenig feucht. So kühl, dass Katjenka bereute, den
Mantel nicht mitgenommen zu haben.

Sie war nicht allzu oft hier unten und vor allem bisher nicht
so lange am Stück. Bis gerade hatte ihr die Atmosphäre beim
Denken geholfen, aber allmählich schob sich die Kälte
nicht verdrängbar in ihre Wahrnehmung. Dabei gab es noch
so viel zum Grübeln. Es gab so viele Puzzle-Stücke,
die noch nicht so recht ein klares Gesamtbild ergaben.

Die Geschichte, die ihr dargelegt worden war, hatte schon
eine allzu geisterhafte Komponente, auch wenn sich Zarin Katjenka
eigentlich nicht davon beeindrucken lassen wollte: Das Schiff, von
dem sie inzwischen wussten, dass es Schattenscholle hieß, hatte
drei ihrer Schiffe gleichzeitig angegriffen, ohne auch nur in
ihre Nähe zu kommen. Große Teile ihrer Crew war dabei
einfach schlafend umgefallen. Bei einem der drei Schiffe hatte
es die Crew auf diese Weise vorübergehend so weit dezimiert, dass es
manövrierunfähig gewesen war und von den anderen beiden hatte eingesammelt
werden müssen. Beziehungsweise nicht müssen. Abwarten, bis alle
wieder aufwachen, wäre auch eine Option gewesen. Aber als
die Schattenscholle durch lokal um sie herum wabernden
Nebel außer Sichtweite gesegelt war, hatte die Crew auf den
zwei anderen Schiffe ohnehin nichts besseres zu tun gehabt.

Trotzdem bot dieser Teil der Geschichte nicht viel Neues. Lediglich
die Information, mit wievielen ihrer Schiffe es ein Schiff der Maare
zugleich aufnehmen konnte. Mindestens.

Zarin Katjenka hätte sich von der Sache durchaus mehr neue
Erkenntnisse erhofft. Wenn die drei Schiffe nicht nebeneinander
sondern in einem Dreieck segelten, immer so zur Schattenscholle gedreht, dass
dasselbe Schiff bezüglich der Schattenscholle hinter den anderen
beiden liegen würde, dann musste der Angriff auf das dritte
an den anderen zweien vorbeipassieren. Das hatte für Zarin
Katjenka vielversprechend gewirkt: Vielleicht
wäre der Abstand zum Zielschiff für einen Angriff durch die Schattenscholle
relevant. Dann hätte sich die Schattenscholle
näher als sonst einem Schiff nähern müssen, -- den
anderen beiden während sie das hintere angriff. Wenn
wenigstens irgendetwas Neues hätte beobachtet werden können. Aber
es schien der Crew auf diesem seltsamen Geisterschiff geradezu
Spaß zu machen, die neuen Herausforderungen zu meistern, Angst
zu machen, zu verwirren und schließlich doch das hintere der
Schiffe aus seiner Abdeckung zu locken.

Und dann, nur einen Tag später,
war die Schattenscholle verlassen vorgefunden worden. Sie hatte sich
voll besegelt in Fischernetzen verfangen, die vor dem Ufer
des Zarenreiches in fischreichen Regionen aufgespannt waren, und
hatte zum Glück keinen erkennbaren Schaden genommen. Fischerleute hatten
sich vorsichtig genähert, waren dann -- trotz der Geistergeschichten -- so
mutig gewesen, an Bord zu klettern und die Segel zu bergen, um sie in den
mizugrader Hafen zu schleppen. Es war ein leichtes, aber stabiles
Schiff. Das unterste Deck hatte unter Wasser gestanden, obwohl
das Schiff nicht leckte. Zarin Katjenka hatte veranlasst, dass
es von einigen wenigen, diskreten, fachkundigen Leuten
leergepumpt wurde, und es war nicht
wieder vollgelaufen. Es war unklar, wie diese Mengen an Wasser
an Bord gekommen waren, aber es gab Theorien.

Es hatte Leichen an Bord gegeben. Sie lagen nun in den Katakomben
des stillgelegten mizugrader Tagebaus aufgebart, wo sich die Zarin gerade
aufhielt, und wurden hier gekühlt gelagert. Katjenka
ließ sie sich von Junita zeigen. Junita hatte eine beachtliche
Bildung genossen, aber auch sie hatte wohl zuvor noch nie eine Nixe
gesehen, mutmaßte Katjenka.

"Die Leiche lag noch unberührt an Bord, in dem Deck, das unter
Wasser stand, als die Fischerleute das Schiff verließen.", berichtete
Junita. "Sie sagten, sie wollten dem Geisterschiff lieber nicht
zu tief in den Bauch steigen."

"Möchtest du mir damit sagen, dass sie nicht davon mitbekommen
haben, dass eine der Leichen die einer Nixe war?", fragte Katjenka.

"Das sieht so aus.", bestätigte Junita.

Zarin Katjenka hatte Junita mit der Untersuchung betraut, genau
wegen so etwas: Dass etwaige Geheimnisse eben nicht gleich die
Runde machen würden. Es gab ein Leck. Von irgendwo drangen
die Pläne über die Forschungsreisen nach außen, erreichten die
Flotte der Maare. Andernfalls wären die gezielten Überfälle nicht
so möglich gewesen.

Dass das süd-ost-maerdhische Zarenreich nun im Besitz der Schattenscholle
war, war vielleicht nicht zu verheimlichen. Aber was sie sonst
noch alles in Erfahrung bringen konnten, vielleicht schon. "Gut.", sagte
Katjenka. "Das soll so bleiben."

Sie besah sich die drei Leichen, die sie geborgen hatten. Sie
waren von geworfenen Messern ermordet worden. Sie wussten nicht
sicher, ob der Ork und der Zwerg zur Besatzung gehört hatten. An
sich war es nicht unwahrscheinlich, aber Katjenka hatte noch nie von
einer Zusammenarbeit zwischen Orks mit irgendwelchen anderen
Völkern gehört. Vielleicht hatte auch nur der Zwerg zur
Schattenscholle gehört, während der Ork zu einer angreifenden Partei gehört
hatte, es war zum Gefecht gekommen und es waren auf beiden Seiten Personen
gefallen. Oder umgekehrt, der Ork hätte zur Schattenscholle gehört
und der Zwerg zu den Angreifenden.

Junita überreichte Katjenka ein Messer, dessen Griff wie eine
Öse aussah. "Das steckte in der Nixe. Weitere Waffen konnten nicht
gefunden werden. Es liegt nahe, dass die anderen Opfer mit einem
ähnlichen oder demselben Messer getötet worden sind. Wir haben
aber auf dem ganzen Schiff keine weiteren solcher Messer finden können. Auch
insgesamt keine Waffen. Also,
keine, die als Waffe gedacht waren. In der Kombüse gab es
sehr wohl Messer.", berichtete Junita. "Wahrscheinlich ist das
eine noch da gewesen, weil es mit der Nixe unter Wasser lag und auf
diese Art nicht so leicht entfernt werden konnte."

Katjenka nickte und nahm das Messer entgegen. Vorsichtig. Es
wirkte sehr scharf. Ihr Blick ruhte darauf eine Weile. "Ich kenne
das Messer und die Handschrift dieser Ermordungen.", murmelte sie.

Junita blickte sie fragend an. Katjenka dachte darüber nach, ob sie
die ungestellten Fragen beantworten sollte. Junita musste
auch nicht alles wissen.

Aber auf der anderen Seite war sich Katjenka
absolut sicher, Junita vertrauen zu können. Und Stellung hin
oder her, eine Zarin brauchte auch Beratung. "Das Attentat
auf meine Eltern. Da wurden ähnliche Messer benutzt, nur
vielleicht altmodischer."

"Salvenische Assassinen.", schloss Junita richtig aus Katjenkas
Gedanken. "Wenn die Auftraggebenden
die gleichen sind wie vor nunmehr zwanzig Jahren, dann wurden die Assassinen vom Königreich
Namberg gekauft."

Es war damals nicht leicht herauszufinden gewesen. Die Ermittlungen
hatten Jahre gebraucht. Aber vielleicht war dieses benachbarte Königreich, das
nach außen hin so pazifistisch tat, und dann Angriffe aus dem Hinterhalt
fuhr, dieses Mal unvorsichtiger.

Als das Königreich Namberg als verantwortlich für das Attentat auf
Katjenkas Eltern erklärt worden war, hatten es argumentiert, dass so
ein Attentat viel weniger Tote einforderte als etwa ein Krieg, und hatte
eine komplexe Geschichte erfunden, warum der Angriff ein notwendiges Übel
gewesen wäre. Eigentlich war klar, was das Königreich Namberg gewollt
hatte: Günstige Handelsverträge in Bezug auf den Tagebau des Zarenreichs. Durch
die Unterstützung, die sie in Folge des Attentats auf das verstorbene Zarenpaar angeboten
hatten -- bevor bekannt gewesen war, dass sie selbiges auch verübt hatten --, hatten
sie sich entsprechende Verträge erschlichen. Katjenka
war damals jünger und noch recht naiv gewesen, aber immerhin nicht nur sie, sondern
auch Herrschende und Vertretende umliegender Reiche und Nationen. Es war
eine Zeit gewesen, in der viele neue und vor allem junge Personen
mehr oder weniger beabsichtigt in die Politik und Machtspiele geraten waren.

Inzwischen wussten eigentlich alle, was tatsächlich gelaufen
war, aber in der Politik hielten Regierende lieber den Mund, wenn sie andernfalls
auf Handel und Routen durch das zentrale Königreich Namberg hätten verzichten müssen. Zarin
Katjenka spürte die Übermacht und Unverschämtheit des Königreichs jeden
Tag, aber konnte nur wenig dagegen ausrichten.

Jedenfalls trug dieser Mord die Handschrift eines solchen Attentats. Es
war auch relativ ersichtlich, was das Interesse des Königreichs
sein würde, wenn es tatsächlich dahinter steckte. Vielleicht
agierten sie dieses Mal weniger verdeckt, weil
die einzige Interssengruppe, die etwas gegen einen Sieg
über die Flotte der Maare einzuwenden hätte, die Flotte selber
war. Andere Nationen würden vielleicht über die Methoden murren, aber
wären im wesentlichen froh, wenn der Flotte Einhalt geboten würde.

Wenn sich Katjenka anschaute, wer sich nun alles gegen die Flotte
der Maare verschworen hatte, hoffte sie, dass jene irgendwann einsehen
würde, dass sie Hilfe bräuchte, und sich dann an sie wenden würde. Es
war nicht leicht, Verhandlungsangebote an die Maare heranzutragen, aber
es war ihr nun mehrfach gelungen. Ohne, dass die Maare darauf eingegangen
wären, bisher. Aber damit hatte Katjenka bei den ersten Malen auch nicht
gerechnet. Sie war schon inzwischen erfahrener,
schon länger in Politik involviert. Sie wusste, dass
manche Verhandlungen, vor allem solche, die nicht so freiwillig passierten,
wiederholter Angebote bedurften, bis sie in Erwägung gezogen werden würden. Und
nun hatte sie nicht nur zuletzt die großzügige
Versicherung vorgebracht, dass sie nicht einfach nehmen wollte, was
die Flotte verteidigte -- ein recht unkonkretes Angebot, weil die Kommunikation
mit der Flotte, oder auf welchem Niveau sie sich unterhalten wollen würden, für
sie immer noch nicht so gut greifbar war --, sondern sie war nun
auch im Besitz der Schattenscholle.
