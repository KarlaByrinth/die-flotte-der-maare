### Anmerkungen zu den Content Notes

Ich versuche hier eine möglichst vollständige Liste an Content Notes
zur Verfügung zu stellen, aber weiß, dass ich nicht immer alles
auf dem Schirm habe. Hinweise sind willkommen und werden ergänzt. Über
die Content Notes hinaus darf mir gern jede Frage nach Inhalten gestellt
werden und ich spoilere in privaten Konversationen nach bestem
Wissen. Es bedarf dafür keiner Begründung oder Diskussion. Ich mache das
einfach.

Ich nehme außerdem teils sehr seltene Content Notes für Personen mit auf, die
ich kenne, weil sie sich für meine Kunst interessieren.

### Für das ganze Buch

**Zentrale Themen:**

- Ableismus
	- Besonders gegen neuroatypische Personen
	- Gaslighting
- Trauma
- Geschlechtsthemen
	- Gaslighting und Gewalt erwähnt
	- binäres Weltbild
	- GateKeeping
	- Dysphorie
	- Transfeindlichkeit
	- Sexismus
	- Misgendern, hauptsächlich thematisiert, aber auch einmal indirekt passiert
- BDSM
	- Bedrohung
	- Unterwerfung
	- Ausliefern
	- Verlieren
	- Aufgeben
	- Erniedrigen
	- Objektivizieren
	- Würgen - angedeutet
	- Tease and Denial
	- Nacktheit
	- Sex
	- Genitalien, Brüste
	- Petlay-artiges - eigentlich nicht Petplay, aber schon Rollenspiel als Tier
	- Gegessen werden
	- Angst
	- Erektion
	- Vorübergehend vulgäre Sprache

\Leerzeile
\Leerzeile
**Mittleres bis häufiges Thema:**

- Eine Entsprechung zu Misantropie (In der Geschichte sind Menschen nicht so häufig.)
- Rassismus
- Speziesismus
- Tierleid erwähnt, unter anderem Fischleid
- vegetarische Ernährung
- Morddrohungen
- Mord an mehreren Personen
- Messer
- Lebensmüde Gedanken
- Nervengift, Betäubung, Nadeln
- Alkohol(verbot)
- Trauer
- Verlieben-/Romantikdinge
- Kuscheln
- Traumaflashback
- psychische und physische Folter
- Verletzungen
- Füße

\Leerzeile
\Leerzeile
**Ein- oder zweimaliges Aufkommen:**

- Hunger
- Shaming von Essgewohnheiten
- Amputierte Gliedmaßen
- Leichenschändung
- missbräuchliche Eltern-Kind-Beziehung
- Kinderleid, auch anderes
- Slutshaming
- toxische Beziehungen
- Sex mit Minderjährigen - impliziert
- Pädophilie (keine Gewalt)
- Unerfüllter Kinderwunsch
- Schwangerschaft erwähnt
- Tod eines Elters
- Fäkalsprache
- Atemnot
- Thema gegessen werden, auch außerhalb von BDSM-Kontext
- Fäkalien
- Blut
- Blutbad
- Gemetzel
- etwas Gore
- Schock
- Narben
- Erpressung
- Angst vor Folter
- Frust
- körperliche Übergriffigkeit
- Gefangennahme
- Panik
- Fesseln
- Völkermord - thematisiert
- Selbstverletzendes Verhalten
- Gedanken zu Suizid
- Hinrichtung als Thema
- Pandemie
- Schmerz
- Interfeindlichkeit
- Genital-OP - erwähnt
- Romantik zwischen Geschwistern - erwähnt
- Operation im Zusammenhang mit Rassismus
- Albträume
- Shutdown?
- zumindest etwas offenes Ende
- wirbellose Tiere (erwähnt)
- Quallen (erwähnt)

### 01 DieSchattenmuraene 
- Amputierte Gliedmaßen
- Eine Entsprechung zu Misantropie

### 02 Eine Mär und Geisterschiffe

- Lebensmüde Gedanken
- Leichenschändung
- Erwähnung von Sex
- missbräuchliche Eltern-Kind-Beziehung
- Gaslighting und Gewalt erwähnt
- Slutshaming
- Ableismus
- Sex mit Minderjährigen - impliziert
- binäres Weltbild
- GateKeeping im Zusammenhang mit Geschlecht
- Erwähnung von Genitalien
- wirbellose Tiere (erwähnt)
- Quallen (erwähnt)

### 03 Seereis

- Dysphorie

### 04 Hafenmeisterin

- Unerfüllter Kinderwunsch
- Schwangerschaft erwähnt
- Pädophilie (keine Gewalt)
- Tod eines Elters

### 05 Die Flotte der Maare

- Amputation
- Nervengift, Betäubung
- Tierleid erwähnt
- vegetarische Ernährung
- Fäkalien

### 06 Mist

- Thema gegessen werden
- Fäkalsprache

### 07 Jammerübungen

- Depressive Stimmung

### 08 Tauchen

- BDSM
- Ausliefern
- Atemnot
- Aufgeben
- Verlieren
- Erektion

### 09 Unheimlichkeiten

- Grusel
- Selbstkritik

### 10 Überwindungen

- BDSM
- Vorübergehend vulgäre Sprache
- Erniedrigen
- Objektivizieren
- Ausliefern
- Würgen - angedeutet
- Tease and Denial
- Nacktheit
- Sex
- Genitalien

### 11 Anreden

- Morddrohungen
- Messer
- Alkohol(verbot)
- indirektes Misgendern in der Vergangenheit

### 12 Beobachten

*keine bisher*

### 13 Sortieren

- Blutbad
- Gemetzel
- Ableismus
- Trauma
- Erwähnung von Mord

### 14 Befehle

- BDSM mindestens angedeutet

### 15 Strichrichtung

- Lebensmüde Gedanken

### 16 Mäh

- Genitalien nur erwähnt
- Petlay-artiges - eigentlich nicht Petplay, aber schon Rollenspiel als Tier
- Bedrohung
- Unterwerfung
- Gegessen werden
- Angst
- Objektifizierung


### 17 Kuscheln

- BDSM
- Es geht um kuscheln
- Teil-Nacktheit
- Messer
- Bedrohung
- Heulkrampf
- Trauma
- Misgendern
- Sex ist Kern einer Unterhaltung, aber wird nicht ausgeführt

### 18 Stille

- Tierleid
- Shaming von Essgewohnheiten
- Mord an mehreren Personen
- Trauer
- Hunger

### 19 Psychen

- Rassismus

### 20 Berühren

- BDSM, aber vielleicht auch nicht?
- Bisschen D/s-Spiel
- Psychisches Hingeben
- Sex am Rande erwähnt
- Küssen
- erwähnt: Messer, Bedrohung

### 21 Die Schattenscholle

- Tierleid, insbesondere Fischleid impliziert
- Mord
- Rassismus

### 22 Unglück

- Mord
- Blut
- etwas Gore
- Schock
- Trauer
- Kinderleid
- Transfeindlichkeit
- Sexismus
- Gatekeeping

### 23 Scherben

- Misgendern
- Lebensmüdigkeit
- Sex erwähnt
- Verliebtheitskuscheldinge

### 24 Leben

- spärlich bekleidetes Kuscheln
- Brüste
- Sex - erwähnt
- Transition?
- Ausliefern
- BDSM
- Narben
- Traumaflashback
- Erpressung
- Mord - erwähnt
- Folter - erwähnt
- Trauer

### 25 Wesen

- Mord
- morbide Gedanken
- gegessen werden
- Frust
- körperliche Übergriffigkeit
- Trauma/Trauma-Trigger - Metapher zumindest

### 26 Wenden

- Gefangennahme
- Speziesismus
- Panik
- Ertrinken - erwähnt

### 27 Abstand

- Betäubung
- Ableismus
- Fischleid impliziert

### 28 Verhalten

*Dieses Kapitel ist für mich eine Art Herzstück, es ist aber auch
sehr schlimm. Es reproduziert eine Menge Ableismus, aber es
ist in gewisser Weise für mich katharsisch? Sowas. Und vielleicht
ist dieser Ableismus für Leute, die es nicht kennen,
schwer als eine psychische Folter wahrnehmbar.*

- Fesseln
- Gefangennahme
- psychische und physische Folter
- Ableismus besonders gegen neuroatypische Personen
- Völkermord - thematisiert
- Rassismus
- Selbstverletzendes Verhalten
- Blut
- Verletzung
- BDSM-Anspielung
- Gedanken zu Suizid

### 29 Töten

- Töten - erwähnt
- Betäubung
- Hinrichtung als Thema

### 30 Aussichten

- Rassismus
- Tod
- Trauma
- Betäubung
- Nadeln
- Messer

### 31 Knoten

- Schmerz

### 32 Einsichten

- Alkohol
- Belästigung
- Tod
- Pandemie
- Rassismus oder Speziesismus

### 33 Sinken

*Besonders dieses Kapitel sollte wohl noch ein Sensitivity
Reading bezüglich Rassismus und Interfeindlichkeit
bekommen. Passt auf euch anf.*

- Narben
- Interfeindlichkeit
- Genital-OP - erwähnt
- Operation im Zusammenhang mit Rassismus
- Romantik zwischen Geschwistern - erwähnt
- toxische Beziehungen
- Misgendern
- Luftnot

### 34 Trauma und Träume

- Trauma
- Shutdown?
- Albträume
- Gewalt
- Selbstverletzendes Verhalten
- Gaslighting

### 35 Schnee

- Zumindest etwas offenes Ende
