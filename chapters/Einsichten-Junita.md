\BefehlVorKapitelDFDM{Junita-n.png}{Junita berät Zarin Katjenka. Sie ist sehr gebildet. Zarin Katjenka vertraut ihr.}{Alkohol, Belästigung, Tod, Pandemie, Rassismus oder Speziesismus.}

Einsichten
==========

\Beitext{Junita}

Junita riss sich zusammen. Sie war mit der Vorgabe auf ihre Gemächer geschickt worden, sie
möge sich in Bereitschaft halten. Sie hatte andere Pläne und setzte
sie nicht um, einfach um da zu sein, sobald es Zeit war. Jede halbe Stunde, die
verstrich, ohne dass nach ihr geschickt wurde, fühlte sich wie verschwendet
an. Als hätte sie es doch schaffen können, ein paar Momente woanders zu sein, oder
auch etwas länger, ohne dass es aufgefallen wäre.

Aber nach zwei Stunden war es dann endlich so weit. Eine der bediensteten
Personen klopfte und informierte sie, dass die Zarin nach ihr schickte. Sie
erwarte sie in ihren Privatgemächern. Junita war schon oft dort gewesen, aber
heute war sie sehr nervös. Sie warf noch einen Blick in den Spiegel, ob
die Zöpfe in ihrem Bart gepflegt genug aussahen, oder ob in dem Durcheinander
sich doch eine Strähne verselbstständigt hätte, und brach auf.

Sie stand kerzengerade da, als die Zarin ihr öffnete. Sie sah auf
den ersten Blick, das Katjenka aufgewühlt war.

Sie kannte das Protokoll und befolgte es immer haargenau. Jede Bewegung war
berechnet. Auf diese Weise wirkte sie zuverlässig und präzise. Sie
war es auch. Eigenschaften, die die Zarin schätzte.

Sie folgte dem einladenden Wink der Zarin an den kleinen Tisch mit Blick durch das
Fenster zum Hof. Es war spät in der Nacht. Eine Nacht, in der Nixen
im Hafen ein historisches Ereignis vorbereiteten. Junitas Gesichtsausdruck
war glatt, wie immer.

Glatt, als hätte sie keine Emotionen. Die Zarin hatte auch ihr mit der
psychischen Folter an Sindra weh getan. Nun, ihr Problem war ein anderes als Sindras. Junita
verbarg Emotionen tatsächlich hinter einer Fassade. Junita schauspielte. Und
trotzdem hatte es sie tief verletzt, weil sie Katjenka gegenüber nie
Emotionen über ein kleines Lächeln hinausgehend zeigte, auf
diese Weise zu erfahren, wie die Zarin über sie
dachte, oder sie behandeln würde, wenn sie sie unter anderen Umständen
kennen gelernt hätte. Nicht in einem Universitätskontext, in dem Junita
der Zarin als Kind aufgefallen war. Und wo diese sich Junita ausgesucht
hatte, als ihre persönliche Beratung ausgebildet zu werden.

Die Zarin musterte sie mit einem Schmunzeln. Sie schwieg ungewöhnlich
lange. Meistens brachte sie zeitig an, worüber sie reden wollte. Ahnte
sie etwas?

"Was kann ich für dich tun?", fragte Junita.

"Es ist alles sehr merkwürdig.", antwortete Katjenka. "Möchtest du
etwas trinken?"

Das hatte sie noch nie angeboten. Junita kannte das Protokoll dazu
nicht. Höflich ablehnen? Damit fuhr sie vielleicht gut. Oder? "Wie
dir beliebt.", sagte sie.

"Junita." Kantjenka lehnte sich zurück. "Mir ist aufgefallen, dass
ich dich überhaupt nicht kenne. Du bist immer da. Immer höflich,
immer abrufbereit. Du erklärst viel, ausführlich und geduldig. Dein
Rat ist stets durchdacht und hilfreich. Ich schätze deine Unterstützung
sehr."

Junita konnte nicht verhindern, dass ein warmes Gefühl sie durchströmte. Die
Zarin war nicht unbedingt sparsam mit Komplimenten. Ihr gegenüber
aber schon. Weil sie sie nicht als Motivationsantrieb brauchte, Komplimente
also keinen direkten Zweck erfüllten. Und weil sie immer gleich agierte, es
also keine besseren Tage gab, die die Zarin über schlechtere hinausstellen
konnte. Junita fühlte auch Unbehagen. Es war ein Moment des Wandels und
Junita wusste noch nicht wohin. Es machte ihr Angst, dass die Zarin dies
nun aussprach und nicht zu einem anderen, weniger ereignisreichen
Zeitpunkt. Aber es berührte sie auch
positiv und im Innersten. Sie nickte einmal, die Komplimente
zur Kenntnis nehmend. "Es freut mich, dass ich dir eine Hilfe sein
kann.", sagte sie. "Ich hoffe, ich kann die mir erteilten Aufgaben auch
weiterhin zu deiner Zufriedenheit erfüllen."

Die Zarin lächelte einen Moment noch etwas breiter. "Ich möchte dich
aber gern als Person besser kennen lernen. Du hast dich nie über die
Mode beklagt, die dir zugeteilt wurde, nie über Essen oder deine
Gemächer. Aber du hast auch nie Wünsche geäußert. Was sind deine
Bedürfnisse?"

"Vor allem deine Zufriedenheit.", antwortete Junita hilflos. Es war
nicht, was Katjenka hören wollte, das wusste sie. Auf dieses Gespräch
war sie nicht vorbereitet. Der Gedanke brachte sie auf eine Lösung
des Problems: "Gerade beschäftigen mich die jüngsten Ereignisse, muss
ich zugeben.", sagte sie. "Ich habe mir über meine Bedürfnisse keine
Gedanken gemacht. Das gehörte bisher nicht zu meinem Aufgabenfeld. Wenn
dir das wichtig ist, befasse ich mich damit in den kommenden Tagen."

Katjenka nickte. "Magst du ein Glas Sandbeerenwein mit mir trinken?", fragte
sie. "Wenn du keine Bedürfnisse hast, würde ich es mir wünschen."

Junita wurde innerlich heiß. Sie wusste, dass nichts ihres Unbehagens
nach außen durchdrang. Sie atmete flach aber bewusst ein und aus, um doch, nun
in diesem denkbar ungünstigen Augenblick, das erste Mal ein Bedürfnis
zu äußern. "Ich möchte aktuell lieber keinen Alkohol trinken." Tatsächlich
wollte sie es nie. Aber mit der Akutsituation ließ sich das vielleicht
unauffälliger vorübergehend begründen. Und später wäre es nicht
mehr so schlimm, wenn dabei etwas auffällig wäre, wenn sie den Wunsch
noch einmal allgemein äußerte.

"Wie die Maare.", antwortete die Zarin.

Junita lächelte schmal. Ein passender Zeitpunkt dafür. "Eine womöglich
nicht zufällige Übereinstimmung." Junita freute sich innerlich
über die Überraschung in Katjenkas Blick.

"Nicht zufällig?", fragte diese.

"Es gibt laut der Geschichten, die in Umlauf sind, zwei mögliche
Gründe, warum die Maare sich ein Alkoholverbot auferlegt haben. Wenn
jenes überhaupt der Realität entspricht, und nicht auch eine
Mär ist, um sie zu entfremden.", erklärte Junita. "Es ist bekannt, dass
Alkoholkonsum klares Denken verändert oder erschwert. Die Maare sind aber
wenige und müssen ihre Möglichkeiten so gut es geht ausschöpfen." Junita
hielt einen Finger hoch und fügte nun einen zweiten hinzu. "Die
zweite Erklärung ist, dass die Kapitänin üble Erfahrungen im Zusammenhang
mit Alkohol machen musste und deshalb jegliche alkoholische Substanz
von der Flotte bannt." Junita senkte die Hand wieder und machte eine
kurze Redepause, um der Zarin Zeit zu geben, selbst nachzudenken. "Beides
sind Gründe, die sich einigermaßen mit meinen decken, nun
keinen Alkohol konsumieren zu mögen. Wir
sind in einer Lage, in der ich meine Denkfähigkeit möglichst nicht
beeinträchtigen möchte. Und ich wurde in meiner Vergangenheit mehrfach
auf Feiern bei Hof von betrunkenen Personen belästigt. Diese Erinnerungen
verknüpfe ich mit Alkohol, sodass Alkohol und besonders Wein bei
mir ein unangenehmes Gefühl auslöst."

"Das tut mir leid, das wusste ich nicht.", sagte die Zarin. Sie wirkte
ernsthaft betroffen.

"Ich hielt es nie für notwendig, darüber zu reden.", sagte Junita. "Aber
dein Wunsch war, mich näher kennen zu lernen." Sie war eine Spur
stolz auf sich selbst, das Gespräch wieder in sichereres Fahrwasser
gelenkt zu haben. Sie war allerdings halbwegs überzeugt, dass die
Zarin sie mindestens testen wollte, ob sie vielleicht etwas mit
der Flotte der Maare zu tun haben könnte.

Und das hatte sie.

Mehr noch. Sie war eine Nixe. Sie war Kind einer Nixe. Und Nixen sahen
das nicht so eng mit den Fischschwänzen, hatte sie erfahren dürfen. Ein
Kind einer Nixe war eine Nixe, wenn es wollte, Fischschwanz oder
nicht.

In Junitas Fall war die gebärende Person Zwerg gewesen. In Jentels
Fall war die gebärende Person Nixe gewesen. Daher hatte sie Beine und
Jentel einen Fischschwanz. Sie hatten die ersten
Jahre ihres Lebens viel beieinander verbracht, weil ihre Eltern
eine etwas unkonventionelle, größere Familie bildeten, bis Junitas sie geboren
habendes Elter -- dafür hatte Siren ein eigenes Wort, weil die Sprache
in Geschlechtszusammenhängen offener oder präziser war -- in
Mizugrad hatte studieren wollen. Es hatte sich am Aufbau des Informationssystems
der Nixen beteiligen wollen, noch lange bevor es die Maare überhaupt
gegeben hatte, und es hatte sein Kind mitgenommen. Es hatte nicht
lange überlebt. Als Junita gerade zwölf gewesen war, hatte eine
Welle einer Seuche viele Leben dahingerafft, besonders in den
wärmeren Regionen Maerdhas und ihr Elter war dem Fieber erlegen.

Junita war in seine Rolle geschlüpft. Dass die Zarin sie aussuchen
würde, war ihre Hoffnung gewesen, aber es war nicht geplant, kein
ausgelegter Köder, einfach Glück gewesen. Und dann war sie in dieses komplizierte
Leben hineingewachsen. Ein Leben, in dem sie über die Flotte der Maare
alles wusste, einmal von ihnen persönlich und einmal, weil zu ihren
Aufgaben gehörte, die Gerüchteküche zu verfolgen. Sie speicherte
die beiden Stränge derselben Geschichten getrennt von einander
ab. Es war etwas, was ihr auch nicht wenig Spaß bereitete.

"Wir haben die Assassinperson einzeln eingesperrt.", teilte
Katjenka mit.

Junita nickte. Erleichterung durchtrömte sie. Amira war noch am
Leben. Sie hätte Katjenka in diesem Zusammenhang zugetraut, von
ihren sehr überlegten Entscheidungen bezüglich Hinrichtung
abzuweichen.

"Weißt du, ich mag manchmal impulsiv und oft nicht aufmerksam
genug sein.", fuhr die Zarin fort. "Aber ich weiß sehr wohl, dass
ich heute gestorben wäre, wenn sie es gewollt hätte."

"Das ist richtig.", stimmte Junita sachlich zu.

"Ich habe die Nixe grausam behandelt.", räumte Katjenka ein.

Auch das entsprach der Wahrheit. Trotzdem zögerte Junita
hier einen gespielten Augenblick lang, bis sie zustimmte. Die
Zarin schätzte sie für ihre Ehrlichkeit, aber auch dafür, dass
sie sie nicht gern kritisierte.

"Die Kapitänin hat die Nixe gerettet.", fuhr Katjenka fort. "Es
war für sie aussichtslos, selbst zu entkommen, aber sie hat
uns alle ausgetrickst, um die Nixe zu retten. Sie hat sogar
einen Namen."

"Marah." Der Name war mehrfach gefallen. Junita wünschte sich, die
Zarin würde ihn verwenden.

"Marah.", wiederholte die Zarin. "Sindra hatte jedenfalls recht. Ich
kann aus all ihren Handlungen und Verhaltensweisen in größter
Not so leicht und klar ableiten, was ihre Prioritäten und Motive
sind, worum es ihnen geht, dass es mir fast unheimlich ist. Sie
haben niemanden langfristig verletzt. Darin sind sie gründlicher
als mein Hofstaat."

"Es hat noch nie ein Ereignis gegeben, das ihre Werte anders
auslegen würde.", bekräftigte Junita.

Die Zarin nickte. "Ich möchte sie nicht hinrichten lassen.", sagte
sie.

Dieses Mal fragte sich Junita, ob sie es wirklich schaffte, den Stein
zu verbergen, der ihr vom Herzen fiel. "Hast du bereits über alternative
Pläne nachgedacht?", fragte sie.

Katjenka lächelte. "Gib es ruhig zu, dir liegt daran, dass ich sie
ziehen lasse."

Auch Junita hob die Mundwinkel ein klein wenig. "Ich halte es zumindest
nicht für verkehrt, auf Sindras letzten Vorschlag einzugehen.", sagte
sie. Es fiel ihr dieses Mal schwer, nicht einfach zu sagen, was
Katjenka hören wollte. Aber es war verhältnismäßig offenliegend, in
welche Richtung ein Vorschlag von ihrer Seite als gute Beratung
gehen musste. "Ich halte es für eine gute Option, dich mit
der ehemaligen und vielleicht auch zukünftigen Kapitänin
der Maare zu treffen, etwas mehr auf Augenhöhe als zuletzt, und
sie zu fragen, was für eine Zusammenarbeit sie sich vorstellen
könnte. Für eine Verhandlungsposition, die sich wie eine solche
und nicht wie Erpressung anfühlt, sollte sie natürlich zügig nicht
mehr in Gefangenschaft sein, oder zumindest sich nicht
so fühlen. Vielleicht ist -- natürlich gut
abgesichert -- die Schattenscholle selbst ein Verhandlungsort, der
das richtige Zeichen für deine Bereitschaft zur Zusammenarbeit setzt." Junita
hatte eigentlich ein ganz gutes Gespür dafür, womit sie zu weit
ginge, aber hatte trotzdem Angst, die Grenze hier doch zu strapazieren, weshalb
sie das Gesicht der Zarin genau beobachtete.

"Jegliche Zusammenarbeit mit Sindra und ihrer Flotte würde einschließen, dass ich
mich vertraglich daran binde, die Erforschung Grenlannds aufzugeben.", erinnerte
die Zarin.

Junita nickte. "Du könntest versuchen, einen zeitlich begrenzten Vertrag
auszuhandeln.", schlug sie vor. "Es ist vielleicht nicht schlimm, wenn wir
in den nächsten fünf bis acht Jahren Grenlannd nicht erforschen. Die
Erforschung ist natürlich besonders gewinnbringend, sollten wir die
ersten sein. Aber wenn wir die Flotte der Maare solange unterstützen, -- nicht
so offenliegend für andere Völker natürlich --, wie
diese realistisch anderer Nationen Forschungsunterfangen aufhalten
kann, haben wir tatsächlich größere Chancen, die ersten zu sein."

Katjenka lehnte sich zufrieden im Sessel zurück. Es wirkte
gemütlich. Es war einer der Momente, in denen Junita sich wünschte, ihr
näher zu stehen, nicht nur Beratung zu sein. Sie war immer verwirrt von
sich, wenn sie diesen Gedanken hatte. Sie mochte die Zarin. Trotz
allem. Sie wusste nicht genau, was sie sich für eine Nähe vorstellte. Vielleicht
war es sogar eine elterliche, weil sie so kurz nur Eltern in ihrer
Nähe gehabt hatte. Aber so ganz richtig benannt fühlte sich ihr
verschwiegener Wunsch damit nicht an.

"Ich mag die Richtung deiner Gedanken.", sagte die Zarin. "Ich glaube, ich
verstehe schon, worauf du hinaus willst. Aber magst du mir trotzdem ein bisschen ausführen, warum
du glaubst, dass wir dann größere Chancen hätten, die ersten zu sein?"

Junita nickte. "Während die anderen Nationen sich weiter darauf konzentrieren
müssen, gegen die Flotte der Maare anzukommen, können wir uns auf die Überfahrt
an sich konzentrieren und versuchen, einen neuen Schiffstyp zu entwickeln.", erklärte
sie. "Wir haben die Schattenscholle noch. Natürlich müssen wir sie wahrscheinlich abgeben, wenn
wir einen Vertrag mit der Flotte der Maare schließen. Aber wir können ihr
bei uns einen sicheren Hafen bieten und vielleicht selbst Crewmitglieder
zur Verfügung stellen, und auf diese Weise ihre Schiffsbautechnik ausforschen." Junita
versuchte ihre Gedanken zu entheddern. "Wenn die anderen Nationen irgenwann
einen Weg finden sollten, an der Flotte vorbeizukommen und es Zeit für
uns selbst wird, aufzubrechen, haben wir schnellere Schiffe, und immer noch
das Geheimnis der Überfälle. Mit einem Vertrag mit uns müssen sie ihre
Strategie nicht ändern. Das wird uns in dem Augenblick zu Gute kommen."

Katjenka nickte. "Nicht die Art Hinterhalt, die mir sehr behagt, aber
du hast recht.", sagte sie. "Zumal auch das Wissen als Druckmittel allein schon viel
wert sein kann."

"Mir behagt es auch nicht.", gab Junita rasch zu. "Wir möchten natürlich
die Flotte der Maare, wenn sie den Vertrag mit uns einhalten, nicht
hintergehen. Meine größere Hoffnung, zu der ich nun kommen wollte, wäre, dass
wir von einander lernen und Vertrauen aufbauen können. Ich glaube, die
Flotte der Maare ist Forschung gegenüber gar nicht grundsätzlich abgeneigt. Sondern nur
unseren Forschungspraktiken. Vielleicht ist am Ende des zeitlichen Vertrags
ein Hinterhalt gar nicht notwendig."

"Die Flotte der Maare ist, wie du mir weitergegeben hast, einigen
Gerüchten zufolge sogar in Grenlannd gebaut worden.", fiel der Zarin ein.

"In diesem Punkt sind sich die Geschichten nicht einig. Es kann sein, aber
es gibt große Inkonsistenzen in den Versionen der Gerüchte.", sagte
Junita sachlich.

Katjenka nickte wieder. In ihr Gesicht trat ein neuer Ausdruck. Vielleicht
ein ahnender. Was immer sie ahnte. Er war auch warm.

Junita fragte sich einen Moment, ob die Zarin ihr überhaupt übelnehmen würde, wenn
sie von Junitas Doppelleben erfahren würde. Aber es wäre viel zu
gefährlich, etwas in dieser Richtung zu riskieren.

"Vielen Dank für dieses aufschlussreiche Nachtgespräch.", sagte die
Zarin. "Ich habe eine andere sehr seltsame Bitte."

Junita nickte. "Ich bin für dich da, so gut ich kann.", versicherte
sie.

"Würdest du heute Nacht in meinen Gemächern schlafen?", fragte
die Zarin.

Junita stockte einen Augenblick der Atem. Es zog sich in ihr zusammen. Nicht
nur vor Angst, sondern auch vor Zuneigung.

Eigentlich musste die
Zarin ihr nicht begründen, warum sie sie bat. Junita konnte
es sich denken. Katjenka tat es trotzdem: "So
sehr ich der Assassinperson auch trauen möchte, dass sie mir nichts
antun wird, ich fürchte mich mit ihr unter einem Dach. Mich
haben außerdem all die Erinnerungen an damals überflutet. Mir wäre
es lieb, heute Nacht eine Person in meiner Nähe zu wissen, der ich
ganz und gar vertraue."

Junita nickte. "Sehr wohl.", sagte sie. "Fühlst du dich sicher genug, wenn
ich mir meine Nachtbekleidung und Zudecken hole? Ich habe Verständnis, wenn
dem nicht so ist. Dann schlafe ich gern auf dem Sofa, oder wo du mich
am liebsten hättest." Junita fühlte sich ängstlich, als sie dies aussprach. Sie
wollte nicht bei der Zarin im Bett schlafen. Und ein kleiner, unaufgeforderter
Teil fügte in Gedanken ein 'noch nicht' hinzu.

"Du kannst dir holen, was du brauchst.", sagte die Zarin. "Du darfst
hinter dieser Türe schlafen, wo du willst." Sie deutete auf die Tür, die
den Zugang zu ihren Gemächern bildete. "Ich erwarte dich in spätestens
einer Stunde."

Das war genug Zeit, einen Briefwels mit einem Brief zu versorgen.
