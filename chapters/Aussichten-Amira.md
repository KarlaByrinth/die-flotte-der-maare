\BefehlVorKapitelDFDM{Amira-n.png}{Amira war einst ein Assassinan mit dem Auftrag, die Kapitänin zu töten.}{Rassismus, Tod, Trauma, Betäubung, Nadeln, Messer.}

Aussichten
==========

\Beitext{Amira}

Die Lage war aussichtslos. Amira hatte es zu spät bemerkt. Sie
hätte andernfalls zum Rückzug aufgefordert, in der Hoffnung, dass
die anderen auf sie hören würden. Kantas Pläne hatten sich
brauchbar realistisch angehört. Aber nun standen sie in der
Eingangshalle der Kramelin und Amiras Blick streifte über die
Menge der Wachen. Viel mehr, als die Zarin je um sich
versammelt hatte, wenn den Aussagen Glauben geschenkt
werden konnte, die Ushenka im Vorfeld eingeholt hatte. Dazu
sagten die Haltungen der Wachen aus, dass sie tatsächlich
nicht nur zum Einschüchtern da waren, sondern wussten, wie
gezielt Gewalt angewandt werden konnte. Amira konnte Personen
das ansehen. Sie hätte Janasz von der Wache, die Janasz vertrat,
unterscheiden können, ohne Janasz zu kennen. Und das konnten
auch einige der Wachen in der Halle, in die Janasz sie
nun führte. Sie sah die Skepsis in ihren Blicken.

Amira gehörte nicht zu der Gruppe. Sie bewegte sich um Säulen, von
denen es einige gab, synchron zu den Bewegungen der Wachen und
geschützt vor den Blicken selbiger, weil sie ihren Ort im
Schatten der Gruppe wechselte, so selbstverständlich, als gehörte
sie hier her.

Sie hörten Sindras Stimme. Amira wusste nicht, wie viele Mitglieder der
Crew der Maare wie sie wahrnehmen konnten, wie sehr die Kapitänin
litt. Amira fühlte nichts. Sie war noch nie einen so langen
Zeitraum am Stück nicht in diesem Geisteszustand gewesen, aber
nun war er wieder da und saß, wie eine zweite Haut. Dem Geisteszustand, in dem
ein Teil von ihr abgekoppelt war. Der, der fühlte. Ihr Kopf
war klar, wusste, wo die Ausgänge waren, war wachsam für alles, und
wusste, wie aussichtslos die Lage war. Es sei denn, sie tötete.

Amira war als erstes im anderen Teil der Halle, wo Marah hing. Die
Gruppe, wie sie es abgesprochen hatten, würde sich nun etwas
ausbreiten, als wären sie einfach neugierig und unsicher. Auf diese
Weise wären sie eine größere Herausforderung für die Wachen. Aber
es waren einfach zu viele. Angesichts der Tatsache, dass die einzige
Person, die sie dabei hatten, die mit den Blasrohren gut umgehen
konnte, Janasz war. Yanil von der Schollencrew, der
es auch konnte, war Ork. Es gab keine Möglichkeit,
einen Ork unauffällig in die Kramelin zu bringen. Das hätte
für Aufregung gesorgt. Orks waren, ähnlich wie Riesen, ein
gejagtes Volk. Also wartete Yanil bei den Nixen.

Amira brauchte sich nicht umzublicken, um zu wissen, dass Rash
die Person war, die sich ihr von hinten -- wie sich zufällig verirrend --
näherte.

"Aussichtslos.", flüsterte sie.

"Flucht?", fragte Rashs Stimme direkt in ihr Ohr.

Sie schüttelte den Kopf.

"Dann leite ich Angriff ein.", murmelte Rash. "Ich halte die
Folter kaum aus."

Amira nickte. "Ich auch nicht." Plötzlich hatte sie ein Messer in
der Hand. Und wenn sie alle sterben würden, Marah würde es nicht
an einem Seil hängend tun.

Rash Lippen streiften ihren Hals, ohne zu küssen. Das Gefühl, das
es in Amira auslöste, war nicht kontrollierbar und so stark,
dass ihr ganzer Körper erzitterte. Sie
mochte es. Sie wollte es. Aber nicht hier, nicht jetzt. "Rot."

"Es tut mir leid.", flüsterte Rash. Und weg war Rash.

Amira blickte sich um. Atmete. Nun wünschte sie sich, nicht
'rot' gesagt zu haben. Es kam ihr abweisend vor. Rash hatte
vielleicht ein schlechtes Gewissen. Das war kein gutes Gefühl, um in den Tod zu
gehen, falls das passieren würde. Amira schloss einen Moment
die Augen, atmete, und sperrte all die Gefühle wieder aus. Sie
liebte Rash gerade nicht. Und das war gut. Das Messer lag
sicher in ihrer Hand. Sie blickte sich um und der Alternativbaum
aller Möglichkeiten erstreckte sich in ihrem Verstand. Was
tun, wenn dieses passierte, und was, wenn jenes passierte. Und
von dort aus Verzweigungen und Verästelungen an Plänen. Wie
ein riesiges Slik-Spiel. Nur mit Personen statt mit Figuren.

Rashs Stimme erscholl durch den Raum: "Die Welt ist voller
Monsterherrschenden! Aber Sindra ist keine davon!"

Amira hatte Rash noch nie so voller Wut schreien gehört. Rash
hätte vielleicht auch Amira damit eingeschüchtert, hätte sie ihre Gefühle
nicht weggesperrt. Die Zarin hingegen wirkte ängstlich.

Amira nahm den ersten Zweig ihres Plans. "Bist du in der Lage,
Marah aufzufangen?", fragte sie Sindra in kontrollierter Lautstärke, in
ihrer Sprache, Solvenit. Eine Sprache, die voraussichtlich nur
Sindra außer ihr verstehen würde. Zu ihrer Überraschung
sah die Zarin neben Sindra allerdings doch verstehend aus. Na gut, dann verstand
sie also auch Solvenit. In diesem Moment war es gut, denn
es lenkte sie von Sindra so sehr ab, dass sie Sindra nicht nicken
sah. Amira schleuderte das Messer in dem Moment, als Sindra unter Marah
trat. Sie nahm sich nicht die Zeit, zu beobachten, wie Marah
fiel. Sie hielt bereits ein neues Messer in der Hand, als sie in
dem Moment aus dem Sichtfeld der Zarin huschte, als diese
sich wieder der Ex-Kapitänin zuwandte.

Amira konnte all die Emotionen der Zarin lesen. Die Verwirrung, als
sie wieder zurückblickte, wohin Amira verschwunden sein könnte. Und den Horror, den
sie zuvor gefühlt hatte, als sie Amira entdeckt und verstanden hatte. Amira machte
ihr Angst, und zwar viel mehr Angst, als für Leute üblich, die ein
Assassinan erblickten und sich darüber bewusst wurden, dass
Amira tödlich sein konnte.

Auch das war ein Vorteil. Die Zarin hatte Tendenzen dazu, impulsiv
zu sein. Sie würde vielleicht schlechtere Entscheidungen fällen, weil
sie mit Amiras Anblick ein Trauma verknüpfte. Amira brauchte
nicht einmal nachzudenken, um zu wissen, dass dann der Mord
an der Zarenfamilie durch ein anderes solvenisches Assassinan
ausgeführt worden sein musste. Vom Mord an Katjenkas Eltern hatte
sie im Vorfeld gewusst, aber nicht, wie er ausgeführt worden war.

Amira spürte einen kurzen Moment Selbstabscheu, als sie berechnete, wie
sie sich verhalten müsste, um das Trauma der Zarin durch
gezieltes, unheimliches Auftauchen an den falschen Stellen zu
triggern. Auf Arten und Weisen, dass sie Wachen dichter
zu sich befehligen würde. Wenn sie weniger verstreut wären, wären
sie leichter angreifbar. Das war ja der Grund, warum die Maare inzwischen
großflächig über den ganzen Raum verteilt waren.

Aber ein Trauma war behandelbar. Wenn sie dadurch
entkommen könnten, erschien es ihr vertretbar, ihr Wissen darum
auszunutzen. Es räumte ihnen eine Chance ein. Keine sehr reale.

Die Zarin erteilte Befehle, denen Amira aufmerksam lauschte, während
sie sich gegenläufig zu Janasz durch den Raum bewegte. Janasz
verschoss einen Pfeil nach dem anderen. Dadurch war er als falsche
Wache endgültig entlarvt. Aber während er das Blasrohr an die
Lippen setzte und sich bewegte, war er konzentriert, und selbst
die Tränen in seinem Gesicht hielten ihn nicht davon ab, dem
Plan zu folgen, zu tun, was Sinn ergab und nötig war.

Rash befand sich jeweils gegenüber von Amira im Raum, Janasz als
Ankerpunkt zwischen ihnen, und tat
Dinge, die alle Anwesenden sehr irritierten. Irritation
war gut. Rash trat jeweils der Wache gegenüber, die Janasz
als nächstes ins Visier nehmen würde. Rash diente dann kurz
als eingeplante Zielscheibe für einen Angriff, dem Rash
auswich, aber ehe ein erfolgreicher Angriff
stattfinden konnte, kippte die jeweilige Wache in tiefen
Schlaf und Rash fing den Sturz ab, geradezu zärtlich.

All das konnte Amira beobachten, während sie selbst geschickt
Langmessern und Degen auswich und Betäubungspfeile in nachlässig
gekleidete Fußgelenke stach.

Die Wachen waren unvorsichtiger mit ihr als mit den anderen. Sie
hatten wohl den Auftrag, niemanden zu töten, wenn es sich vermeiden
ließe, aber sie gingen bei Amira durchaus das Risiko ein, und
sie verstand warum. Sie hatte ein Messer in jeder Hand. Sie benutzte
sie nicht, höchstens, um eine Wache von einem der anderen
Crewmitgliedern abzulenken, die durch den Raum flohen, indem
sie es dicht an ihr vorbei schleuderte. Aber vor allem nutzte
sie die darunter verborgenen Nadeln, so unbemerkt wie
möglich. Sie bewegte sich mit Abstand am geschicktesten. Sie wirkte
gefährlich und tödlich. Und sie trug die
Uniform der solvenischen Assassinanen. Sie hatte im Vorfeld nicht
damit gerechnet, dass sie letzteres verraten würde, aber unter
den Umständen ergab es Sinn und war ebenfalls von Vorteil. Natürlich
stürzten sich die Wachen auf die offensichtlichste und größte
Gefahr, sie, und erkannten erst viel später, dass Janasz, die
unscheinbare und ungeschickt wirkende, falsche Wache, ebenfalls eine
wesentliche Gefahrenquelle war.

Amira fühlte sich lebendig in diesem feuerlosen Inferno. Das Ausweichen
war wie Tanz. Sie spürte die Gefahr und Ausweglosigkeit unter der
Haut. Sie fühlte die Bewegung und Bedrohung in jedem ihrer Glieder. Nur
der Moment war wichtig.

Sie kamen weit. Viel weiter, als sie je damit gerechnet hätte. Wachen
lagen auf dem Boden verstreut. Sie sprang geschickt über sie hinweg, sodass
den Wachen, die ihr nachsetzen wollten, Körper im Weg lagen. Nur noch wenige Meter
trennten sie von der Zarin. Es hatte viele Gelegenheiten gegeben, in denen sie
sie mit einem Messerwurf hätte töten können. Aber sie wollte sie
erreichen, ihr Leben nur bedrohen, um erpressen zu können, weil das ihre
realistischste Chance war, zu entkommen.

Aber irgendwann passierte das Unausweichliche: Janasz wurde überwältigt. Nur
Sekunden später konnte die Wache, die Rash hätte auffangen wollen, Rash
in die Knie zwingen, mit einem Langmesser an der Kehle. Weniger zärtlich, als
Amira es damals getan hatte.

Sie tauschte mit Rash einen Blick, als sie ihre Messer fallen ließ und
die Hände hob. Rash lächelte und formte den Mund kurz zu einem
Kussmund. Amira schloss einen Moment die Augen und erwiderte dann die
Geste.
