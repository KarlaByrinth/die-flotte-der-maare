\BefehlVorKapitelDFDM{Sindra-n.png}{Sindra ist die Kapitänin der Flotte der Maare. Sie ist sehr groß.}{Morddrohungen, Messer, Alkohol(verbot), indirektes Misgendern in der Vergangenheit.}

Anreden
=======

\Beitext{Sindra}

Als Sindra die Tür zur Kajüte öffnete, brauchte ihr Gehirn mal
wieder erstaunlich wenig Zeit, sich darauf einzustellen, dass
sie sie unerwarteterweise nicht verlassen vorfand. Obwohl die Person, die am kleinen
Tisch saß, es wie selbstverständlich tat, als gehörte sie eben dort hin. Auf dem
Tisch dampfte außerdem Tee und zwei der Porzellantassen standen bereit, die
wohl bedeuteten, dass eine zweite Person -- vielleicht Sindra --
eingeladen war. Jene Porzellantassen, die sie nie benutzte und sich gelegentlich
fragte, warum sie sie überhaupt hatte. Immerhin waren sie
abgestaubt. Das Messer in der Hand der Person wirkte erstaunlich
wenig ausladend, aber ausladend genug, dass Sindra sich überlegte
und erste Anstalten machte, den Raum lieber wieder zu verlassen.

"Wenn du dich weiter zurückziehst, töte ich dich.", informierte die Person
sachlich.

Salvenit. Die Sprache hatte Sindra eine Weile nicht mehr
gesprochen. Sindra hielt in der Bewegung inne. "Ich kommandiere die Flotte der
Maare. Da das nicht so sehr ein Geheimnis ist und du schon einige
Tage an Bord bist, gehe ich davon aus, dass du das weißt.", sagte
sie. "Ich bin aber auch ersetzbar. Vielleicht bin ich der Crew
tot von mehr Nutzen als als Geisel. Oder vielleicht auch
nur verletzt, abhängig davon, wie gut du auf die Distanz töten kannst."

Eine unscheinbare Bewegung später steckte das Messer direkt neben Sindra
in der Tür und die Hand der Assassinperson hielt bereits ein weiteres. Es
war so schnell gegangen, dass Sindra sich nicht ganz sicher war, woher
jenes aufgetaucht war. Aber
es hatte einen Griff, der so aussah wie die Haken an der Knopfleiste
der auf den Körper passgenau zugeschnittenen Kleidung. Die Knopf- oder
viel mehr Ösenleiste wirkte auch, als gehöre da eine Öse mehr hin.

"Ist es mir erlaubt, in der Tür stehen zu bleiben?", fragte Sindra.

"Sobald Crewmitglieder hinter dir auftauchen, werde ich mich selbst
schützen. Das werden sie wahrscheinlich nicht überleben.", verkündete
die Assassinperson.

Sindra nickte langsam. Dann war es sicher besser, die Tür hinter
sich zu schließen. Der Gedanke, möglicherweise als Geisel
benutzt zu werden, ließ sie nicht ganz los.

Sie richtete ihren Blick auf das Messer in der Tür, als sie sie schloss. Es
hatte keine solche Öse und war etwas größer. Und es hatte das Material der
Tür beschädigt. Natürlich hatte es das, sonst würde es kaum dort
stecken.

"Ich möchte dir eigentlich nichts Böses.", sagte die Person. "Ich
möchte mit dir reden. Die Morddrohungen sind meine Lebensversicherung."

Sindra konnte nicht vermeiden, eine Spur zu schmunzeln. Von einer
Assassinperson als Lebensgefahr wahrgenommen zu werden, war neu.

"Würde es sich für dich als eine Bedrohung anfühlen, wenn ich das
Messer aus der Tür entfernte und dir zurückbrächte?", fragte Sindra.

Das Gesicht der Assasinperson machte einen skeptischen Eindruck. Es
war hellbraun, wenig Rotanteil darin, mit sehr dunklen Augen, mit
Schminke schwarz umrandet. Ein
dunkelblaues Kopftuch verdeckte die Haare und band sie gleichzeitig fest
zusammen.

"Mich stört die Unordnung.", erklärte Sindra. "Sicherheit geht aber
natürlich vor Ordnungsfaible. Wenn du dich damit nicht wohl fühlst..."

Die fremde Person unterbrach sie mit einer Geste. "Fass es nicht an
der Schneide an.", empfahl sie. "Ich bin nur verwundert, wie du
mit einer Person umgehst, die dir das Leben nehmen könnte, und
angedroht hat, das zu tun. Ich wurde informiert, dass du kaltblütig
wärest, aber ich hatte mir eine andere Art von Kaltblütigkeit
vorgestellt."

"Eine unfreundlichere, nehme ich an.", sagte Sindra. Sie seufzte, aber
vor allem innerlich. "Das liegt daran, dass Leute Kaltblütigkeit
mit reduzierter Expressionen durch Lautstärke, Tränen, schnelleres
oder deutlicheres Sprechen verwechseln. Ich habe starke Emotionen, wie die meisten
anderen auch." Sindra wandte sich dem Messer zu und zupfte es aus dem
Material. Um die Narbe würde sie sich später kümmern. Sie
näherte sich dem Tisch und nahm der Assassinperson
gegenüber auf dem bereitgestellten, anderen Stuhl Platz. Jene hielt ihr
die freie Hand hin, damit Sindra ihr das Messer wiedergeben könnte. Aber
Sindra hatte keine Eile, das zu tun. Sie betrachtete neugierig die
Klinge und den Griff. "Ein schönes Messer.", sagte sie. "Ich
kann nicht leugnen, dass ich auch ein Faible für Messer habe, wenn
sie nicht gerade in meiner Tür stecken. Ist die Klinge giftig?"

Die Assassinperson schüttelte den Kopf. "Nur viel schärfer, als die meisten
es einschätzen würden."

"Dann wird sie nicht so regelmäßig in Türen geschmissen, nehme
ich an? Oder regelmäßig wieder geschliffen." Sindra fuhr doch
vorsichtig mit dem Daumen über die Schneide. Die Klinge
fühlte sich schön an. Die fremde Person ihr gegenüber wirkte
leicht nervös, also reichte Sindra ihr die Waffe zurück. Dieses
Mal beobachtete sie genau, wie die Klinge mit der Öse wieder in der
Schließleiste der Kleidung verschwand und die zurückgereichte Klinge
wieder in der rechten Hand der Assassinperson landete.

Sindra wischte ihre Tasse mit ihrem Ärmel aus und hob die Teekanne
an. "Darf ich?", fragte sie.

Die fremde Person ihr gegenüber nickte. Sindra goss den Tee langsam
in die beiden Tassen. Hielt dabei den Deckel sachte mit der
anderen Hand fest. Nicht zuletzt, um das Manöver mit adäquater
Eleganz zu untermalen. Sie mochte, wie dabei der Dampf aufstieg und der
Geruch sich im Zimmer verbreitete. Aber entspannt saß sie nicht, wenn
ihre Füße auf dem Boden waren. Also sortierte sie die Beine mit
auf die Sitzfläche. Eines angewinkelt zur Seite gekippt, das andere so, dass
sie ihre Arme zum Greifen der Tasse darum herumführte. Sie hielt
sich die Tasse unter ihre
Nase und atmete langsam und mit geschlossenen Augen ein.

"Du lässt eine Person aus den Augen, die dich mit Messern bedroht?", fragte
die Asssassinperson.

"Ich habe ohnehin keine Chance.", sagte Sindra trocken. "Oder
doch?"

"Unwahrscheinlich.", stimmte die Person zu. "Ich habe einfach noch
nie eine Person bedroht, die sich völlig ohne Manieren gemütlich
mir gegenüber hingesetzt hätte. Das ist kein Vorwurf. Nur eine
Feststellung. Vielleicht sollten wir einfach mal zur
Sache kommen."

"Keine verkehrte Idee.", sagte Sindra. "Wie heißt du? Wie möchtest
du, dass ich dich anrede, und mit welchen Begriffen und Pronomen
möchtest du, dass ich über dich rede?"

"Amira. Ich bin Amira.", sagte Amira. "Ich", Amira zögerte, "darf
mir aussuchen, mit welchem Pronomen du über mich redest?"

"Auf jeden Fall!", versicherte Sindra.

Amira hob die Tasse mit der anderen Hand, ohne die Drohgebärde
mit dem Messer mit der einen zu vernachlässigen. Es war wahrscheinlich
ein Manöver, um zu kaschieren, dass Amira sich Zeit zum
Nachdenken nahm. "Ich habe mir ja viele Gedanken gemacht, wie dieses
Gespräch verlaufen könnte. Diese Version war definitiv nicht
unter den Möglichkeiten, auf die ich vorbereitet bin."

"Lass dir Zeit, wenn du sie dir selbst zugestehen kannst.", forderte
Sindra Amira auf.

Amira nutzte Zeit zunächst noch, um ein weiteres Mal Verwirrung
zum Ausdruck zu bringen. "Du redest mit einer
Person, die dir gedroht hat, dich zu
ermorden, und gibst ihr eine Wahl, mit welchen Pronomen du
über sie mit anderen reden würdest, gesetzt den Fall, du
überlebst?"

"Ich halte das für interessantere und wesentlichere
Manieren als Sitzhaltung.", kommentierte Sindra. "Ich gehe derzeit
davon aus, dass ich wahrscheinlich überlebe."

"Wahrscheinlich.", stimmte Amira zu.

Sindra lächelte. "Es stellt dennoch eine Erleichterung dar, die
Vermutung bestätigt zu bekommen.", sagte sie. "Soll ich nach
den Pronomen einfach später noch einmal fragen?"

"Ich dachte eben bis jetzt, ich müsse akzeptieren, dass Leute
einfach eines nehmen.", sagte Amira.

"Willkommen in der Freiheit.", sagte Sindra sanft. "Natürlich
nicht völlige Freiheit. Eine Entscheidung, der Crew beizutreten,
ist eine Entscheidung für immer. Aus Sicherheitsgründen. Versteht
sich. Und es gilt in meiner Flotte striktes Alkoholverbot. Aus
persönlichen Gründen. Ansonsten versuchen wir hier Zwänge und
Richtlinien, die das Leben unnötig einschränken, und unter
denen Personen sich nicht entfalten können, so gut es geht
abzubauen. Das ist am Anfang häufig ungewohnt. Und es braucht
lange."

"Und sich mit Bezeichnungen wie 'Frau' und Pronomen abzufinden, ist
ein solcher Zwang?", fragte Amira.

"Genau.", bestätigte Sindra mit einem zusätzlichen Nicken und
ernstem Lächeln. Sie nippte einen Schluck Tee aus der Tasse. Er
hatte etwas zu lange gezogen für ihren Geschmack.

"Alkohol gegen Pronomen.", murmelte Amira.

"Was würdest du wählen?", fragte Sindra.

Es war in diesem Moment, dass Amiras Körper zum ersten Mal nicht mehr
vollkommen angespannt war. Es war keine angespannte
Anspannung gewesen, keine nervöse, die etwas Böses erwartete, sondern
lediglich Körperspannung und Bereitschaft, jederzeit jenes Messer
zu werfen. Nun atmete Amira aus und es war dem Körper anzusehen. "Ich
würde gern Crewmitglied werden.", sagte sie. "Als Person, die
gerade das Leben der Kapitänin bedroht hat, rechne ich mir
eigentlich nur kleine Chancen aus. Ist Kapitänin richtig?"

"Ja, Kapitänin ist richtig. Und ja, Morddrohungen
kommen nicht so gut im Lebenslauf bei einer
Bewerbung, das ist auch richtig.", stimmte Sindra zu. "Würdest du das
Messer wegstecken?"

"Was würde es jetzt noch nützen?", fragte Amira. "Ich habe dein
Leben bereits bedroht, das lässt sich nicht mehr rückgängig machen. Wer
sagt mir, dass du mich nicht in dem Moment angreifst, in dem
keine ausreichend starke Gefahr mehr von mir ausgeht?"

"Bisher habe ich die Drohung als eine Handlung aufgefasst, die dich absichert, während
du mit mir ins Gespräch trittst. Das verstehe ich.", sagte Sindra. "Ich weiß, dass deine
Absichten nie -- oder wenigstens schon länger nicht -- waren, mich umzubringen. Dazu hast du in den vergangenen Tagen
genügend Gelegenheiten verstreichen lassen.", fuhr sie fort. "Ich
verstehe auch, wenn du diese Sicherheit immer noch brauchst. Für dich als
Privatperson, die vielleicht irgendwann wieder von hier verschwinden möchte. Weil
unser Interesse sicher nicht ist, dass du uns verlässt und auf diese
Weise wesentliche Informationen Preis geben kannst." Sindra machte
eine kurze Betonungspause. "Aber wenn du Crewmitglied werden möchtest, dann
ist Vertrauen auf einer gewissen Ebene eine Grundvoraussetzung. Für den
Antrag auf Mitgliedschaft verlange ich, dass du auf entsprechende Drohungen
verzichtest."

Sindra ließ Amira Zeit. Es war eine schwere Entscheidung, natürlich. Sindra
fragte sich sogar, ob die Forderung unfair wäre. Ein seltsamer Konflikt, denn
Amira konnte sie aktuell sehr wohl töten, und umgekehrt war das nicht
der Fall. Aber sie verstand Amiras Angst, und dass es sich wie ein
Ungleichgewicht anfühlen würde. Vielleicht auch eines war.

Amira atmete noch einmal tief aus, dieses Mal weniger entspannt. Amira legte
das Messer behutsam zwischen sie auf den Tisch, nicht in die Mitte, sondern
weiter auf Sindras Seite. "Ich liefere mich damit der ganzen
Crew aus. Ich sehe, wie in der Bedrohungssituation hier mein Leben
gegen deines steht. Aber ein Spielball einer größeren Gruppe zu
werden, fühlt sich ganz anders an."

"Als Crewmitglied wärest du ebenso in der Position, Spielball
werden zu können, wie jedes andere Mitglied auch. Die Crew hat nicht
mehr Macht gegen dich, als gegen jedes andere Mitglied.", argumentierte
Sindra. "Aber ich verstehe das Gefühl. Es geht hier darum, dass
wir in der Gruppe zusammengewachsen sind und uns gegenseitig
Rückhalt geben. Das hast du noch vor dir. Aber Drohung ist
kein guter Start dafür, meinst du nicht?"

Amira zupfte wieder das Messer von vorhin aus der Kleidung, um es
auch auf dem Tisch abzulegen.

"Das kannst du stecken lassen.", sagte Sindra.

Amira hielt in der Bewegung inne und blickte Sindra skeptisch an. "Ich
könnte dich immer noch umbringen.", hielt sie fest. "In einem Wimpernschlag."

Sindra nickte. "Wenn ich dich nicht gefangen nehme -- und das habe ich nicht
vor --, wirst du dazu in nächster Zeit sehr viel Gelegenheit haben.", sagte
sie. "Es geht mir nicht darum, dass du wehrlos bist. Sondern um die
kommunizierte Drohung dabei."

Amira schob das Messer wieder in die Kleidung zurück. Das Messer, das
zwischen ihnen lag, ließ Amira liegen, sah es einige Momente unschlüssig
an, bis Amira wohl beschloss, dass es ruhig genau da liegen könnte. Stattdessen
kramte Amira tiefer in der Kleidung nach einem Umschlag aus Spaltleder
oder ähnlichem Material. "Ich hatte von meinem durch Morddrohungen
negativ eingefärbten Lebenslauf geplant mit einer Information abzulenken, die
ich gegen eine Crewmitgliedschaft einzutauschen gedachte.", sagte Amira.
"Es erscheint mir nun taktisch sinnvoller, eher mit Ehrlichkeit zu
punkten und sie unabhängig von Bedingungen zu überreichen. Ich
kann leider nicht anders als taktisch denken."

Sindra nahm den Umschlag entgegen. "Ehrlichkeit, und vielleicht viel mehr
Direktheit und Unverblümtheit wird hier sehr wertgeschätzt. Wie taktisch
Leute jeweils ehrlich sind, ist eine philosophische Frage, über die -- würde
ich meinen -- die meisten nicht ausreichend reflektiert haben, um sich
ihr Lieblingsurteil zu erlauben, dass Ehrlichkeit aus taktischen
Gründen etwas Verwerflicheres wäre als aus anderen."

"Ich kann mir nicht leisten, mich aus anderen als taktischen Gründen
für Ehrlichkeit zu entscheiden.", sagte Amira. Es klang scharf, vielleicht
verletzt.

Sindra blickte sie ein paar Momente überlegend an, während ihre
Finger über den Umschlag strichen. Spaltleder. Weiches
Spaltleder. "Es ist ein Privileg, möchtest
du damit sagen."

Amira nickte.

Sindra wiederholte die zustimmende Geste. "Vielleicht hast du recht.", fügte
sie hinzu. "Ich wünschte, ich könnte dir Sicherheit geben."

"Ich habe mich noch nie so sicher gefühlt wie jetzt.", sagte Amira, plötzlich
viel weicher und leiser.

"Oh, das ist interessant.", hielt Sindra fest. Ihr Blick war auf den
Umschlag gewandert, über den ihre Finger streiften. "Und schade.", fügte
sie hinzu. "Und nicht ungewöhnlich für Personen, deren Ansinnen ist, bei
den Maaren anzuheuern."

"Das heißt nicht, dass ich mich sicher fühle.", stellte Amira klar, nun
wieder in bestimmterem Tonfall.

"Ich weiß.", antwortete Sindra schlicht.

Sindra wickelte die dünne
Lederschnur vom Knopf, der den Umschlag zusammenhielt und öffnete ihn. Ihr
Körper zeigte nicht häufig starke Gefühlsreaktionen, aber nun hatte
sie eine: Einen kurzen Schweißausbruch. Im Umschlag lag ein leichtes, dünnes
Papier, auf das mit einem einfachen Kohlestift ein Brief geschrieben
war. Kazdulan. Die Sprache, die die Flotte für Schriftverkehr benutzte, um, falls
Briefe abgefangen würden, am ehesten für mit einem der Zwergenvölker
alliiert gehalten zu werden. Es war von keinem der Landvölker zu erwarten, dass
sie sie je unterstützen würden, aber wenn von einem, dann am ehesten vom
süd-west-maerdhischen Zarenreich der Zwerge, unter Zarin Katjenka.

Sie nutzten solches Papier eigentlich nicht, was nicht hieß, dass sie
kein solches irgendwo an Bord haben könnten, und auch eher andere
Schreibutensilien, aber es war Rashs Handschrift. Und Rash würde
niemals Briefe von Bord schmuggeln. Oder doch? Doch nicht Rash! Der
Text war viel weniger ausführlich und ausgefeilt geschrieben, als
Rash das für gewöhnlich tat, die Sätze waren eher möglichst kurz und
wortarm gehalten und die Groß- und Kleinschreibung war
durcheinander. Letzteres sah aber eher wie Absicht aus.

Der Text beinhaltete zunächst eine Beschreibung ihrer Person, ihrer
Stärken und Schwächen, ihrer Geschmäcker und ähnliches. Eine Liste
der Sprachen, die sie sprach. Salvenit, die Sprache, in der Amira und sie
immer noch kommunizierten, war nicht mit aufgelistet. Hatte Amira
es auf gut Glück probiert? Gab es mehr Briefe?

"Steht auf Frauen.", las Sindra vor. "Da hat die Person, die das
schrieb, aber nicht gut aufgepasst. Beziehungsweise, nun ja,
ich stehe auch auf Frauen, so ist das nicht. Aber eben nicht
mehr, als auf Personen mit beliebigen anderen Geschlechtern."

"Mirash.", lenkte Amira das Thema für einen kurzen Moment wieder
auf Sindras Gedanken von vorhin, die sie nicht ausgesprochen hatte.

Dieses Mal fühlte sie körperlich nichts. Sie vermutete, dass
ihr auch der Schweißausbruch zuvor nicht anzusehen gewesen war. Wie fast immer. Mirash
war Rashs ganzer Namen, aber Rash wurde eigentlich immer abgekürzt.

"Und ja. Das war meine Eintrittskarte.", sagte Amira. "Sie
suchten wegen der Zeilen eine Frau für den Auftragsmord."

"Das ist Information, die bei mir einige Fragen aufwirft." Sindra
schmunzelte und blickte wieder auf in Amiras Gesicht.

"Du darfst sie alle stellen.", lud Amira ein.

"Bist du eine Frau?", fragte Sindra, aber fügte dann doch
hinzu: "Es ist eine sehr persönliche Frage, deren Antwort mich
nichts angeht. Aber vielleicht möchtest du sie gern für dich haben."

"Ich habe mich noch nie wohl gefühlt, wenn ich als 'Frau' bezeichnet
wurde, oder mich selbst so bezeichnen musste.", gab Amira zu. "Aber
das Pronomen 'er' fühlt sich sehr falsch an in meinem Kopf."

Sindra hielt ihr Lächeln nicht zurück. "Dann werde ich dich niemals 'Frau'
nennen.", sagte sie. "Pronomen gibt es viele, und falls sie nicht reichen,
darfst du dir auch welche ausdenken."

"Mir ist das zu stressig. Ich bleibe bei 'sie'.", beschloss
Amira. "Erst einmal." Sie griff nach der Tasse Tee, mit beiden
Händen, vielleicht, um sich daran festzuhalten. Was Unfug war, von
einem physikalischen Standpunkt aus betrachtet, aber von einem
psychologischen aus sehr wohl Sinn ergab. "Wieso fühle ich
so eine starke Emotion, nur, weil du mich nicht 'Frau'
nennen wirst? Das ist doch lächerlich!"

Sindra schüttelte den Kopf und verknotete die Beine neu. Als
sie realisierte, was sie tat, musste sie beinahe lachen. "Wie
ich schon sagte, sind das aus meiner Sicht interessantere und
wesentlichere Manieren als Sitzhaltungen.", sagte sie. Sie
wartete das Ausbreiten des schmalen Lächelns auf Amiras Gesicht ab, bevor
sie hinzufügte: "Und sehr wohl etwas, was viele Leute ohne zu zögern
über Alkoholkonsum wählen."

"Aber warum ist es so stark?", fragte Amira. "Es sind
Worte!"

Sindra schüttelte noch einmal den Kopf. "Es ist Respekt. Und
Raum. Und Freiheit."

"Und warum ist das deine erste Frage?", fragte Amira "Und nicht etwa, wer
dich umbringen möchte."

"Das ist in der Tat eine Frage, die ich stellen möchte. Ich
bezweifle nicht, dass wir darüber in den nächsten Tagen reden
werden. Daher habe ich sie nicht als erste Frage gewählt." Sindra
schloss die Augen wieder und trank sehr langsam einen weiteren
Schluck vom Tee. "Zu lang gezogen. Definitiv.", sagte sie.

Amira lachte auf.
