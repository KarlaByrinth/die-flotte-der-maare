\BefehlVorKapitelDFDM{Amira-n.png}{Amira ist erst kürzlich an Bord und hat insgesamt sehr viele starke Gefühle.}{Spärlich bekleidetes Kuscheln, Brüste, Sex - erwähnt, Transition?, Ausliefern, BDSM, Narben, Traumaflashback, Erpressung, Mord - erwähnt, Folter - erwähnt, Trauer.}

Leben
=====

\Beitext{Amira}

Amira fühlte. Fühlte sich so lebendig, wie vielleicht noch nie. Der
Morgen brach wohl an, aber Amira fühlte sich nicht nach aufstehen. Und
zwar nicht, weil sie keinen Sinn darin sah, aufzustehen, sondern
weil es sehr viel Sinn hatte, in dieser unbewussten Umarmung
liegen zu bleiben. Rash schlief, die Arme lose um sie gelegt. Rash schlief nur
in Unterwäsche. Das Unterhemd lag dünn und lose über Rashs
Körper. Die Körperwärme drang durch den dünnen Stoff und wärmte
Amiras nackte Haut. Ein paar Falten hatten Muster in selbige
gedrückt.

Amira spürte, wie Leben in Rashs Hände kam. Sie blieben an Ort und
Stelle, aber fühlten bewusst Amiras Haut darunter. Irgendwovon
war Rash aufgewacht. Amira bemerkte erst daran, dass sie Rash unbewusst
in der Halsbeuge geküsst hatte. Mehrfach. Rash hatte noch nicht die Augen
geöffnet, da zitterte der Atem schon wieder sachte. Amira
fühlte die Vibrationen über die Lippen, die immer noch an der
Haut lagen. Und küsste sie noch einmal, wo der Mund schon da
war.

Und wieder wanderten Rashs Hände über ihren Körper. Amira konnte
nicht leugnen, dass sie immer noch nicht genug davon hatte. Würde
der Zeitpunkt irgendwann kommen? Oder der Zeitpunkt, zu dem sie
dann Sex wollte und nicht nur romantisch geliebt und gestreichelt
werden mochte? Wo war überhaupt die Grenze? Aber Amira
konnte sich zumindest im Moment nicht so richtig
vorstellen, etwas mit Rash tun zu wollen, was sie selbst Sex
nennen würde, also kannte sie wohl eine persönliche Grenze. Es
war auch nicht wichtig. Entweder es kam oder es kam nicht. Jetzt
erstmal erfühlte sie Rashs Körper. Streichelte über das dünne
Stofffabrikat, das raue und feine Strukturen vereinte. Sie hatte
sich bisher eher streicheln lassen. Nun erwischte sie sich dabei, wie
ihre eigenen Hände immer wieder unter Rashs Hemd schlüpften, und
sie sie wieder zurückzog, wenn sie es realisierte, weil
Rash gesagt hatte, dass sie Rash nicht weiter als bis auf
die Unterwäsche ausziehen dürfe. Rash wehrte sich gegen nichts, aber
in Amira bohrte ein schlechtes Gewissen, dass es ihr passierte, bis
Rash einfach selbst das Unterhemd auszog.

Rash ließ sich betrachten, in diesem Halbdunkel. Lächelte vorsichtig,
vielleicht unsicher. Rashs Brust war nicht ganz flach, die
Brüste waren klein, vielleicht wie sie bei Amira etwa nach einem halben
Jahr Pubertät ausgesehen haben mochten. Vorsichtig ertastete
Amira die weiche Haut und Rashs Atem zitterte noch
mehr. Rash wusste nicht so genau, wohin mit den Armen. Sie
wollten Amira wieder fest umarmen, glaubte sie, weil sie
sie so berührten, wie sie es sonst taten, wenn das der Fall
war, aber dann wäre Amira nicht mehr an Rashs Vorderseite
gekommen. Gegen Arme, die nicht wussten, wo sie hinsollten,
kannte Amira ein Mittel. Sie blickte Rash ins Gesicht, als
sie Rashs Arme auf Rashs Rücken sortierte und dort
fixierte. Sie tat es sanft. Rash hätte sich wehren können, tat
dies aber nicht. Aber Rashs Einatmen, als Amira plötzlich
festhielt, als die Arme angekommen waren, wo sie
hinsollten, sodass Rash die Arme eben nicht mehr hätte
einfach wegnehmen können, war fast laut. Amira befürchtete, es
könnte Janasz und Ashnekov wecken. Sie genoss, wie Rash
die Augen schließen musste und die Fassung verlor, während
sie über Rashs Brüste und Rashs Brustbein
streichelte. Wie der Körper unter ihr bebte und
genoss. Das, was Rash bei ihr in den letzten Tagen so oft
ausprobiert hatte, funktionierte also auch umgekehrt.

Rashs Haut am Oberkörper war zart und weich. Amiras Hand
streichelte auch über den vor Anspannung eingezogenen
Bauch. Der untere Teil des Bauchs war übersät von
vielen kleinen Narben. Amira streichelte darüber, ohne
zu fragen, liebte auch die Narben wie jeden anderen Teil
von Rashs Körper. Sie fühlte sich danach, Rash zu sagen,
dass sie Rash liebte.

Sie durfte nicht lieben. Das machte sie erpressbar.

Amira erinnerte sich nicht daran, wie sie die Haltung aufgelöst
hatten. Als sie wieder zu sich kam, hielt Rash sie sanft im
Arm und flüsterte ihr Dinge ins Ohr wie "Ich bin da." Sie
bekam mit, wie Ashnekov die Tür hinter sich zuzog. Auch
Janasz war nicht mehr da. Sie war allein mit Rash, lag
da, zitterte und heulte wieder. Sie durfte nichts in
der Welt haben, dass ihr so viel wert wäre, dass sie
darüber erpresst werden könnte. Erinnerungen an Gefühle
rannen durch ihren Körper, als gehörten sie zu einem anderen
Teil von ihr, mit dem sie nicht gut kommunizieren konnte. Zu
einem ohne Gedächtnis, der versuchte, eine
Tür in ihr einzureißen, aber sie war nicht imstande, sie
wirklich zu öffnen, ob sie wollte oder nicht. Gefühle, die
sie nicht einmal richtig benennen konnte. Gefühle, die
sie im Griff hatten, die sie bestimmten, ein Teil von
ihr waren, den sie nicht verstand oder fassen konnte.

"Ich habe alles kaputt gemacht.", flüsterte sie. "Ich
habe Janasz und Ashnekov verjagt und dich um Genießen
gebracht. Ich wünschte, ich könnte auch gut für
dich sein." Das Problem war das einfachere. Und es
lenkte von der Tür ab.

"Du bist gut für mich, wie du bist.", sagte Rash leise
und sanft in ihr Ohr. "Bist du. Dabei musst du für überhaupt
niemanden gut sein."

Amiras Psyche schmerzte. Wo kam das ganze Gefühl her? Und
warum musste es solche Kontraste fahren? "Ich möchte
dich nicht belasten.", sagte Amira. "Ich weiß, dass
das für dich in Ordnung ist, aber es fühlt sich für mich
so an."

Rashs Arme zogen sich vorsichtig ein bisschen enger, als
wäre Amira etwas sehr Zerbrechliches. "Ich habe dich
lieb.", sagte Rash.

Wie konnte so etwas so schnell gehen? Waren das nicht viel
eher Hormone? Aber Amira vertraute den Worten. Und eine
neue Welle des Abgrenzungsgefühls überrollte sie. "Ich
darf dich nicht lieben. Was, wenn du gefangen genommen
wirst und ich damit unter Druck gesetzt werde?"

"Dann verabschiedest du dich von mir.", sagte Rash. "Mein
Leben ist es nicht wert, dass du Dinge tust, die du nicht
vertreten kannst."

Amira zog sich etwas aus der Umarmung und starrte in Rashs
entspanntes Gesicht. Sie glaubte, dass Rash das absolut
ernst meinte, aber es half nicht. Oder wenig. Amira
versuchte, sich vorzustellen, sich gegen Erpressung
zu wehren, während Rash ermordet würde. Sie schloss
die Augen und versuchte mit dem schrecklichen Gedanken
klarzukommen. Es war schwierig, weil die Vorstellung
an einer Erinnerung hing. Keine, die sie hätte benennen
können, eigentlich mehr ein Gefühl. Amira wusste nicht
genau, was da war, oder womit es genau verknüpft war.

"Atme.", flüsterte Rash.

Amira folgte der Empfehlung. Eigentlich wusste sie, dass
Atem das war, was sie erdete. Der Atem floss gerade
nicht sinnvoll von alleine. Sie kontrollierte ihn eine
Weile bewusst, bis der nächste schlimme Gedanke sie
überrollte. "Sie könnten dich nicht bloß töten wollen, sondern
würden dich vielleicht foltern."

In Rashs Gesicht trat ein sehr unscheinbares Lächeln. "Ich
habe da einen Fetisch.", sagte Rash. "Spiele sind immer besser
als die Elemente meiner Fetische in der Realität. Das funktioniert
eigentlich nicht. Ich würde trotzdem versuchen, ein
Spiel daraus zu machen und schauen, wie wem auch immer
dabei die Gedankenwelt explodiert. Ich schätze, von
meiner Seite ist dann Mal in Ordnung, wenn ich für
das entsprechende Spiel nicht nach Konsens frage?"

Humor? Das war nicht nur Humor, aber auch. "Nimmst
du mich ernst?"

"Ja.", sagte Rash ohne Zögern. Das Lächeln verschwand. "Vielleicht
gehe ich mit deinen Fragen völlig falsch um."

Amira schüttelte den Kopf. "Mit so etwas gibt es keinen
richtigen Umgang."

"Weise.", kommentierte Rash sanft. Und legte die Arme wieder
vorsichtig um Amira, als ihr Körper gegen Rashs kippte. "Vielleicht
hilft es, dass ich das weiß. Dass es keinen richtigen Umgang
gibt. Und ich mir wünschen würde, dass du dich schützt, so gut
es geht, aber mit dem Wissen akzeptieren würde, wie auch
immer du entscheidest."

"Und wenn das heißt, dass ich dich nicht lieben kann?", fragte Amira. Warum
klang das so, wie künstliches Drama in einem schlechten Roman?

"Dann kannst du es nicht.", antwortete Rash schlicht. "Darf ich
dich lieben, oder übt das Druck auf dich aus, der auch
schlimme Dinge anrichtet?"

Amiras Körper zitterte. Sie antwortete nicht direkt. Und
dann sprach sie aus, was eben einfach die Wahrheit war: "Ich
bin doch schon längst verliebt in dich. Die Gefühle
scheren sich nicht darum, was sie dürfen."

Wie sanft und vorsichtig konnten diese Arme sein? Rashs
Atem auf ihrem Gesicht, als Rash sie ganz leicht auf die
Schläfe küsste. Rashs Kehlkopf an ihrer Wange, als Rash
schwer schluckte. Und dann Feuchte auf ihrer Stirn. Weinte
Rash? Sie sah nicht nach. Sie nahm Rash zurück in den
Arm, als es etwas mehr wurde. Aber Rash blieb ruhig dabei.

---

Als Amira das nächste Mal aufwachte, fühlten sich die
Gefühle einigermaßen sortiert und sinnvoll verpackt
an, sodass sie sich in der Lage fühlte, aufzustehen. Rash
schlief. Sie war sich nicht sicher, ob Rash
kurz aufwachte, als sie von der schmalen Kiste herabstieg.

Sie spürte den Boden so bewusst unter den Füßen wie
ganz am Anfang, nur vielleicht nun mit einem glücklicheren
Grundgefühl, oder einem, das weniger gleichgültig war, was gewissermaßen
auch eine Form von Glücklichkeitsgefühl war. Es war seltsamer Boden, das hatte sie
auch schon damals festgestellt. Es war kein Holz. Er
war sehr leicht, hatte eine geringe Dichte, aber war
eher noch weniger Feuchtigkeit aufnehmend als Holz, sogar beinahe
wasserabweisend. Und er fühlte sich lebendig an. Als
würde etwas darin ganz sachte rauschen. Oder atmen? Das
ganze Schiff bestand aus diesem Material.

Sie hatte mit ihren Messern neu üben müssen, wie
sie darin stecken bleiben würden, und das Üben hatte
sich dabei etwas unangenehm angefühlt. Als würde
sie das Schiff verletzen. Aber sie hatte das Gefühl
damals beiseite geschoben, weil der Gedanke ihr albern
vorgekommen war. Und dann hatte Sindra so behutsam
reagiert, das Messer entfernt, als handele es sich
tatsächlich um eine Verletzung. Auch das hatte Amira
zunächst Sindras besonderem Charakter zugeschrieben: Sindra
hatte einige Eigenheiten, die Ordnung, Sitzen, Haltungen
oder Kommunikation betrafen. Sie hatten nicht selten
etwas mit Fürsorglichkeit zu tun, was nicht immer sofort
auf der Hand lag.

Aber nun, als Amira das Gefühl unter den Füßen wahrnahm,
hatte ihre Gedankenwelt endlich Gelegenheit, ihre
Wahrnehmung ernst zu nehmen. Sie lächelte. Mehr als
das war es nicht. Nur der Schritt, sich die Frage
ernst gemeint zu stellen: Lebte das Schiff?

Sie zog sich achtsam an und stieg an Deck. Es war
Mittag und fast alle Crewmitglieder saßen auf den Planken
im Kreis. Einige auf dem Boden und einige auf Kisten
oder Fässern. Die Stimmung war entspannt, aber auch nicht
glücklich oder ausgelassen. Jentel machte ihr sofort
Platz und lud sie mit einer Geste ein, sich dazuzusetzen.

"Dies ist eine Trauerfeier.", informierte Sindra sie
in einer Gesprächspause. "Janasz meinte, wir sollten
euch besser gerade nicht stören. War das in Ordnung?"

Amira nickte einfach.

"Aber nun, wo du da bist, sollten wir vielleicht nach
Rash schicken?", überlegte die Kapitänin.

Amira stand sofort wieder auf. "Ich hole Rash."

---

Es war eine schöne Trauerfeier, fand Amira. Sie hatte so etwas
noch nie erlebt. Sie war durchaus schon auf Trauerfeiern anwesend
gewesen, aber diese hier war anders. Sie war provisorisch. Einige
sprachen davon, dass sie am liebsten Blumen gehabt hätten, um
sie ins Meer zu werfen, aber sie fuhren gerade in so tiefen
Gewässern, dass sie nicht einmal an schönere Algen gekommen
wären. Wasserblumen.

Die Feier war außerdem individuell. Keine Form von Trauer war verboten. Der Reihe
nach kam jede Person dran und durfte etwas sagen, sagen, wie
sie trauerte, was für Trost sie brauchte, wen sie vermisste. Es
wurden Erinnerungen ausgetauscht. Nicht unbedingt nur gute, sondern
was die Personen mit den Verstorbenen verknüpften. Auf
diese Art lernte Amira die Crewmitglieder besser kennen, sogar
die, die nun nicht mehr waren. Manchmal wurde sogar gelacht.

Und dann kam die Reihe überraschend an sie: "Amira.", sagte
Sindra. "Ich weiß, du kennst niemanden der Verstorbenen. Du
bist natürlich trotzdem eingeladen, in dieser Runde etwas zu
sagen oder Trauer zum Ausdruck zu bringen."

Amira wollte erst den Kopf schütteln, als es ihr plötzlich
die Kehle zuschnürte. Sie schloss die Augen und atmete
bewusst, bis sie sich zutraute, zu sprechen. "Ich weiß
wirklich nicht, ob das hierher gehört.", murmelte sie.

"Alles gehört hierher.", motivierte Kamira freundlich.

Amira musste fast lachen. In dieses traurige Gefühl hinein. Weil
Kamira etwas Ähnliches in der Sitzung mit ihr gesagt
hatte. Amira war sich nicht sicher, wie lange ein Körper
dieses Gefühls-Hin-Und-Her aushalten könnte. "Als
ich vierzehn war, ist meine Schwester gestorben." Weil
es mit ihrem Hintergrund auch andere Gründe geben könnte, die
offensichtlich scheinen könnten, fügte sie hastig hinzu: "An
einer schlimmen Krankheit. Wir wussten nicht welche, sie
hatte schlimmes Fieber." Sie runzelte die Stirn. "Mein
Geschwister.", korrigierte sie. "Ich weiß gar nicht, ob
es Majil nicht ähnlich gegangen sein könnte, wie mir." Dann
kam sie zurück zum Punkt. "Es ist lange her und hat
nichts mit jetzt zu tun. Aber ich habe nie getrauert. Es
gab nie Raum dafür."

Jentel neben ihr streckte anbietend eine Hand aus, falls sie sie
nehmen wollte. Amira zögerte nur einen Augenblick. Jentels
Hand war tröstend kalt. Die Schwimmhäute zwischen sainen
Fingern legten sich zart um ihre Haut.

"Ich habe trotzdem Angst, dass das irgendwie fehl am Platz
ist.", murmelte Amira.

Die Reaktion, die darauf folgte, -- ein geschlossenes Kopfschütteln
und Gemurmel, dass sie hier richtig wäre damit -- , schob wieder
diesen mächtigen Kloß durch ihren Hals. Damit hatte Amira
nicht gerechnet. Sie weinte beinahe gar nicht um ihr Geschwister, sondern
weil hier alle so lieb zu ihr waren.

"Ich hätte damals gern eine Kerze angezündet.", sagte sie
leise. "Majil mochte Flammen. Majil war wie Feuer. Wütend an
den richtigen Stellen. Die Wut hat immer geholfen."

Die anderen ließen sie reden. Und schweigen. Sie erzählte
eigentlich nicht viel. Aber plötzlich war sie voller längst
verdrängter Bilder.

"Ich glaube, wir haben zwei Kerzen an Bord.", sagte Sindra. "Ich
hole sie. Da war vorhin schon Mal der Wunsch danach, da hatte
ich noch nicht daran gedacht, dass wir so etwas ja haben."

Amira warf einen verwirrten Blick auf die Kapitänin, die nun
aufstand und zur Kajüte ging. Nicht, weil sie etwas
Falsches gesagt hatte, aber ihr völlig unberührter Ton
irritierte Amira dann doch. Kannte sie keine Trauer? Oder
zeigte sich das bei ihr bloß sehr anders.

Aber sie verdrängte alles wieder, als Bewegung aufkam. Smjer
brachte eine weitere Kiste, auf
die die Kerzen in eine Schale platziert wurden. Sie waren sehr
vorsichtig an Bord mit Feuer.

Marah holte ein schon leicht angekohltes Holzstäbchen hervor
und stellte eines der Öllämpchen mit auf die Kiste. Das war
wohl dazu gedacht, die Kerzen anzuzünden. Die andere Person, die
eine Kerze hatte anzünden wollen, war Janasz gewesen.

Es war ein Moment, der sich andächtig anfühlte, als sie
beide sich in der Mitte gegenüberhockten, die Kiste zwischen ihnen, und
erst Janasz eine Kerze anzündete und dann Amira die andere. Sie
blickte in die Flamme, als sie klein entstand, wie sie das
Wachs vorsichtig anlöste und dann gemütlich vor sich
hinflackerte. Sie assoziierte ihr Geschwister mit dieser
Flamme und fühlte vorsichtig in das Gefühl hinein. Manchmal
drohte der leichte Wind, die Flamme auszupusten, aber
es war zum Glück kein starker Wind und die Schale hielt das nötigste
vom Wind ab. Der Himmel war bedeckt, aber es regnete nicht. Es
war eine gute Atmosphäre für eine Kerzenflamme.

---

Später stand Amira an der Reling und blickte aufs Meer hinaus. Sie
fühlte sich so viel leichter. Sie fühlte sich zittrig, als hätte
sie ein sehr schweres Gewicht durch die Gegend getragen. Und
vielleicht hatte sie das auch.

Rash trat neben sie. "Möchtest du lieber allein sein?", fragte
Rash.

Amira schüttelte den Kopf. "Ich habe eine seltsame Frage."

Rash grinste. "Ich liebe seltsame Fragen!"

"Würdest du deine Beziehung mit mir gern für alle sichtbar
leben?" Amira fühlte bei der Frage schon wieder ein Zittern
in der Bauchgegend. Ein Schönes. Und dann Angst: Hatte sie
sich mit der Bezeichnung Beziehung zu weit über die Reling
gelehnt? War das eine sinnvoll übertragene Redewendung?

"So sichtbar, wie du dich wohl fühlst.", anwortete
Rash. "Wenn du Angst hast, wegen der Erpressungsache, dann
führe ich mit dir Scheinstreitgespräche und kuschel mit dir
in der Bilge." Rash runzelte die Stirn und grinste. "Besser
nicht in unserer, wenn wir nicht gerade Gleitfahrt machen. Unter
Wasser bekomme ich noch schlechter Luft, als wenn du
mich berührst." Dann grinste Rash wieder. "Aber wir können
Ashnekov und Janasz fragen, ob wir auch mal mit Ausräumen
von Schiffsbäuchen dran sein dürfen. Die haben brauchbar
trockene Bilgen. Bilges? Wie ist der Plural?"

Amira grinste. Das, das war flirten! Leises, fieses
Flirten. "Ich habe Angst davor, aber ich möchte es gern
maximal sichtbar.", sagte sie. "Jetzt."

Und Rash nahm sie auf eine Weise in den Arm, die nur sehr
schwer anders interpretiert werden könnte, als etwas sehr
Inniges. "Wenn du möchtest, brülle ich über das Deck, dass
du meine Liebesperson bist. Oder ich dir gehöre, wenn du
willst."

"Ich möchte nicht, dass du mir außerhalb von ausgemachten
Spielen gehörst.", stellte Amira klar. Und fügte dann
sanft hinzu: "Du musst nichts brüllen. Das hier reicht mir."

Rash biss ihr zärtlich ins Ohr. "Damit kann ich gut leben."
