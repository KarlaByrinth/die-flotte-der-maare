\BefehlVorKapitelDFDM{Smjer-n.png}{}{Amputierte Gliedmaßen, Eine Entsprechung zu Misantropie.}

Die Schattenmuräne
==================

\Beitext{Smjer}

Ihm fiel nichts ein, was ein gutes Gegenstück eines Holzbeins für
eine fehlende Fluke gewesen wäre. Das war der
erste Gedanke, der Smjer durch den Kopf ging, als ihm klar wurde, dass er vielleicht
als Pirat in die Geschichte eingehen würde. Vielleicht. Wenn
Geschichtsschreibende nicht finden würden, dass Piraterie und
Nixendasein sich gegenseitig ausschlössen. Ein an stereotypische Vorstellungen
angepassteres Erscheinungsbild durch eine
einfache Prothese am Ende seines Fischschwanzes, wo
ihm besagte Flosse fehlte, wäre dahingehend möglicherweise
ja zuträglich gewesen. Eben etwas in Richtung fußloses Holzbein
oder Hakenhand. Eine Nixe mit
einem Haken statt Heckflosse mochte schon einen
makaberen Eindruck machen. Aber ein Haken am Ende des Fischschwanzes
hatte einfach nicht den richtigen Flair. Vielleicht dann
eher ein Rechen, eine Harke oder eine Sense.

Smjer hatte schon Prothesen ausprobiert, die einigermaßen
wie eine Fluke aussahen, die sogar aus weichem, flexiblem
Material waren, das dem Wasser ähnlichen Widerstand bot, wie
seine Fluke es damals getan hatte. Es fehlte natürlich
das Gefühl in der Fluke. Und noch so einiges war anders. Es
war schon gute Technik, durchaus. Smjer hatte eine Zeitlang
damit schwimmen geübt. Aber eigentlich war er auch schon
vor dem Vorfall, bei dem er seine Fluke verloren hatte,
nicht so übermäßig begeistert vom Tauchen und Schwimmen
gewesen. Für dieses Landsvolk mochte das ungewöhnlich
klingen. Dass eine Nixe nicht so gern schwamm. Aber
schwimmen war ja auch nicht schwimmen. Smjers Sprache
kannte dafür viele Worte. Schwimmen war so wenig schwimmen, wie
laufen, gehen, spazieren oder rennen dasselbe waren. Die Schwimm-Entsprechung
eines gemütlichen Spaziergangs fand Smjer jetzt gar nicht so übel. Aber
für ein Spazierschwimmen brauchte Smjer keine Prothese. Zumindest
nicht, wenn andere ihr Tempo für ihn ein wenig anpassten, wofür
sich sein Umfeld gern die Zeit nahm. Das
ging dann auch ohne Prothese. Um dazu ausreichend Schwung unter Wasser zu
bekommen, war sein Körper geschmeidig und wendig genug. Robben
hatten ja auch keine Fluke und tauchen lief bei ihnen
super.

Smjer hatte Technik und Bauwerk studiert. Schwerpunkt Schiffsbau
und Optik. Er hätte nicht unbedingt damit gerechnet, dass
er als Schiffsmechanikeran auch gefragt werden würde, Teil der
Crew zu werden, aber als der Bau der Schattenmuräne abgeschlossen
war, deren Konzept, Entwurf und Bau zu einem großen
Teil sein Verdienst war, war genau das passiert.

"Es spricht nichts dagegen, an Bord an Entwürfen für den Bau
weiterer Schiffe zu arbeiten?", fragte er.

Kamira schüttelte den Kopf. Er stellte den Nixenanteil der
Crew zusammen. Es war das erste Hybridschiff der Geschichte. 'Hybrid'
bedeutete, dass neben Nixen auch Fußvolk an Bord kommen würde. "Im
Gegenteil.", sagte Kamira. "Ein Hybridschiff zu fahren, ist für uns
alle eine neue Erfahrung. Ich stelle mir vor, dass es sehr hilfreich
für weitere Projekte ist, wenn die hauptverantwortliche Person
für den Bau die Informationen, wie es funktioniert und wie sich das Schiff
verhält, direkt miterlebt und nicht erst aus zweiter Hand
erfährt."

"Da ist was dran.", brummte Smjer. "Dann baue ich die ganze Takelage
gar nicht erst ab, mit der ich gut über das Deck tanzen kann." Er
hatte einen Haufen Seilwinden angebracht, sowie während des Baus
auf fast jedem Deck einen Rollstuhl und mehrere Rollbretter
platziert, um sich rasch fortbewegen
zu können. Rollbretter, mit denen er sich mit den Händen auf dem Boden
Anschwung gab, und die höhenverstellbar und kippbar waren. Die
meisten Nixen robbten an Land, das
reichte. Auf Schiffen hatten sie natürlich Rollsitze und
Gurte. Auf den Schiffen, die keine Hybridschiffe
waren, gab es nicht so viel Raum, der anders hätte überwunden
werden müssen, weil sie eben nur für Nixen gebaut
waren. Das Hybridschiff hatte Rutschen. Die Rollbretter, -stühle
und Seilwinden waren eine Besonderheit, die er sich
eingerichtet hatte, überall einrichtete, wo er arbeitete. Da er sich durch die
Vorrichtungen an Land mehr Flexibilität eingerichtet hatte, als
für Nixen gewöhnlich, wurde
er manchmal auch scherzhaft als Landnixe bezeichnet. Er mochte
die Bezeichnung.

"Magst du auch das Vize-Kommando übernehmen?", fragte Kamira
als nächstes.

Damit überraschte ihn Kamira noch mehr. "Was ist damit gemeint?" Er
hatte eine ungefähre Vorstellung, aber wollte es genauer wissen.

"Das Kommando über die Schattenmuräne übernimmt Kapitänin Sindra.", leitete
Kamira ein.

"Ein Landsleut.", sagte Smjer.

Viele Nixen standen dem Hybridprojekt
kritisch gegenüber. Mit Landsleuten -- oder Fußvolk, wie sie es oft
nannten -- zusammenzuarbeiten, hatte sich eher selten als gute
Idee herausgestellt. Das Fußvolk war sich seiner Übermächtigkeit allein
durch ihre Menge, aber auch durch die vielen
gelebten Kulturen des Besitzens, nur
allzu bewusst. Nicht jedes Landsvolk lebte so eine Kultur, aber
die Ausnahmen waren nicht ohne Grund unscheinbarer. Und es gab
auch innerhalb der Landbevölkerung starke Uneinigkeiten und
Auseinandersetzungen. Politik war komplex. Es war nicht so, dass
sich Nixen zwangsläufig daraus heraushalten wollten. Aber
Vereinigungen hätten bereits Kompromisse dargestellt, die sie
nie hätten guten Gewissens eingehen wollen. Und das Bewusstsein
der Dominanz war auch bei den meisten Landsleuten da, die sich gegen
das eigene System stellten und sich für Nixen interessierten. Es
war oft ein wissenschaftliches Alles-Wissen-Wollen-Interesse
und ein Beschützeninstinkt dabei, die in der Beziehung überwogen.

Das Hybridprojekt war aber ein politisches Projekt, das für
alle Beteiligten, auch für die Landsleute, über
eine Neugierde hinausging. Die Landsleute, die sich hierfür
gefunden hatten, waren von vornherein Leute, denen es um
die Sache ging: die Verteidigung des Kontinents Grenlannd. Die
Verteidigung der Natur und Luft-, Land- und Wasservölker dort. Ohne
den Kontinent je zu besuchen, einfach mit dem Wissen, dass es
dort eine schützenswerte Welt gab.

Die Landsleute verzichteten außerdem mit dem Beitritt des
Projekts darauf, je wieder an Land zu gehen. Sie waren eher
aus Sicherheitsgründen zu dieser Übereinkunft gekommen, weil
auf diese Weise viel schwieriger verratende Informationen an Land getragen werden
konnten, aber es hatte den Nebeneffekt, dass zwischen dem
Fußvolk-Anteil der Crew und den Nixen ein ausgeglicheneres
Machtverhältnis bestehen würde.

Smjer konnte nicht leugnen, dass er sich gewünscht hatte, Teil einer
interkulturellen Crew zu werden, die
Landsleute kennen zu lernen und herauszufinden, wie
es war, mit ihnen zu arbeiten. Er
fragte sich, welche Motive sie hatten, sich für ein Leben vollständig
auf See zu entscheiden.

"Entschuldige, ich habe ab meinem Einwurf nicht mehr zugehört, weil
ich in Gedanken abgedriftet bin.", sagte Smjer.

Kamira blickte ihn milde lächelnd an. "Das habe ich bemerkt.", sagte
er. "Ich habe relativ schnell aufgehört zu reden."

"Bist du sicher, dass ich mich für ein Vize-Kommando eigne?", fragte
Smjer.

"Eigentlich schon.", antwortete Kamira zuversichtlich. "Ich kenne dich nun schon
sehr lange."

Das stimmte. Kamira hatte eine Ausbildung in psychologischem Beistand
und Barriereabbau und hatte Smjer lange Zeit unterstützt, mit sich selbst
besser zurechtzukommen. Auch das wäre schön: Die Gespräche taten
ihm gut. Wenn er mit Kamira zur See führe, könnten sie sie fortführen.

"Du driftest häufiger in Gedanken ab, ja.", fuhr Kamira fort. "Aber
während der Bauarbeiten warst du immer voll
bei der Sache mit einem sehr beeindruckendem
Dauerfokus. Du kannst dich über einen ganzen Tag am Stück konzentrieren, wenn
es nötig ist."

Smjer nickte. "Das stimmt auch wieder. Warum bin ich gerade
abgedriftet?"

"Weil du Zeit hast, dir über eine komplexe Frage Gedanken zu
machen.", sagte Kamira. "Ich glaube, wenn es nötig ist, dass du das
Kommando der Schattenmuräne übernimmst, dann wüsstest du, dass
zeitnahe Entscheidungen wichtig sind. Ich glaube, das könntest
du sehr gut. Und niemand kennt die Schattenmuräne so gut wie du."

Smjer grinste und nickte. "Aye.", sagte er. Dann runzelte
er die Stirn. "Sagen wir das jetzt so?"

Kamira lachte warm. "Vielleicht."

"Ich kann das mit dem Vize-Kommando wohl machen, wenn wir einmal
ordentlich darüber reden, was das genau heißt. Was dann
alles in meinen Aufgabenbereich fällt, und was
Leute von alleine machen.", beschloss er.

"Natürlich.", sagte Kamira. "Wir werden eine Jungfernfahrt und
ein paar Testfahrten machen. Erst nur wir Nixen und dann auch mit
der ganzen Crew."

"Aha.", brummte Smjer, als ihm etwas klarwerden wollte. "Die
Notmanöver werden den Landsleuten nicht verraten, bis sie gebraucht
werden?"

Kamira lächelte und nickte.
