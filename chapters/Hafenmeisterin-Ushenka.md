\BefehlVorKapitelDFDM{Ushenka-n.png}{}{Unerfüllter Kinderwunsch, Schwangerschaft erwähnt, Pädophilie (keine Gewalt), Tod eines Elters.}

Hafenmeisterin
==============

\Beitext{Ushenka}

Ushenka war Hafenmeister. Oder vielleicht Hafenmeisterin. Auf
dem Papier war sie Hafenmeister. Aber für sich selbst und Daheim
nannte sie sich Hafenmeisterin und das nicht ohne ein
Gefühl von Genugtuung.

Ushenka war eine mollige Frau Mitte fünfzig mit glücklicherweise
relativ wenig Oberweite, ein Elb, sodass sie ganz gut zum
Stereotyp eines minzteraner Hafenmeisters passte. Schiffe liefen
ein, liefen aus, und informierten sie jeweils über ihre Pläne. Größere
Schiffe reservierten bei ihr Plätze und Arbeitskräfte, wenn sie ein-
oder ausliefen. Es war eine Verwaltungs- und Schreibarbeit direkt
an der Küste eines der schönsten Meere vor Maerdha. Und eine
Arbeit mit viel Getratsche.

Ushenka hatte das Gefühl, sie sollte sich eigentlich nicht
über ihr Leben beschweren. Es war interessant, sie lebte in einem
gewaltfreien, sozialen Umfeld, und sie hatte es zu etwas gebracht,
wovon sie als Jugendliche geträumt hatte, ohne je damit zu
rechnen, dass die Träume wahr werden könnten. Trotzdem
hatte sich ihr Leben ganz anders entwickelt, als sie es sich
heute wünschte, und sie bereute es fast jeden Tag ein bisschen. Ein
wenig zumindest. Und hoffte manchmal, dass es noch nicht zu
spät wäre.

Ushenka hätte eigentlich gern eine Familie gegründet und
Kinder groß gezogen. Sie hätte gern Kinder geboren. Aber
es wäre dann doch aufgefallen, wenn der Hafenmeister schwanger
geworden wäre. Das war nicht der einzige Grund, der dem
Wunsch im Weg stand: Sie führte außerdem eine Zweckehe. Es
war eine gute Zweckehe, die sie sich wohlüberlegt ausgesucht
hatten. Sie würde es wieder so machen. Selbst, wenn das eben
hieße, dass der Kinderwunsch unerfüllt bliebe. Mit ihrem
Mann wollte sie keine Kinder kriegen und er auch
mit ihr nicht. Er hatte ihr Mal gestanden, dass er sich
zu Kindern erotisch hingezogen fühlte, und deshalb den Kontakt zu
Kindern so vollständig mied, wie es eben ging. Es ging gut. Er
arbeitete als Forscher an der Universität Minzter. Universitäten
waren eher selten ein Ort, wo sich Kinder aufhielten.

Ushenka hatte ein Kind groß gezogen, wenn sie das so bezeichnen
durfte. Nicht von klein auf und es hatte weitere Besonderheiten
gegeben. Als sie vor zwanzig Jahren etwas abseits der Stadt einen
ausgiebigen Strandspaziergang gemacht hatte, bei dem sie
wie immer in dieser schmalen, unscheinbaren Bucht vorbeigeschaut
hatte, hatte sie
ein Nixenkind gefunden. Das war noch vor der
ganzen Sache mit der Ehe und der Arbeit im Hafen gewesen.

Minzter und Umgebung war kein Ort, an dem
sich Nixen normalerweise lang aufhielten. Aber das Nixenkind war
trotzdem nicht die erste Nixe, der Ushenka begegnet war. Ein Jahr zuvor
war sie schon einer begegnet. Sie war verletzt gewesen. Das war
der einzige Grund gewesen, warum sie sich ihr offenbart hatte. Sie
wäre noch zwei Tage angestrengtes Schwimmen von ihrem
Ziel entfernt gewesen und hatte gewusst, dass sie es nicht
mehr schaffen würde.

Ushenka war mehrmals täglich zu ihr gekommen, um sie zu
pflegen und hatte ihr, wie die Nixe es gewünscht hatte,
ihr Wort gegeben, niemals davon zu
erzählen. Sie hatte es gehalten. Sie hatte diese Geschichte
wirklich niemandem erzählt.

Sie hatten sich gegenseitig
übereinander ausgefragt, was mehr schlecht als recht gegangen
war. Die Nixe sprach Ilderin nur bruchstückhaft. Ushenka
selbst war nicht sehr sprachtalentiert. Obwohl Minzter
als Stadt der Sprachen galt.

Die Nixe hatte sich in besagter Bucht versteckt, die Ushenka seitdem
bei jedem längeren Spaziergang besuchte, um zu schauen, ob sie
vielleicht wieder da wäre. Was nie passierte, aber dafür hatte
sie dann dort Marah gefunden. Ein etwa elfjähriges Kind. Allein
mit einem Segelboot, das einen Sturmschaden
hatte und dadurch segeluntauglich war.

Als Ushenka Marah nach ihren Eltern gefragt hatte, hatte
Marah geantwortet: "Wahrscheinlich tot." Und hatte dann
hinzugefügt: "Das eine. Das andere habe ich nicht gekannt." Marah
sprach einwandfreies Ilderin.

Ushenka hatte sich damals über so vieles gewundert. Zuallererst
darüber, dass ein elfjähriges Kind vielleicht nicht
so abgeklärt sein sollte. Es hörte bei der trockenen
Feststellung über den Tod des Elternteils
nicht auf. Marah wusste auch genau, welches
Flickzeug sie brauchte, dass sie um nichts in der Welt
zu ihrer Restverwandtschaft in den Norden, sondern zu einer
Organisation weit in den Süden wollte, und dass sie
die Welt verändern würde. Ushenka hatte sich darüber gewundert, dass
Marah von ihren Eltern wie über eine Sache gesprochen hatte, aber
hatte später gelernt, dass das gar nicht der Fall war. Marah
hatte ihnen lediglich kein Geschlecht zugewiesen und
benutzte für sie beide als Bezeichnung 'das Elter'.

Und auch, dass Marah besonders abgeklärt gewesen wäre, stellte
sich im Nachhinein als falsch heraus, aber für die
Erkenntnis hatte Ushenka länger gebraucht.

Ushenka besuchte Marah oft, brachte ihr das Flickzeug und Vorräte. Dann
war Marah mutig genug, Ushenka zu erzählen, dass ihr
anderes Elter ein Lobbud in Minzter war, und ob sie es
ausfindig machen könnte.

Das war keine leichte Sache, wenn Auflage war, dass
Ushenka möglichst nichts über Nixen Preis geben sollte. Ihre
Strategie war gewesen, in Gegenwart der wenigen Lobbuds, die
sie in Minzter fand, in Schenken, Kneipen, Restaurants, beim
Spazieren, falsche Geschichten über Nixen zu streuen. Alle
kannten irgendwelche Sagen oder Geschichten über Nixen. Ushenka
wurde viel korrigiert, aber nur selten gewannen die Geschichten
dadurch an Wahrheitsgehalt. Ihre Hoffnung war gewesen, dass
ein Lobbud, der mit einer Nixe ein Kind bekommen hätte, einiges
wissen würde und sie vielleicht auffällig richtig
korrigieren würde. Eine
vage Hoffnung. Gegeben, dass sich besagter Lobbud dann auch
einfach nicht einmischen könnte, oder Marahs Elter eine
zurückgezogene Person sein könnte.

Marahs Elter war kein zurückgezogener Lobbud. Sie erkannte
ihn daran, dass er die Geschichten ins noch unermesslichere
verfälschte, aber auf eine Art, in der sie sich irgendwie
durch viel kaum verständlichen Subtext näher kamen. Sie
sprachen hinterher privat, brauchten eine Weile, bis sie
sich gegenseitig ausreichend vertrauten. Aber das Ergebnis brach Ushenka
stellvertretend für Marah das Herz: Marahs Elter wollte Marah
nicht kennenlernen, wollte sich nicht kümmern. Es meinte, die
politische Sache, für die sich Marahs Nixen-Elter eingesetzt hätte,
wäre eigentlich eine sinnvolle, aber auch lebensgefährlich. Und
es habe seinen Anteil daran nun geleistet, würde gern diese
Gelegenheit nutzen, um auszusteigen.

Marahs Eltern hatten sich geliebt. Zumindest behauptete
der Lobbud das. Er hatte Marah selbst nie gesehen. Und
meinte, es wäre für ihn und das Kind besser, wenn das
so bliebe.

Und so war es gekommen, dass Ushenka nicht nur ein Kind
betreut hatte, sondern auch in diese ganze Grenlannd-Politik
verwickelt worden war. Das war eine aufregende und sehr
anstrengende Zeit gewesen.

Sie hatte sich gefragt, ob es für das Kind eine leichtere
Botschaft gewesen wäre, wenn auch ihr anderes Elter verstorben
wäre. Aber Ushenka wollte ehrlich mit dem Kind sein. Es
waren schreckliche Nachrichten, aber es wäre nicht in
Ordnung gewesen, das Kind zu belügen. Auch auf diese Nachricht
hatte Marah abgeklärt reagiert, als wäre sie fünfzig und
halbwegs abgestumpft. Aber wenn Ushenka sich der Bucht
näherte und Marah noch nicht wusste, dass sie gleich
da wäre, hörte Ushenka das Kind oft weinen.

Es stellte sich heraus, dass Marah eigentlich große Angst vor
Elben hatte, und das Vertrauen langsam und behutsam aufgebaut
werden musste. Und als es da war, entwickelten sie Pläne, wie
sie gemeinsam nach Grenlannd reisen könnten. Marah hätte es
vielleicht nie vorgeschlagen, wenn es Ushenka darum gegangen
wäre, dort hinzugelangen. Aber Ushenka hatte eben nur eines
im Sinn gehabt: Für Marah da zu sein, dieses Kind nicht
bei dem Vorhaben allein zu lassen, den Ozean zu überqueren.

Finanziert wurde die kleine Yacht, mit der sie dorthin
segelten, von ihrem heutigen Mann, Arym. Sie hatte damals mit
so vielen Leuten möglichst unauffällig gesprochen, um Marah den Wunsch zu
erfüllen, über eineinhalb Jahre hinweg, bis sie diesen
Mann kennen gelernt hatte, ebenfalls ein Lobbud, der
ein uneigennütziges Interesse und etwas Geld
übrig hatte, das Vorhaben zu
unterstützen. Marah hatte darauf bestanden, ihn auch
kennen zu lernen, um selbst herauszufinden, ob sie
ihm genug vertraute.

Die Überfahrt war Ushenkas bisher größtes Abenteuer
gewesen. Marah kannte sich mit Segeln aus, aber es war
keine für Nixen typische, gebrauchte Segelyacht gewesen, die sie
in desolatem Zustand erworben und dann aufgerüstet
und repariert hatten. Ushenka hatte zusammen mit Arym
so viel über Seefahrt und Segeln gelernt, wie das für
Laien eben in kurzer Zeit möglich war. Schließlich hatten sie testweise
drei Segeltörns zu dritt gemacht, jeweils über ein paar
Tage, bevor Ushenka mit Marah dann die Überfahrt gewagt
hatte. Niemand an Land wusste Bescheid darüber, wo es
langgehen sollte, außer Arym. Das hatte Marah immer wieder
verlangt. Sonst, so wusste Ushenka nun, wären sie vielleicht
nicht in Grenlannd angekommen. Obwohl: Damals war das
Informationssystem noch nicht so gut. Ushenka war
vielleicht heute die Person, die
die meiste Information sammelte und weitergab.

Sie war damals ein knappes halbes Jahr bei den
Nixen vor Grenlannd geblieben, die versuchten, die Erforschung
des Kontinents Grenlannd durch andere Völker zu verhindern. Sie
erklärten sehr genau, warum. Und wie wichtig es ihnen war. Sie
hatten eigentlich nie vorgehabt, die Landvölker zu schädigen oder
sich allzu sehr einzumischen. Aber sie hatten bereits zwei Forschungsschiffe
versenkt. Die erste Nixe, die sie in der Bucht gefunden hatte, die sie nun hier
wieder traf, war bei dem einen der Vorfälle
verletzt worden, und Marahs Elter beim anderen ums
Leben gekommen. Dass es nicht mehr lebte, schien nun außer
Zweifel, sonst hätte es sich wohl in Grenlannd in der Werft, die
Marah angepeilt hatte, eingefunden.

Ushenka hätte damit gerechnet, Marah hier in die Hände
einer oder mehrerer Nixen abzugeben, die sich dann um das
Kind kümmern würden, aber das war nicht der Fall. Stattdessen
schmiedeten sie Pläne, wie Ushenka eine gut einsetzbare Informantin
werden könnte. Über Arym hatte Ushenka Verbindung zur
Universität, die in die Genehmigung von Forschungsreisen verwickelt
werden würde. Da Segeln ihrer beider gemeinsame Freizeitaktivität
war, konnten sie im Hafen unauffällig tratschen. Es
war diese Zeit, in der Ushenka ihre Kindheitsträume, irgendwann
als Mann verkleidet irgendwelche Arbeiten auszuführen, die
sonst eher nur Männer ausführten, anfing, für realistisch zu
halten.

Die Nixen versuchten, Überfälle auf die
Forschungsschiffe schon frühzeitiger zu planen, schon
bevor sie in die Nähe des Kontinents kommen würden. Und sie
wollten die Überfälle für beide Seiten weniger lebensgefährdend
gestalten. Damals hatte noch völlig in den Sternen gestanden, wie
das gehen sollte. Glücklicherweise waren damals auch die
Versuche noch weniger gewesen.

Sie kehrte zusammen mit Marah von Grenlannd über lange Strecken begleitet von
einem kleineren Nixenschiff zurück, das mit extra kleiner Segelfläche
fuhr, um Marah und ihr nicht davonzufahren -- denn Nixenschiffe waren
unsagbar viel schneller, als alles, was Landsvolk baute --, und
stürzte sich in ein Leben, das sie sich in ihren kühnsten Kinderträumen
erträumt hatte: Sie führte ein Doppel- oder Dreifachleben, sammelte
Informationen, schickte Briefe an Nixen und veränderte die Welt. Und
betreute ein Kind, Marah. Es
war nicht einfach, eine Nixe versteckt zu betreuen. Eine Badewanne
eignete sich dafür jedenfalls nicht. Marah pendelte mit ihrem
kleinen Segelboot, nun, da Briefverkehr hergestellt war, gelegentlich
mit Ladungen an Reparaturzeug, Proviant oder
sonst etwas zu den Schiffen der Nixen, blieb manchmal ein paar
Tage an Bord und kam dann wieder. Ushenka und sie saßen viele
Abende zusammen in der kleinen Bucht. Ushenka kümmerte sich
um Marahs Bildung selbst, speiste mit ihr, hörte zu und
erfand mit ihr Geschichten. Ob das alles wirklich kindgerecht
gewesen war, bezweifelte Ushenka. Aber manchmal war es schwer, in
einer schlimmen Welt oder unter schlimmen Voraussetzungen die
Dinge so zu gestalten, dass sie gut waren. Manchmal mussten es
die besten Kompromisse sein, auch wenn diese immer noch schlimm
waren.

Zwanzig Jahre waren seit dem vergangen. Inzwischen hatte die
kleine Gruppe von Nixen von damals sich ganz schön vergrößert
und machte mit der Flotte der Maare die See für Forschungsschiffe
unsicher. Nun, nicht furchtbar unsicher. Aber das wussten die
Forschungsgruppen zum großen Teil nicht. Ushenka selbst war
für viele der Horrorgeschichten verantwortlich, die über die Flotte
der Maare in Umlauf waren. Als Hafenmeister tratsche sie natürlich
gern, erzählte davon, was irgendwelche Fischerleute angeblich
gesehen hätten. Und lauschte dabei darauf, was sie tatsächlich
sahen. Mehr als das, sie konnte sehr genaue Maße der Schiffe
unauffällig in Erfahrung bringen. Die Flotte der Maare
wusste so über Material, Besatzung, Ladung und geplante Routen Bescheid.

Marah wohnte nicht mehr in der Bucht. Sie hatte auf der
Schattenmuräne angeheuert. Und sich in die Kapitänin
verliebt. Es war ein merkwürdiges Gefühl. Nicht, weil
Ushenka irgendetwas dagegen einzuwenden gehabt hätte. Sondern
weil Marah nun ihr eigenes Leben aufbaute, und
sie eine weniger zentrale Rolle darin spielte. Die
Veränderung beschäftigte sie, aber sie war natürlich auch
gut. Die meisten Kinder zogen eben irgendwann aus.

Marah brachte immer noch kleinere Ladungen
von hier zu den Schiffen oder
transportierte sie zwischen den Schiffen hin und her. Ushenka
vermisste sie viel. Sie schrieben sich allerdings Briefe. Und
manchmal trafen sie sich dann doch wieder in der Bucht.

Ushenka stellte das Hafenbuch zurück ins Regal, verließ
das Hafenhäuschen und schloss die Tür ab. Für heute war
alles getan. Die Bücher waren so geführt, wie sie sollten, verrieten
nichts darüber, dass der Hafenmeister die Stelle aus einem
bestimmten Grund angetreten war. Die Wanten der Schiffe pfiffen im Wind
und die Fallen schlugen gegen die
Masten. Es klingelte und schwappte immer irgendetwas im Hafen. Ushenka mochte
das Geräusch. Es klang nach Reisen. Sie wollte vielleicht
auch doch irgendwann Mal wieder reisen. Als sie
den Weg nach Hause einschlug, bettelte da wieder dieses
Kind. Sie hatte ihm in der vergangenen Woche jeden Tag etwas
zugesteckt. Aber dieses Mal reichte Ushenka das nicht. Vielleicht, weil
sie heute so viel an Marah hatte denken müssen.

Sie hockte sich neben das Kind. "Hast du Eltern?", fragte sie.
