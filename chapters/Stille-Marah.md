\BefehlVorKapitelDFDM{Marah-n.png}{Marah wurde von Ushenka großgezogen und hat die Flotte der Maare quasi mitgegründet. Sie ist verliebt in die Kapitänin.}{Tierleid, Shaming von Essgewohnheiten, Mord an mehreren Personen, Trauer, Hunger.}

Stille
======

\Beitext{Marah}

Heute Nacht sangen sie nicht. Die Flaute demotivierte
Marah extrem. Schlecht gelaunt lehnte sie im Mastkorb
neben Jentel am Mast, ihre
Schulter an saine geschmiegt.

"Ich mag Amira.", teilte as ihr mit.

Das versprach ja wieder, interessant zu werden. "Du
magst ein Assassinan. Einen Menschen."

"Und du hast furchtbar schlechte Laune.", teilte
Jentel ihr mit.

So wie as klang, hatte as eine Weile mit sich gerungen, ob
as wirklich das Offensichtliche aussprechen sollte. Marah
seufzte. "Ja, sehr. Hättest du lieber heute deine Ruhe vor mir?"

Jentel war eine Person, die keine Probleme gehabt hätte,
Marah einfach ins Gesicht zu sagen, wenn dies der Fall
wäre. Damit, dass as eine Weile nicht reagierte, überraschte
as sie schon wieder. "Ich kann wirklich nicht leugnen, dass
deine Gesellschaft heute eher unspaßig ist.", gab as schließlich
zu. "Aber morgen kommt Wind auf, wenn Kamira das Wetter, wie
immer, einigermaßen richtig vorhersagt, und dann bist du wahrscheinlich
wieder weg. Und dann ertrage ich dich heute lieber noch einmal
so, als für längere Zeit gar nicht."

Marah musste unweigerlich grinsen. Sie kannte wirklich
keine andere Person, die ihr so etwas so direkt ins
Gesicht gesagt hätte. Nun ja, Sindra war auch relativ direkt, aber
Sindra war nie genervt. "Wind.", sagte sie.

"Das heitert dich auf, was?", fragte Jentel mit sarkastischem
Unterton.

"Sei doch froh!", spaßte Marah zurück.

Jentel blies ihr ins Gesicht. Marah wandte sich lachend und
sich scherzhaft beschwerend ab. "Woher weißt du, dass ich
nur auf Wind warte?"

"Ich weiß es nicht sicher.", antwortete Jentel. "Aber Amira ist
sicher nicht ohne Grund an Bord. Solche Veränderungen sind für
dich oft Grund zu verschwinden. Vielleicht
musst du Kleider holen, wie damals für Kanta. Oder Messernachschub."

"Messernachschub, weil sie ein Assassinan ist?", fragte Marah
belustigt.

Jentel nickte. Vielleicht war das Grinsen ein selbstgefälliges darüber, dass
as Marah tatsächlich aus ihrem Schlechte-Laune-Loch heraus hatte
holen können. "Assassinan ist sie also. Dafür brauchte sie die
Endung '-an'. Wir hatten über Neo-Endungen in Sprachen gesprochen."

"Oh, mir war nicht bewusst, dass noch nicht alle wissen, dass
sie Assassinan ist.", fiel Marah auf. Ihr wurde ein bisschen heiß
dabei. Sie plauderte selten Dinge aus, aber wenn es passierte, hatte
sie jedes Mal ein schlechtes Gewissen.

"Ich kriege hier oben nicht alles mit. Vielleicht war das
bekannt.", versuchte Jentel sie zu beruhigen.

"Warum magst du sie?" Das war die spannende Frage von vorhin. Und
vielleicht hatte sie, jetzt da ihre Laune angenehmer war, bessere
Chancen auf Antworten.

Jentel grinste. "Sie ist sehr flexibel im Kopf.", sagte as. "Wir
haben uns über Geschlechter unterhalten. Sie ist da nicht so
festgefahren. Sie mag Sprache verändern, damit sie besser zu
Bedürfnissen passt und weniger weh tut. Sie ist sehr
rücksichtsvoll." Jentel machte eine kurze Betonungspause und
grinste noch breiter. "Und sie hat Rash ganz schön den Kopf
verdreht."

"Warum interessiert dich das?", fragte Marah skeptisch. Die
Gedanken bis zu diesem letzten verstand sie. Aber Jentel war
normalerweise keine Person, die sich für Romantikdinge
interessierte.

"Ich gönne Rash, glücklich zu sein.", sagte Jentel schlicht.

Das stimmte Marah plötzlich nachdenklich. "Ich eigentlich
auch." Sie erinnerte sich zurück daran, wie Rash mit der Ziege
umgegangen war. Niemand konnte leugnen, dass Rash sich um Aga
am meisten gekümmert hatte. Aber auf der anderen Seite aß Rash
totes Tier. Jentel hatte ihr mal erklärt, dass Rash ein gesundheitliches
Problem hatte, wenn Rash keines aß. Für Marah war das
schwer vorstellbar. Sie hatte es akzeptieren wollen, aber es fiel
ihr schwer, das zu tun. Vielleicht wäre es ihr weniger schwer
gefallen, wenn Rash nicht auch Elb gewesen wäre. Elben schickten
die meisten Forschungsschiffe. Gerade Elben waren außerdem in Sagen
als Volk dargestellt, das auf das Töten von Lebewesen, um sie
zu essen, weitgehend verzichtete. Erst durch den aufkeimenden
kulturellen Austausch unter den Völkern hätte sich das
verändert, hieß es.

Marah sagte nie etwas dazu, kämpfte weiter gegen ihr Unbehagen
an. Vielleicht sollte sie irgendwann Mal mit Rash darüber reden, um
sich besser da hineinfühlen zu können. Und dann wiederum wollte
sie Rash auch nicht in irgendwelche Ecken drängen, wenn es für Rash
eben wirklich nicht anders ginge.

Sindra mochte Rash auch, fiel Marah ein. Ja, Marah gönnte Elben
im Allgemeinen wenig, aber Rash gehörte zu den wenigen
Ausnahmen, denen Marah Gutes wünschte, sogar
wenn das, was sie dabei gönnte, glückliche Gefühle
für einen Menschen waren. Sie nickte. "Danke, ich habe Mal wieder
ein wenig reflektiert.", sagte sie. Und hatte das Gefühl, dass es
da noch viel mehr zu reflektieren gab. Nicht mehr heute.

"Ich gönne dir auch, glücklich zu sein.", sagte Jentel. "Bist
du es?"

Marah nickte. "Schon, ja." Und dann dachte sie an den Wind. "Es
kommen gefährliche Zeiten auf uns zu. Befürchte ich."

Jentel nickte ernst. As hätte einen Witz darüber machen können, dass
ein Leben, das aus Überfällen auf Schiffe bestand, die etwas dagegen
hatten, überfallen zu werden, ohnehin nicht dafür vorgesehen war,
ungefährlich zu sein. Aber as ließ es bleiben. "Ein Assassinan an
Bord, auch, wenn es niemanden bisher getötet hat, sendet
da eine gewisse Botschaft. Ich dachte mir das schon."

Von der Schattenscholle war nun seit einer Woche keine Nachricht
mehr eingetroffen. Sie befürchteten, dass auf jener ähnliches passiert
war, wie bei ihnen. Im besten Fall. Vielleicht ging es der Crew gut
und sie hatten lediglich Bedenken, dass der Schriftverkehr
nicht mehr sicher wäre, und ihn deshalb vorübergehend eingestellt. Aber
es konnte auch viel Schlimmeres bedeuten.

Sobald Wind wäre, da hatte Jentel recht, würde Marah versuchen, die
Schattenscholle zu erreichen. Wobei sie natürlich nach zehn Tagen
ohne Nachricht gefühlt überall sein könnte.

Marah hatte mit Smjer und Sindra in den vergangenen Tagen
darüber gebrütet, an welchen Orten sie am ehesten sein könnte. Sie
kannten ihre Routen und Ziele einigermaßen. Sie wussten auch
über die Forschungsschiffe in der süd-ost-maerdhischen See
Bescheid. Es war eine gefährliche Region, weil von der
Küstenlinie Forschungsschiffe von gleich drei Völkern in
verschieden regelmäßigen Abständen versuchten, die Schattenscholle
zu passieren. Das Mandulin-Volk war finanziell nicht gut aufgestellt
und hatte immerhin erst einen Versuch gewagt. Mit einem deshalb sehr
kleinen, nicht sonderlich hochseetauglichen
Schiff, das besonders schwer auszurauben gewesen war, weil die
Bilge kaum einen Meter gemessen hatte. Und weil sie im Mandulin-Volk
kein Informantan hatten, das sie hätte vorher über Details der
Crew und des Schiffs in Kenntnis setzen können. Sie hatten
Informantanen in den benachbarten Ländern und damit gerechnet,
dass die Pläne des Mandulin-Volks nicht an jenen vorbeigehen
würden, weil das Mandulin-Volk nicht selten etwa vom benachbarten
Zarenreich der Zwerge Unterstützung erbat und bekam, aber in
diesem Fall hatte es interessanterweise darauf verzichtet. Die wenigen Informationen,
die sie zu diesem Forschungsschiff erhalten hatten, waren dazu
über mehrere Breitengrade nach Norden geradeso rechtzeitig im
minzteraner Hafen bei Ushenka angekommen.

Eher aus dem Westen hatte es nun drei Mal das Menschenvolk des Königreichs
Namberg versucht. Das Königreich lag eigentlich im Gebiet der
Schattenmuräne, kaum südlich der Bantine, aber das Königreich nutzte
als Ausgangspunkt für weitere Seereisen wegen der Meeresströmungen einen
Partnerhafen im westlichsten Zipfel des süd-ost-maerdhischen Zarenreichs der Zwerge. Die
Zarin war nicht glücklich darüber, aber saß genügend unter Druck, es
zu akzeptieren.

Während über lange Jahre Minzter mit der Anzahl an Versuchen vorne
gelegen hatte, Grenlannd auf dem Wasserweg zu erreichen, -- wie auch
sonst --, kamen seit Kurzem die meisten Schiffe aus eben dem süd-ost-maerdhischen Zarenreich
der Zwerge unter Zarin Katjenka. Die Schattencrew nannte es oft bloß das Zarenreich der
Zwerge ohne den Zusatz süd-ost-maerdhisch, was eigentlich nicht
korrekt war. Es gab noch andere zwergische Zarenreiche, aber diese
lagen binnen, in Gebirgen, waren für die Schattencrew uninteressant. Die
Besonderheit jener Schiffe aus dem Zarenreich war nicht nur, dass die
Zwerge auf Geschwindigkeit und Menge setzten, um die Schattenscholle
zu passieren, was immerhin ein geschickter Ansatz war, sondern auch, dass
die Zwerge versuchten, im Auftrag der Zarin Katjenka mit der Flotte
der Maare zu verhandeln. Die Maare hatten wenig Interesse zu
verhandeln, aber nutzten die Gesprächsbereitschaft, um der Zarin
ihre Motive zu übermitteln, während sie hofften, dass der Gruseleffekt
darunter nicht litt.

---

Ein paar Stunden vor Morgengrauen verzog sich Marah ins Nixendeck, um
zu schlafen. Jentel würde saine Nachtschicht noch zu Ende machen, irgendwann
mit Janasz frühstücken und dann nachkommen. Aber als as dann kam, schlief
Marah schon fest. Und als sie wieder aufwachte, wehte tatsächlich
endlich wieder Wind.

Marah tauchte, um sich umzuziehen, zu ihrer Kleiderkiste ins Lenzdeck, das
auch oft Tauchdeck genannt wurde, weil es meist unter Wasser
stand. Es war so praktisch, Kleidung in Kisten mit
Löchern unter Wasser auf einem Schiff zu lagern, das schaukelte, und sie
dabei auswusch. Unpraktisch vielleicht für Leute, die nicht gern nasse
Kleidung trugen. Aber das Problem hatte Marah ja nicht.

Sie wechselte ihr Nachtkleid gegen Reisekleidung. Jene hatte allerlei Taschen, in
denen ein Kompass steckte, ein kleines Fernglas, ein Marlspieker
zum Knoten lösen oder zum Spleißen von Seilen -- die Arbeit, knotenfrei Seile
zu verbinden, hieß spleißen --, scharfe Messer und weiteres klassisches Bootswerkzeug. Sie
hatte die Kiste kaum geschlossen, als sie eine vertraute Berührung auf ihrem
Rücken wahrnahm. Endlich! Der Briefwels, der sich auf ihrem Körper niedergelassen
hatte, drehte sich streichelnd um sie herum, als sie sich auf den Rücken
drehte. Sie strich dem Tier vorsichtig über die Flanken. Es schloss dabei
genießend die Augen. Marah inspizierte den Wels gründlich auf mögliche
Verletzungen, bevor sie endlich die nah am Körper befestigten, dünnen
Taschen öffnete und einen Brief herauszog. Dann würde sie also nicht sofort
abfahren, sondern erst ein weiteres Mal mit Smjer und Sindra sprechen. Es war der Briefwels,
der zwischen der Schattenmuräne und der Schattenscholle pendelte. Er war vielleicht
ein Drittel so groß wie sie. Er war unverletzt, aber forderte noch eine längere
Streicheleinheit ein.

"Kommst du mit?", fragte Marah auf Sirenu. "Ich fahre heute wohl zur
Schattenscholle."

Für die Schattenscholle hatten sie ein eigenes Wort in der Sprache
entwickelt. Wie gar nicht mal wenige Tiere verstanden Briefwelse
sehr wohl verschiedene komplexe Sprachen mehr oder weniger gut, aber sprachen sie
selbst nicht. Marah wusste nie genau, wie viel sie verstanden. Aber
sie war sich relativ sicher, wenn der Briefwels mitkommen wollte, dass
sie ihn später immer noch hier vorfinden würde und dass er dann freiwillig
in ihr Segelboot schwimmen würde. Es wäre gut, wenn er mitkäme. Dann
würde er vielleicht Hilfe holen, wenn ihr etwas passieren sollte.

Sie streichelte den Wels, bis sie Luft holen musste, und dann noch
einmal, weil er aufgeregt um sie herumschwamm und vorsichtig an
ihrer Haut nuckelte. Das war auch ein wunderschönes Gefühl. Als
sie endlich genug hatten, zog Marah sich zu Ende an. Das Futtergitter
musste sie dann auch wieder auffüllen, bevor es losginge.

Marah ging vor allem tagsüber nicht gern an Deck. Es trennten sie dort
mehrere Rutschen vom Wasser. Dieses Schiff mochte so hybridfreundlich
wie möglich gebaut sein, aber diese Entfernung von Umgebung, in
der sich Marah natürlich fortbewegte, war ihr unangenehm. Abends zum
Singen war das etwas anderes, aber nun ging es darum, Sindra und Smjer
einzusammeln, um den Brief zu besprechen.

Sie trafen sich dazu im hintersten Ende des schmalen Hecks, wo
sie nur schwer belauscht werden konnten. Die Windrichtung war dafür
ungünstig und der Niedergang lag eher mittig. Viel Raum für Privatgespräche
bot das Schiff nicht. Smjer lag halb auf einem seiner Rollbretter. Wie er
es schaffte, dass die Tampen, mit denen diese am Schiff befestigt waren, nie
beim Rollen im Weg waren, war Marah schleierhaft. Sindra saß einfach in
einem Schneidersitz auf den Planken und nahm den Brief entgegen. Sie entfaltete das
Papier und ihre Brauen hoben sich schneller, als Marah vermutet hätte, dass
irgendjemand lesen könnte. Sie reichte den Brief an sie weiter und sie
verstand: Es standen lediglich Koordinaten darauf. Und eine schlechte
Zeichnung von irgendetwas. Das eine waren Murmeln, vielleicht, und das
andere ein Kochtopf. Sie spürte in den Muskeln ihrer Stirn, dass sie
selbige ebenso runzelte, und reichte den Brief an Smjer weiter, der
wiederum grinsen musste.

"Sie sind nicht so zeichentalentiert.", meinte er trocken. "Ich
schätze, das soll Knollenkohl darstellen."

"Eine der vielen Möglichkeiten, die mir eingefallen sind.", erwiderte
Sindra. "Die Koordinaten sind hilfreich. Ich zeige euch das gleich
auf der Karte. Der Ort fällt mit in das Gebiet, das wir uns überlegt
haben, ist einigermaßen vor Ufer, aber nicht zu dicht."

Dass Sindra das einfach so aus dem Kopf wusste, hätte Marah vielleicht
nicht sehr überraschen sollen. Aber sie bewunderte es trotzdem. "Was
bedeutet das? Dass sie uns keinen Text schreiben.", fragte sie.

"Dass die Crewmitglieder, die eigentlich schreiben könnten, nicht
abkömmlich sind.", sagte Sindra sachlich.

"Nichts Gutes.", fügte Smjer hinzu. "Darf ich einen Vorschlag machen?"

"Selbstverständlich.", lud Sindra ein.

Es war jedes Mal so. Smjer fragte, als wäre nicht klar, dass er
mit dem Vize-Kommando durchaus eines Tages vielleicht die Führung über
das Schiff übernehmen würde, und Sindra versuchte, ihm zu vermitteln, dass
sie ihn als gleichwertig auffasste. Marah hoffte, dass es
nie dazu kommen würde, dass er das Kommando übernehmen würde. Sie
mochte Smjer, aber es würde bedeuten, dass Sindra es eben nicht
mehr tat, was dann verschieden schlimme Gründe haben konnte, aber
Marah fiel keiner ein, der ihr gefallen hätte.

"Die Schattenforelle hat gerade Jungfernfahrt gehabt. Eigentlich
sind noch ein paar Törns vor dem Einsatz vorgesehen. Aber so wie
ich das gerade sehe, braucht die Schattenscholle Hilfe und kann
derzeit wahrscheinlich keine Überfälle fahren.", leitete er ein.
"Ich schlage vor, Marah fährt vor, aber wir planen ein, deren
Revier zu übernehmen, und lassen die Schattenforelle verfrüht unseres
übernehmen."

Sindra nickte ohne zu zögern. "Wir sind uns einig."

"Dann muss ich auf meiner Rückroute einberechnen, wo ihr dann sein
werdet.", murmelte Marah.

Es war absolut möglich. Sie merkte, dass sie traurig klang, aber
das aus völlig anderem Grund, als dass es komplizierter würde.

"Ushenka.", erkannte Sindra richtig. Sie kannten sich
so gut inzwischen.

Marah nickte. "Wir waren bisher immer etwa eine
Tagesreise zu ihr entfernt. Wenn wir
das Revier der Schattenscholle übernehmen, dann werden es eher drei
sein. Und weil ich ja nicht nur Hin- sondern auch Rückreisen rechnen
muss, summiert sich das dann und ich wäre eine Woche weg, wenn ich
Pakete von ihr abhole. Und sie dabei sehe."

Sindra nickte. "Das wäre dann wohl so. Möchtest du dann auf der
Schattenforelle anheuern?"

"Nie!", sagte Marah energisch. "Ich gehöre zur Schattenmuräne. Zu
euch. Zeiten ändern sich eben einfach, und das nicht immer in
einer schönen Weise. Wir führen Krieg. Da passiert so etwas."

---

Marahs Körper fühlte sich immer noch geliebt an, als sie in ihrer
Jolle saß, Kurs südost, einen Briefwels zu ihrer Fluke, im Wasser, das
in der leichten Bootsschale schwamm, und zwei Säcke gekochten Knollenkohl
in den Bordstaschen. Sindra hatte sie zum Abschied
sehr ausgiebig in den Arm genommen. Das spürte sie noch eine
Weile nach.

Marahs Jolle war eine Einhandjolle für
Nixen, eine schnelle, die Marah liebte, als wäre sie ein Teil
von ihr. Es gab eine Schot, mit der sie über einen Flaschenzug das Segel
fierte oder dichtholte, einen Rollsitz, mit dem sie ihr Gewicht dorthin
fuhr, wo es für den Gewichtstrimm hinmusste, ein paar
Gurte zum Einhaken der Fluke, wenn sie sich weiter rauslehnte, und eine
Klapp-Pinnensteuerung mit Teleskoppinnenausleger. Der Wind strich
von der Seite ins Segel, was dafür ausreichte, dass sich die Jolle
etwas aus dem Wasser hob, nur mit den Foils Berührung mit
den Wellen hatte und darüber hinwegraste, sich manchmal von
Wellen schieben ließ. Im Gegensatz zur Schattenmuräne reagierte
die Jolle sofort auf jede Bewegung, jede Veränderung ihrer
Körperhaltung. Jede noch so winzige Böe musste sie mit
ihrem Körpergewicht auffangen, sowie durch leichtes Anpassen
der Segelstellung, für einen kurzen Moment.

Marah war in der Abenddämmerung losgesegelt und orientierte sich
nun, als die Nacht hereinbrach, an den Sternen. Aber auch an den
Strömungen, die sie spürte. Oder an den Walgesängen. Manche Wale
sprachen mit ihr. Das gehörte für viele Nixen dazu. Sie
antwortete. Und sie sang während der Fahrt. Auch das gehörte für
sie dazu. Sie hatte immer gesungen, wenn sie alleine war.

Sie segelte die ganze Nacht hindurch und bis in den Tag
hinein, bis sie ein Schiff erblickte. Kein Forschungsschiff von
dem sie gewusst hätte und kein Schiff ihrer Flotte. Sie
passierte einen Handelsfahrweg. Es war nicht ungewöhnlich, dass hier
irgendein Handelsschiff vorbeikam. Aber sie wollte trotzdem
keinesfalls gesehen werden. Also kenterte sie ihre Jolle
durch. Dazu wählte sie kurzzeitig einen Amwindkurs, einen
Kurs, bei dem der Wind schräg von vorn kam, und bei
dem die Jolle besonders krängte. Als sie so geneigt im
Wasser lag, dass sie sie gerade so mit ausgestrecktem
Körper als Gegengewicht am Umkippen hindern konnte, schmiss
sie sich auf die andere Seite, sodass die Jolle ausreichend
Schwung hatte, um sich auf den Kopf zu drehen. Sie ließ
die Schot locker, als das Segel das Wasser
berührte, damit es beim Durchkentern möglichst wenig Widerstand
bot. Dann war alles still. Das Segel fungierte gleichzeitig
als Treibanker und verhinderte, dass die Jolle zu sehr abdriften
würde. Marah holte das Schwert ein, ein flaches Brett mit stromlinienförmig
scharfen Kanten, das im aufgerichteten Zustand Abdrift verminderte. Sie
würde es später wieder stecken, um es von außen zum Aufrichten als
Hebel zu benutzen, aber gerade war das Schiff von außen
unauffälliger, wenn es eingeholt war.

Sie beobachtete das Handelsschiff mit ihrem Fernglas
durch eine der Lenzluken. Sie hatte zwei Lenzluken im Rumpf, durch
die bei viel Fahrt das Wasser aus der Jolle gesogen wurde. Sie
waren klein, aber ihr Fernglas war es auch und hatte außerdem eine
Spiegeltechnik wie ein Periskop, sodass sie von
unten um die Ecke sehen konnte.

Das Schiff bemerkte sie nicht. So verhielt
es sich zumindest. Aber ein braungrüner, winziger Schiffsrumpf
in der Größe eines kleinen Wals in großer Distanz fiel Landsleuten
meistens nicht auf. Marah nutzte, dass sie ohnehin gekentert war, um die
Gurte im Boot so umzuspannen, sodass sie sich zum Schlafen hinlegen
konnte. Der Briefwels hängte sich dabei so an ihren Körper, dass seine
Kiemen unter Wasser verblieben, aber ihre Hände seinen Rücken
erreichten. So ein verkuschelter Wels. Marah mochte ihn gern.

Sie segelte ungern tagsüber, aber sie tat es doch. Andernfalls
hätte sie wertvolle Zeit verloren. Sie
musste dafür viel öfter auf den Kompass schauen. Sie fühlte die
Sonne unangenehm auf ihrer Haut brennen. Immerhin frischte der
Wind noch einmal auf, so sehr, dass das erste Reff im Segel gerade
so noch nicht notwendig war. Ihre Jolle war nun so schnell, wie
es eben ging. Mehr Wind würde auch nichts mehr bringen. Sie
schnitt die Wellenkämme ab, die sich in feinsten Tröpfchen um sie
herumverteilten. Marah liebte, wie es aussah. Wie es sich
anfühlte und roch.

Aber als sie spät in der Nacht recht sicher war, dass sie die
Koordinaten erreicht hatte, war da nichts und niemand. Wenn die
Personen, die die Koordinaten aufgeschrieben hatten, eigentlich
nicht schreiben konnten, war natürlich möglich, dass sie auch nicht
brauchbar mit Karten umgehen konnten. Aber eigentlich waren
das zwei verschiedene Dinge. Marken anpeilen -- und immerhin war
dies ein Ort, wo eine Insel und eine weit entfernte Landmarke in Sicht
waren --, in einer Karte
Linien einzeichnen, Zahlen am Rand der Karte abmalen und irgendwo
eintragen, war etwas, was jedes Crewmitglied zumindest oberflächlich
lernte. Ein großes Schiff wie die Schattenscholle, sollte eigentlich
nicht zu übersehen sein, wenn sie die Gegend großräumig abfuhr, was
Marah tat. Immer wieder. Bis der Morgen graute.

Sie fragte sich, was sie nun tun sollte. Sie hatte eigentlich
mit Sindra und Smjer abgesprochen, dass sie umkehren würde, wenn sie
bis zum Morgengrauen nichts fand. Sie hatten viele Vorsichtsmaßnahmen
besprochen. Sie hatten mit einkalkuliert, dass es ein Hinterhalt
sein könnte, obwohl das mit einer solchen Nachricht eher unwahrscheinlich
wäre. Trotzdem blieb Marah noch eine Weile in der Gegend. Sie
hatte ihre Gründe, eine vage Hoffnung, nicht besonders
groß, aber sie konnte sie nicht verdrängen.

Sie rollte ihr Segel vorübergehend ein, um sich eine Pause zu
genehmigen und gönnte sich und dem Wels etwas aus ihrem eigenen
Vorrat zu essen. Er nuckelte
an ihrer Fluke und genoss ihre Zuwendung. Aber auch der
Wels verstand, dass etwas nicht stimmte. Schließlich, zu Marahs
Erleichterung, flutschte er über Bord und schwamm in eine
Richtung davon. Briefwelse hatten einen seltsamen Wahrnehmungs- und
Orientierungssinn. Marah hoffte, dass sie mit der Vermutung
richtig lag, dass er von der vermissten Crew etwas mitbekommen
hatte. Sie schmiss ihren angebissenen Knollenkohl zurück in den Segelsack,
beeilte sich, das Segel mehr schlecht als recht wieder zu
hissen und hakte die Öse des Falls -- des Seils, an dem sie
das Segel hochgezogen hatte -- weniger straff gespannt
ein als sonst. Dadurch war das Segel bauchiger und weniger gut getrimmt, aber
das war ihr gerade nicht so wichtig. Sie nahm schon Fahrt in die entsprechende Richtung
auf, während sie ihr Fernglas zückte und den Horizont beobachtete. So
weit musste sie gar nicht gucken. Es war nicht die Schattenscholle. Sondern
das Tauchboot, das an die Wasseroberfläche geschoben worden war.

Marah wurde mit Jubeln willkommen geheißen, als sie neben dem
Tauchboot beidrehte. Ein Manöver, bei dem ein Segelboot einigermaßen
an Ort und Stelle trieb, aber die Segel nicht flatterten. Was mit
zwei Segeln besser ging als mit einem, aber mit bloß einem auch
nicht unmöglich war.

Ein Tauchboot war nicht für Überwasserfahrten gedacht. Die
Crew, oder was davon übrig war, hatte Angst, und wollte
an Land gelangen. Aber sie hatten keine Vorräte mehr. Auch an
Land würden sie nicht so leicht welche bekommen. Mit dem langsameren
Tauchboot wären es vielleicht noch ein oder zwei Tagesreisen gewesen. Aber sie
wussten, dass die Gegend, wo sie anlegen wollten, karge
Wüstenlandschaft wäre.

Myrken war größtenteils für ihre Navigation mit dem Tauchboot
verantwortlich gewesen und sey war nicht wenig stolz darauf, dass
sie hier ungefähr jetzt angekommen waren. Sey hatte abgeschätzt,
dass sie eine Chance haben würden, etwa diese Distanz der
Küste zu erreichen, sobald Marah eintreffen würde, und hatte
gehofft, dass Marah solange in der Gegend bleiben würde, dass
sie sich tatsächlich abpassen würden. Die Erleichterung war
der Crew sehr anzumerken, aber auch die Trauer und die
Angst, die sie noch nicht losgelassen hatte.

"Knollenkohl.", sagte Marah und deutete auf einen der Säcke, die
sie an Bord hatte. "Warum
ausgerechnet Knollenkohl?" Das war nicht unbedingt die Speise, die
es an Bord der Schattenscholle viel gegeben hätte.

"Den konnte ich am besten zeichnen.", sagte Yanil.

Marah musste ob der trockenen, kurzen Antwort ein wenig Lächeln. Sie
mochte den Ork. Yanil war vor allem aus politischer Überzeugung
zur Crew gestoßen, dass sie Richtiges und Notwendiges taten. Er
hatte besonderes, feinmotorisches Geschick und hatte viele
wichtige Kenntnisse über Gifte und weniger offensive
Angriffstechniken mitgebracht.

"Das ist nicht der einzige Grund." Jannam neigte dazu, eigentlich
nebensächliche Dinge richtig stellen zu müssen. Aber
gerade sprach nichts dagegen und immerhin hatte Marah gefragt. "Wir
brauchten Nahrung, die wir nicht kochen müssen. Wir dachten, dass
ihr vielleicht nicht erraten könnt, dass wir keine Kochmöglichkeit
haben. Also haben wir etwas ausgesucht, das Yanil gut
zeichnen kann, und das sich gleichzeitig im gekochten Zustand
gut hält."

Marah nickte. Das ergab Sinn. "Ihr hungert also. Was ist mit
der Schattenscholle passiert? Wo ist der Rest der Crew?" Marah
ahnte Schlimmes. Und behielt recht damit.

"Vermutlich tot. Von Lyria und Nischke wissen wir sicher, dass sie tot
sind, bei den anderen beiden ist es aber auch sehr wahrscheinlich.", sagte
Yanil. Nischke hatte das Kommando über die Schattenscholle gehabt. Lyria
das Fize-Kommando. "Wir wissen nicht wie, aber
wir hatten plötzlich ein Assassinan an Bord. Lyria hat es geschafft, in
den Niedergang zu gelangen, während der Teil unserer Crew, der zu
dem Zeitpunkt an Deck war, wahrscheinlich ermordet wurde. Sie hat
die Tür zugehalten, solange wir ins Tauchboot geflohen sind, um die Schattenscholle
heimlich zu verlassen. Wir wollten auf Lyria warten, aber sie hat es nicht
vom Niedergang ins Tauchboot geschafft. Myrken hat aus der Dunkelheit
des Unterdecks gesehen, wie sie fiel. Wir haben alle Lenzluken geschlossen, in
der Hoffnung, dass ihre Funktion dann nicht verstanden wird, und haben
die Schattenscholle zurückgelassen. Nun sind wir nur noch zu sechst."

Vier Tote, sechs Überlebende, unter letzteren drei
Landsleute, drei Nixen, zählte Marah. "Oh Lyria.", murmelte sie
traurig. Und all die anderen. Sie hatte Lyria gemocht. Vielleicht
geliebt. Etwas in ihr wollte zerreißen, aber sie ließ es
noch nicht zu, kämpfte dagegen an.

"Es gibt viel zu trauern.", sagte Myrken. Sey schwamm neben
dem Tauchboot und war immer noch etwas atemlos vom
Schieben und Tauchen. Marah stellte es sich unglaublich
anstrengend vor, ein Tauchboot zweckzuentfremden, um es
mehrere Tage überwiegend unter Wasser zu halten, und besonders, es
eben gelegentlich für Luftaustausch hinaufzuschieben. Die drei Nixen wechselten
sich vermutlich ab. "Aber wir dürfen auch Ziele und weitere Gefahren
nicht aus den Augen verlieren. Die wichtige Fragen ist: Wo
kam das Assassinan her? Wo will as hin? Wir haben as einen
halben Tag nach dem Überfall mit unserer Beijolle wegsegeln
sehen. Du weißt, diese weniger kippelige, aber auch
weniger schnelle Badewanne, die wir hatten. Wir haben dann versucht,
die Schattenscholle wiederzufinden, aber
es war aussichtslos. Wir haben sie vollbesegelt verlassen
müssen. Wer weiß, wo sie nun treibt."

Das war unheimlich. "Wir haben ein Assassinan an Bord.", berichtete
Marah. "Allerdings eines, das zu uns übergelaufen ist."

"Unpraktisch nur, dass wir nicht morden und daher auf
solche Dienste eher nicht angewiesen sind.", murmelte Myrken
bissig.

"Ich glaube, Amira möchte eigentlich nicht morden." Marah
berichtete, was in den vergangenen Tagen vorgefallen war.

"Ihr habt Glück gehabt. Freundliche Assassinanen sollen eine
Seltenheit sein.", kommentierte Myrken. Es klang fast so, als
gönnte sey es ihnen nicht. Wahrscheinlich war das Unsinn und Myrken
einfach gestresst. Oder es sprachen die verwundeten Gefühle und
die Erschöpfung aus sem.

Sie hatten wohl Glück gehabt, das stimmte. Marah fühlte sich plötzlich ausgebrannt
und ausgelaugt, wollte nur noch weg. Leute verarbeiteten Trauer
verschieden. Mit Hunger und Hoffnungslosigkeit wahrscheinlich noch
einmal anders. Aber Myrkens Zynismus griff sie in diesem Moment
an, in dem sie Trauer fühlte, die Angst, was als nächstes käme, und
eigentlich gerade erst angekommen war.

Marah reichte die Säcke Knollenkohl zu ihnen hinüber. "Ich könnte
eine Person von euch mitnehmen."

Myrken schüttelte den Kopf. "Wir haben darüber nachgedacht, aber
nein.", sagte sey und deutete in Richtung der gerade so
sichtbaren Landmarke. "Wir fahren dorthin unter Land und hoffen darauf, dass
ihr in der Lage seid, uns weiteren Proviant zu bringen und irgendwann auf
die Schattenschiffe zu verteilen, die noch da sind."

Marah nickte. "Wahrscheinlich wird ein neues eingesetzt."

Die Nachricht schaffte es sogar, ein kurzes Lächeln in Myrkens Gesicht
zu zaubern. "Ach Marah.", sagte sey. "Du bist schon ziemlich großartig. Du
hast meine Laune nicht verdient. Aber ich habe sie im Moment nicht
unter Kontrolle."

"Schon gut.", sagte Marah. "Es ist gerade alles schlimm. Ich hole
Hilfe."

---

Sie weinte erst, als das Tauchboot schon lange außer Sicht war und
der Abend wieder dämmerte. Die Nächte waren besser zum Weinen. Ein
klarer Sternenhimmel erstreckte sich über sie. Den Briefwels hatte
sie zurückgelassen. Myrken hatte sich darüber gefreut. Welse konnten
sehr trostspendend sein. Als wüssten sie, worum es eigentlich wirklich
ging.

Der Wind strich ihr über den Nacken, durch die Haare und in
das Segel, leicht und kühl. Marah erinnerte sich an Lyrias
Finger. Sie hatten keine innige, körperliche Beziehung gehabt, aber
manchmal hatte Marah überlegt, ob sie Lyria hätte fragen sollen. Lyria
hatte mit leichten, kühlen Fingern gern über Wangen gestrichen, oder
unter Haaren im Nacken entlang. Einfach als Geste, wie andere Leute
sich vielleicht in den Arm nahmen, und natürlich erst, wenn es
abgesprochen war.

Lyria war eine Frohnatur gewesen. Lebensfroh wie vielleicht
sonst niemand aus der Schattencrew, hatte immer Positives
in allem gesehen. Manchmal war sie Marah damit auf den Keks
gegangen. Es strengte Marah durchaus an, wenn etwas nicht
als Problem stehen gelassen werden konnte, sondern dann
so ein Spruch folgte, dass es vielleicht irgendwofür
auch sein Gutes hätte.

Aber auch dass würde Marah vermissen. Und darunter dieses
riesige Spektrum an Eigenschaften und Interessen, von denen sie bisher
nur eine Ahnung erlangen hatte können und die sie nun nie zu Ende
kennen lernen würde.

Marah weinte, erst leise für sich, und dann sang sie in ihre Tränen
und Trauer hinein. Lyria hatte einen wunderschönen, sehr hohen
Sopran gehabt. Einen, der nicht so stach, der selbst in den
höchsten Lagen noch weich klang.

Marahs Stimme war eher ein Alt bis Mezzosopran, obwohl sie eingesungen
einen recht großen Tonumfang hatte. In Erinnerung an Lyria und
mit der Kraft, die Tränen irgendwo aus dem
Bauch sogen, sang sie nun so hoch, wie ihre Stimme es erlaubte,
so brechend, wie das Zwerchfell es beim Weinen einforderte, legte ihr
ganzes inneres Sehnen und Ziehen in die langen Töne. Sie hatte nun keinen Begleitwels
mehr, der sie hätte trösten können, aber sie war
auch nicht allein. Ein großer Riffelwal tauchte neben ihr auf und
sang mit ihr mit, um sie zu trösten. Er war auf dem Hinweg schon
einmal aufgetaucht. Da war es umgekehrt gewesen. Da hatte der
Wal um irgendetwas getrauert und sie hatte mit ihm gesungen.
