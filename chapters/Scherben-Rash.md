\BefehlVorKapitelDFDM{Rash-n.png}{Rash schreibt die Nachrichten, die von Bord gehen. Rash hat außerdem eine gute Charakterkenntnis und starke Gefühle für Amira.}{Misgendern, Lebensmüdigkeit, Sex erwähnt, Verliebtheitskuscheldinge.}

Scherben
========

\Beitext{Rash}

Rash dachte tatsächlich einen Moment darüber nach, ob Rash die
Kapitänin um diese unnützen Porzellantassen bitten sollte, um sie
auf dem Deck zu zerschmettern.

Stattdessen blickte Rash Ushenka nach, bis sie
hinter der Kapitänskajüte verschwunden
war, zog die Beine mit auf die Kiste und umklammerte sie. In Rashs
Kopf zerstörte Rash ganze Ladungen an Geschirr auf den Planken. Es
half wenig. Rash schloss die Augen und ließ die Scherben in
kleinere Scherben zerbrechen, mit einem Knarzen, in noch kleinere, bis
der feine Sand zwischen den Planken hindurch in das Deck darunter
rieselte oder vom Wind verwehte.

Ushenka war eine Person, die Kämpfe gefochten hatte, die sicher
nicht leicht gewesen waren. Hafenmeisterin zu sein, war ein
täglicher Stress: Nicht nur die permanente, untergründige
Angst, aufzufliegen. Ushenka war auch gezwungen, all die sexistischen
Witze, die in ihrer Gegenwart gerissen wurden, bei der sie
in die Gruppe denen Männer mitgezählt wurde, mitzumachen. Sie
musste sie nicht lustig finden, aber sie musste trotzdem
ertragen, eine bestimmte Rolle darin zugewiesen zu bekommen, die
von den Machtverhältnissen profitierte, während sie das aber
nicht tat. Warum konnte sie das nicht übertragen?

Es brannte in Rash. Es war auf so vielen Ebenen schlimm. Es ging
bei den Machtverhältnissen eigentlich nicht um Männer. Das ging
nur in eine Richtung: Alle, die davon profitierten, waren Männer, aber
nicht alle Männer profitierten davon, und die, die es nicht
taten, waren meist ganz leise. Männer als Gruppe zum Problem zu
machen, war ein Zuschreiben von Eigenschaften
an Männer.

Hinzu kam: Rash wusste nicht so genau, inwiefern Ushenka darüber
informiert war, wie das mit Rash und Geschlecht war. Sie kannte
wahrscheinlich den Begriff nicht-binär, den Rash für sich gewählt
hatte. Jedenfalls hatte sie damit Rash auch indirekt ein Geschlecht
zugewiesen. Dadurch, dass sie sich Ashnekov und Janasz
für den Witz ausgesucht hatte, und nicht Rash, hatte sie zum Ausdruck gebracht,
dass eine Person mit Geschlecht wie Rash, eine nicht-binäre
Person, nicht auch ein Mann sein konnte.

Manchmal mochte Rash auch einfach nicht über Geschlecht nachdenken, aber
nun fraß sich die Frage wieder durch Rash hindurch und es schwappte
so sehr hin und her, dass es anstrengte.

Rash umschloss die Beine fester. Sich selbst umarmend. Versuchte, sich
selbst lieb zu haben. Rash war sogar zu angespannt dafür, die Ziege
zu streicheln, selbst wenn sie es gewollt hätte. Aber Aga hatte
eigentlich vorhin schon genug gehabt.

---

Schwere Schritte näherten sich. Die Schritte der Kapitänin. Rash blickte
auf und machte auf der Kiste neben sich Platz. Sindra setzte sich. Ihre
Knie hatten dabei einen steileren Winkel als Rashs, weil ihr
Körper so viel größer war. Sie blickte Rash freundlich an.

"Was ist dein Begehr?", fragte Rash. Es war verspielt geplant
gewesen. Aber der Ärger steckte noch darin.

"Ich brauche deine Beratung.", sagte Sindra. "Brauchst du
etwas?"

Sie hatte also etwas bemerkt. "Ablenkung.", antwortete
Rash wahrheitsgemäß. "Worum geht es?"

"Es gab Todesfälle.", sagte Sindra. Nun, das war schon länger
bekannt. "Jetzt auch Arym. Sagt dir der Name was?"

Arym war der Sprachwissenschaftler mit der Zweckehe mit
Ushenka. Rash hatte bemerkt, wie sehr sie trauerte, aber hatte
noch nicht gewusst, um wen. "Ja."

"Wie funktioniert das mit der Trauer?", fragte Sindra.

Rash musste beinahe lachen. Wie sie da so entspannt saß, wie
um ein Pläuschchen zu halten, und diese Frage stellte. Ein
warmes Lachen wäre es geworden. Rash liebte Sindra mit all
ihrem Sein. "Warum möchtest du trauern?", fragte Rash.

Die Kapitänin schüttelte den Kopf. "Ich brauche das für
mich nicht.", sagte sie. "Aber ich möchte der Crew geben, was
sie braucht. Gibt es Rituale? Gibt es Fragen, die ich am besten
stelle? Verletze ich dich mit den Fragen?"

"Nie!", beantwortete Rash die letzte der Fragen und lächelte nun
tatsächlich. Dann dachte Rash nach. Es hatte nicht unbedingt
Sinn, Sindra zu versuchen zu erklären, wie sich Trauer
anfühlte. "Du musst mir wirklich vertrauen, wenn du in so
einer Situation mit dieser Frage zu mir kommst.", murmelte
Rash in die Überlegungen hinein.

Sindra nickte. Wie konnte ein Nicken warm sein? Aber
vielleicht war es auch der Blick oder die Bedeutung hinter
der Geste, die so ohne Einschränkung auf Rashs Mutmaßung erfolgt
war.

"Die Bedürfnisse bei Trauer sind verschieden.", leitete
Rash ein. "Manche reden gern über die Verstorbenen, tauschen
Erinnerungen aus. Manche geben ihnen Geschenke, mehr oder
weniger symbolisch. Manchen hilft singen. Möchtest du
eine Trauerfeier ansetzen?"

Sindra hörte aufmerksam zu und nickte vorsichtig. "Ich
fühle mich so denkbar ungeeignet, eine anzuleiten, aber es wäre wohl
meine Aufgabe als Kapitänin, so etwas zu tun."

"Es würde wohl reichen, wenn du eine Zeit festsetzt und dazu
einlüdest, aber die eigentliche Feier in andere Hände
legst.", erwiderte Rash. "Tatsächlich kann es Trauernden sehr
helfen, selbst etwas vorzubereiten."

"Wenn ich jeder Person einen Raum gebe, eine Erinnerung
zu teilen, woraus sich ein Gespräch ergeben darf, und
anschließend wird gesungen, aber Crewmitglieder
dürfen sich Lieder wünschen? Klingt das sinnvoll?", überlegte
die Kapitänin.

Rash lächelte sanft. Empathie konnte eben auch vollständig
auf Logik basieren und war dadurch nicht weniger wert. "Das
wäre sehr gut, glaube ich." Rash bemerkte, wie sich die
eigene Körperhaltung etwas entspannt hatte. "Warte damit,
bis Marah wieder wach ist. Sie braucht so etwas auch. Also
warte bis morgen Nachmittag. Wenn es regnet, eventuell etwas
früher. Das sollte zeitlich passen, dass wir bis dahin keine
großen Manöver fahren müssen, richtig? Also, wenn nichts
außer der Reihe passiert."

Die Kapitänin beantwortete die letzte Frage nicht. "Das
sind sehr wertvolle Hinweise. Ich danke dir.", sagte
sie bloß. "Kann ich wirklich nichts für dich tun? Du wirkst
bedrückt."

Rash fühlte sich nach einer Umarmung. Ganz sicher nicht nach
einer von Ushenka. Umarmungen halfen Rash nur von Personen, bei
denen Rash das Gefühl einer Umarmung bereits kannte. Aber auch Sindra
wollte Rash gerade nicht fragen. Also schüttelte Rash
den Kopf. Und stand auf. "Ich gehe ein anderes Problem in Angriff
nehmen." Es wurde Zeit.

---

Rash fand Kanta im kleineren Schlafraum. Den, wo Sindra
und Kanta schliefen, inzwischen auch Amira, und ab
heute Nacht wohl auch Ushenka. Aber gerade war der Raum
leer, abgesehen von Kanta. Rash
hätte den Raum im Normalfall nie betreten. Privatsphäre war
eine wichtige Sache, aber gerade ging anderes vor.

"Du kannst den Konflikt nicht umgehen und solltest ihn
vielleicht nicht bis zum letzten Moment aufschieben.", sagte
Rash harsch.

Kanta hatte mit dem Rücken zu Rash gelegen, eingewickelt
in ihr buntes Tuch, das Ashnekov ihr damals aus der
Beute eines der Überfälle überlassen hatte. Bis gerade
hatte sie noch leise gewimmert, -- Krankheit
vorgeschützt, wie Rash schon länger interpretiert hatte. Nun
richtete sie sich auf ihrer Kiste auf und blickte
Rash alarmiert an. Und da war auch Trauer in ihrem Gesicht, erkannte
Rash. Ein tiefer Kummer. Kanta hatte doch niemanden der
Verstorbenen gekannt, oder doch? "Du liest mich gerade, wie
ein Buch, oder? Geh!", beschwerte sich Kanta.

Plötzlich fügte sich alles in ein Bild: Kanta
war nervös gewesen, seit sie mitbekommen hatte, dass Ushenka
an Bord kommen würde. Sie hatte es zu verbergen versucht, aber
Rash war so leicht nichts vorzumachen. Obwohl: Kanta war
gut, wirklich gut! Es hatte Rash Wochen gekostet, um
die Nuancen zu erlernen. "Du kennst Ushenka und sie dich. Und
du weißt, dass dir nichts Gutes blüht, wenn sie dich
erkennt und es der Crew mitteilt." Soviel hatte Rash schon
vorsichtig geschlossen, bevor Rash den Raum betreten
hatte, aber nun bot die verborgene Trauer das Puzzlestück
für ein vollständiges Bild. "Du hattest eine Beziehung
mit Arym, dem Sprachwissenschaftler, der ermordet
worden ist." Sie musste ihn gekannt haben, wenn sie an der
Universität so viel über Sprachen erlernt hatte. Sowie
über Rhetorik. Arym war führend in seinem Forschungsbereich
gewesen und vielleicht fast so etwas wie das Statussymbol
der minzteraner Universität. An
mindestens oberflächlichem Kennen hätte für Kanta kein Weg
vorbeigeführt, wenn sie sich mit entsprechenden Inhalten an
der Universität befasst hatte, aber Rash sah ihr
an, dass da mehr war. "Und du kannst sagenhaft gut lügen."

Bei Rashs letzten Worten verkrampfte sich Kantas
Körper. Das hörte sie nicht gern. Und da steckte
mehr hinter, als dass sie einfach erwischt worden war.

Es war ein guter Zeitpunkt gewesen, Kanta aufzusuchen. Rash
hatte im Moment nicht so viel Raum neben der Wut und
Verletztheit für Mitleid. Zu viel Mitleid hätte
Rash im Moment wohl zu leicht dazu gebracht, alles
zu verzeihen und sich ablenken zu lassen.

"Ich wollte das nicht.", wisperte Kanta. "Aber wer
würde mir das glauben? Wenn doch alle wissen, dass ich
auch leicht vorspielen könnte, dass ich es nicht wollte?"

"Ich.", sagte Rash ohne Zögern.

Kanta blickte erneut auf, dieses Mal weniger alarmiert, aber
nicht minder überrascht.

"Natürlich wolltest du nicht, dass eine Person ermordet wird, die
du liebst.", erklärte Rash. Bemühte sich dabei, die
Balance zwischen einfühlsam und sachlich zu treffen, die
Kanta nicht zum Heulen bringen würde, aber ihr auch das
Gefühl geben würde, ernst genommen zu werden.

"Woher weißt du, dass ich Arym liebe? Geliebt habe?", fragte
Kanta.

Eine berechtigte Frage. "Ich lese dich eben wie ein Buch.", antwortete
Rash trocken. Eine bessere Antwort gab es nicht. "Ein
ziemlich kodiertes."

Kanta versuchte ein vorsichtiges Lächeln.

Es hatte auch ein Witz sein gesollt. Aber es
war auch ein Hinweis. "Du kennst dich mit
Kodierung aus.", murmelte Rash.

Kanta hatte zwischendurch den Blick gesenkt, wie vorhin, und
hob ihn nun ein drittes Mal, dieses Mal war es eine blanke
Fassade. Die Rash verstand, als was sie war: Dass Rash
sie erwischt hatte.

Rash unterbrach sie, noch bevor sie das Thema wechseln
konnte. "Meine Schrift.", sagte Rash. "Daher hatte ich
Erinnerungen. Weil ich die Buchstaben geschrieben habe. Und
du lediglich irgendwie weitergegeben hast, nach welchem Muster
einzelne aus den Briefen abgepaust werden mussten. Du
wusstest ja, welche Wörter ich geschrieben habe. Du musstest
sie dafür gar nicht selber schreiben."

Kanta senkte dieses Mal nicht den Blick, wirkte
nachdenklich. Rash konnte fast zusehen, wie Kanta berechnete, dass
sie es nicht verbergen könnte, wenn es so wäre, weil
ja nun mit Ushenka, die ein sagenhaftes Gedächtnis hatte, ein
Wissen an Bord war, das nah genug an den Originalen war. "Es
war ein sehr einfacher Code.", sagte Kanta auch noch. "Immer
der selbe. Hat mit Anfangsbuchstaben zu tun."

Rash hatte die ganze Zeit gestanden, aber plötzlich verließ
Rash die Kraft. Eine Anspannung fiel ab. Das Geheimnis
der eigenen Schrift war gelöst. Rash setzte sich in einigem
Abstand ihr gegenüber auf den Boden. "Warum?"

"Wenn ich es dir erzähle, erzählst du es direkt der
Crew, richtig?", fragte Kanta niedergeschlagen. Ohne
Fassade.

"Das darfst du auch gern selbst tun.", erwiderte Rash.

"Was würde das bringen?", fragte Kanta. "Ihr werdet mich
töten. Damit unterschriebe ich mein Todesurteil selbst."

Rash erinnerte sich daran, dass Kanta recht müde des Lebens
angeheuert hatte. Aber verkniff sich, sie zu erinnern, dass
sie auch bereit gewesen war, ihr Todesurteil selbst zu
unterschreiben, als sie versucht hatte, an Bord
zu kommen. "Nein."

"Ach, denk doch logisch.", brauste Kanta auf. "Ja, klar, hier
herrscht ein anderer Pazifismus vor als in der Bantine." Die
Bantine war die Elben-Nation, zu der Minzter gehörte. "Aber
ich habe euer aller Leben riskiert und einige auf dem
Gewissen. Da würde selbst eure Crew meinen Tod wollen. Und
das ist auch nur gerecht."

"Ist es das?", fragte Rash. Vielleicht etwas spitz. "Ist
es jemals gerecht eine Person zu töten? Und hast du wirklich
eine Person auf dem Gewissen?"

"Die Briefe sind bei Amira gelandet.", sagte Kanta. "Natürlich
trage ich eine Mitverantwortung dafür."

Rash fühlte sich nicht barmherzig, als Rash sagte: "Sehr wohl. Die
will ich dir nicht absprechen."

Kanta rang die Hände ineinander und saß kurz darauf wieder
still da. Traurig. Den Blick wieder gesenkt. "Du hast
gefragt, warum.", erinnerte sie sich. "Ich bin in deren Haushalt
ein- und ausgegangen. Weil ich mit Arym geschlafen habe zum
Beispiel." Kanta blickte einen kurzen Moment auf, aber Rashs Gesicht
zeigte keine Regung. "Ushenka hat Geschichten über die
Geisterflotte erzählt, aber ich kann gut lügen, wie du festgestellt
hast, und ebenso gut erkenne ich Lügen. Ich war damals jung und
lebensmüde, und Forschung war mir wichtig, gab mir Halt. Ich
wollte das Geheimnis lüften, warum Ushenka so viele Märchen
erzählte und habe nach und nach alles über sie aufgedeckt. Bis
sie mich erwischt hat, als ich Briefe von der Schattenmuräne las,
und dafür gesorgt hat, dass ich das Haus nie wieder betreten durfte."

Rashs Körper fühlte sich so müde. Es war auf bizarre Art
erleichternd, so etwas eigentlich Unbedeutendem zu
lauschen, wie der wahren Geschichte von Kanta, in
dieser Stimmung aus Trauer und Verlusten.

"Über Nixen und Ushenka habe ich nichts weiter herausfinden
können. Mir blieb die Beziehung mit Arym, die ich in der
Bibliothek fortgeführt habe. Bis es meiner Cousine auffiel
und meine Welt komplett in Scherben lag.", fuhr Kanta
fort. "Mein Leben außerhalb deren Haushalts und ohne
Arym war nie lebenswert gewesen. Ich musste weg."

Rash realisierte, dass Kanta während des Redens über den
Schal strich. Der Schal, der in einem Kartoffelsack
gesteckt hatte. Wieso würde ein Schal in einem Kartoffelsack
stecken? "Ist der Schal von Arym?", fragte Rash.

Kanta blickte wieder überrascht oder alarmiert auf. Rash
konnte die nicht gespielten Emotionen noch nicht so gut
übersetzen. Was witzig war. Über die Fassaden hatte Rash
nun gelernt, was dahintersteckte, aber über die nicht
maskierten Gefühle wusste Rash es weniger genau.

Kanta schüttelte den Kopf. "Von Arwin.", widersprach
sie.

Arwin. Rash erinnerte sich an den Namen. Es war Kantas
bester Freund gewesen. Sie hatte mehrfach erzählt, dass
sie ihn sehr vermisste. Und dann hatte es plötzlich
aufgehört, fiel Rash ein. Das hätte Rash auffallen
können. "Ihr habt Austausch, seitdem der Schal an Bord ist?"

Kanta schüttelte wieder den Kopf. "Ich habe immer auf
Austausch gehofft.", sagte sie. "Aber er hat mir nur einmal
diesen Schal geschickt." Kanta seufzte tief, bevor
sie eine Wahrheit aussprach, die sie nicht zwangsläufig
hätte teilen müssen. "Mit einer Knotenbotschaft. Wir
haben in der Unviersität gemeinsam eine Knotenschrift
gelernt. Er schreibt in diesem Schal durch winzige Knötchen, die
er hineingearbeitet hat, dass er mich mag, dass sie
eine Person in Ushenkas Haushalt haben schmuggeln können, die
aber nicht lesen kann, und welche Kodierung ich nutzen
soll, wenn ich es einrichten kann, zu beeinflussen, was du
schreibst."

"Das ist gewagt. Wer hätte sich denken können, dass du
mich beeinflussen darfst?", fragte Rash.

"Das war alles nicht so klar.", bestätigte Kanta. "Wir haben
unzählige Alternativpläne ausgemacht. Was ich mache, wenn was
jeweils geht oder nicht geht."

"Aber du wolltest nicht selber schreiben.", fiel Rash
wieder ein.

"Die Kodierung und deine Schrift sollte mich eigentlich
schützen, wenn etwas passieren sollte.", sagte Kanta. "Vor
allem wird eine Person, die selber nicht schreibt, auch weniger
wahrscheinlich verdächtigt."

Rash erinnerte sich an das Gespräch zurück, das Sindra
mit Rash und Kanta gemeinsam gehabt hatte, nachdem sie sie
einzeln verhört hatte. Weil Kanta interessante Ideen
gehabt hatte und sie neugierig gewesen war, was sie
gemeinsam herausfinden könnten. "Du hast Hinweise gegeben, die
dich hätten gefährlich leicht verraten können." Die
Hinweise, dass eine Person die Buchstaben abpauste, waren
von ihr gewesen.

"Weil ein Assassinan an Bord war, das den Brief mitgebracht
hatte!", fuhr Kanta auf. "Ich wollte nie, nie dass so etwas
passiert. In meiner Forschungsgruppe war Töten nie das
Ziel. Gefangennehmen, ja, aber eine Person an Bord zu
schleusen, deren Auftrag es ist, zu töten, das wollte ich
nicht."

Rash nickte gelassen. "Vergleichen wir, wie Amira und du
an Bord gekommen seid, hättet ihr auch ganz schön viel
Geschick dazulernen müssen."

"Nicht mehr 'wir'.", sagte Kanta. "Ich habe schon länger
nicht mehr eigene Briefe in deine gemischt. Es kam nie
Antwort. Ich habe das gemacht, weil ich Arwin liebe." Sie
ließ den Kopf hängen. "Ich war zu dem Zeitpunkt, als Sindra
mich ausfragte, bereit, alles zu teilen, aber ich wollte auch
nicht sterben. Ich habe also versucht, euch alle Hinweise
zu geben, die ihr brauchen könnt, ohne mich zu verraten. Ich
habe euch lieb gewonnen. Und ich fühle mich innerlich zerrissen. Weil
ich so schlimme Dinge getan habe."

Rash wusste nicht wie das passiert war, aber plötzlich
berührte eine von Rashs Händen sanft Kantas Unterarm. Kanta
blickte die Hand an, als wäre sie surreal. "Wenn du auf meinen
Rat hören möchtest, dann gehst du jetzt zu Sindra und legst
ihr alles dar. Alles, was du mir auch erzählt hast.", sagte
Rash. "Ich kann nicht behaupten, dass ich besonders glücklich
bin darüber, was du getan hast. Aber ich werde da sein und
dich auffangen, wenn du es brauchst." Rash hatte Kanta
auch aufgefangen, als Arwin Anlass gewesen war. Anfangs.

Kanta legte für einen Moment die eigene Hand auf Rashs, dann
stand sie auf. Nickte. "Ich mache das jetzt einfach.", sagte
sie. Einfach würde es bestimmt nicht werden. "Ich nutze
deine Überzeugungskraft und tue es. Bevor ich es mir anders
überlegen kann." In der Tür drehte sie sich noch einmal
um. "Danke, Rash."

Rash blieb einen Moment ermattet hocken, seufzte lautlos
und tief nur für sich selbst. Aber lange verharrte
Rash nicht für sich allein. Es war sicher besser, wenn Rash mit
nun dem vollen Überblick über die Lage in Kantas Nähe bliebe.

---

Kanta teilte Sindra zunächst alles allein in ihrer Kajüte mit, aber
es wurde sich anschließend bis spät in die Nacht ernst mit verschiedenen
Leuten darüber unterhalten. Mit Smjer zum Beispiel. Marah
wurde immerhin nicht geweckt. Aber Ushenka und Kanta sprachen
sich aus. Bei dem Gespräch wünschte sich Kanta, dass Rash
dabei sein möge. Es war erleichternd und deshalb nicht weniger
anstrengend.

Rash taumelte ganz zum Schluss den Niedergang hinab in
Richtung der Kiste in der Nische, auf der Rash schlief, auch
Koje genannt, aber als Rash sich näherte, lag etwas vor
der Kiste auf dem Boden. Rash beugte sich hinab. Und
ein Gefühl wie warmes, sehr flüssiges Öl rann durch Rashs
Körper, so unvermittelt und stark, wie Rash gar nicht
vermutet hätte, das Rash überhaupt hätte fühlen können, geschweige
denn nach diesem Tag. Rashs Hals fühlte sich immer noch heiß
und der Atem zittrig an, als Rash Amira vorsichtig
an der Schulter berührte. "Amira?", flüsterte Rash.

Amira richtete sich in der Dunkelheit auf. Wie konnte dieses
Verliebtheitsgefühl und die Anziehung so
unfassbar stark sein? Rash wünschte sich
die eigenen Hände über Amiras Haut verteilt, die auslösten, dass
ihre zarten Küsse nach mehr verlangten. Den leichten Druck ihrer
Hände, der ausreichte, um Rash einen Platz zuzuweisen, einzufordern,
wie Rash sie verwöhnen möge. Die zarten Geräusche, die ihr
entwichen, unbewusst, unbeabsichtigt.

"Darf ich bei dir schlafen?", flüsterte Amira.

Rash legte sich an die Rückwand auf die Kiste und öffnete
die Arme.

"Aber nicht zu laut knutschen!", kam Ashnes Stimme vom
Nachbarbett.

Rash kicherte in den Kuss hinein, den Rash sehr weich und
definitiv geräuscharm auf Amiras Stirn schmiegte. Das
war, worum Rash die beiden damals gebeten hatte, als Janasz
und Ashne zu kuscheln angefangen hatte.

Rashs Arme schlossen sich um Amiras Körper. In Rashs Kopf
bildete sich ein weiteres Mal diese Frage, die
Rash vorhin Kanta gestellt hatte, aber dieses
Mal sprach Rash sie nicht aus: Warum?
