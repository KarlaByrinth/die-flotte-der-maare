\BefehlVorKapitelDFDM{Jentel-n.png}{Jentel ist die Mastkorb-Nixe. As verbringt sowohl Zeit mit Janasz beim Essen im Mastkorb, als auch mit Marah zum Singen}{Keine bisher.}

Beobachten
==========

\Beitext{Jentel}

Jentel hatte noch nie zuvor einen Menschen gesehen. So
aufregend war das jetzt auch nicht. Der Menschheits-Anteil
seiner Anwesenheit war es nicht. Wie er an Bord gekommen
war, war durchaus eine mysteriöse Frage. Jentel war sich
schon einigermaßen sicher, dass an ihm nicht so leicht
eine Person vorbeischleichen könnte. Das bedeutete, dass
diese Person entweder durch das Tauchdeck hineingekommen
war, oder tagsüber. Und dass sie auch bis jetzt höchstens tagsüber
an Deck gewesen war. Es hieß, sie wäre schon etwa drei Tage
unbemerkt an Bord gewesen. Über mehr hatte Sindra as nicht
informiert. Lediglich darüber, dass da nun
eine Person mehr an Bord wäre, damit as keinen Alarm schlagen
würde. Und sie hatte as gefragt, ob ihm nicht doch irgendetwas
auffällig vorgekommen wäre. Ihm war nichts auffällig vorgekommen
und das war gewissermaßen unheimlich.

Nun war dieser Mensch, von dem Jentel nicht einmal wusste, wie
er hieß, für alle sichtbar in Erscheinung getreten. Nur
Janasz hatte schon zuvor etwas bemerkt. Oder zumindest
glaubten sie, dass nur Janasz etwas bemerkt hatte, es
konnte natürlich auch ein Crewmitglied
über dergleichen schweigen. Der Mensch hatte sich sehr
unauffällig Essen stibitzt. Janasz hatte keine Details erzählt. Es
war überraschend, dass er überhaupt so viel erzählt hatte. Janasz
hatte Angst. Und er glaubte, die Kapitänin nähme das alles
zu locker. Sie hatten sich darüber unterhalten, dass sie
ihn nicht unbedingt in all ihre Pläne eingeweiht haben musste, mit
der sie gedachte, die Sicherheit der Crew zu garantieren. Und
dass Sindra eben ohnehin selten Beunruhigung zeigte.

Jentel drehte sich im Mastkorb im Kreis, um gründlich Ausschau
zu halten, bevor as sainen Blick wieder auf die Gestalt senkte, die
dort verloren allein an Deck hockte und in die Ferne blickte. In die
dunklen Wolken, die den Himmel überdeckten, und aus denen es
noch nicht regnete, aber feinste Tröpfen lagen bereits
in der Luft, legten sich auf sain Gesicht. Jentel hatte gehofft, dass
die Wolken auch Wind bringen würden, aber ungewöhnlicherweise
taten diese es nicht. Am Horizont waren sie wunderschön. Jentel
vermutete, dass die Person dort unten das auch so empfand, oder
wenigstens das in die Ferne blicken sie beruhigte. Von
hier oben könnte sie weiter blicken, dachte Jentel trocken. As
beobachtete sie nun schon seit Stunden. Sie blickte gelegentlich
zu ihm hinauf. Nicht mit so einem Blick wie Kanta, nicht, weil
sie saine Aufmerksamkeit suchte, sondern eher als routinierte
Kontrolle, um as im Blick zu behalten. So wie as immer wieder
über das Meer blickte.

Jentel mochte die Kleidung der Person. Praktische, enge Kleidung mit
Verziehrung, die Haare unter einem Kopftuch verborgen, weiches
Schuhwerk. Jentel mochte manche Schuhe sehr, und das war
der einzige, alberne Grund, aus dem sich Jentel je überlegt
hätte, dass Füße auszuprobieren auch ganz spannend sein
könnte. Die Kleidung an diesem Menschen war sehr
ordentlich gepflegt und achtsam getragen. Die Bewegungen waren unscheinbar
und doch perfekt, als durchdachte die Person jede davon. Fast als
versuchte sie mit den Schatten zu tanzen. Jentel
wusste nicht so genau, was as zu der Entscheidung brachte, die
Person heraufzuwinken, als ihr Blick das nächste Mal in saine
Richtung schweifte.
