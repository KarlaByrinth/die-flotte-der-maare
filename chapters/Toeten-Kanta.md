\BefehlVorKapitelDFDM{Kanta-n.png}{Kanta wurde von Jentel aus dem Wasser gefischt und ist seit dem an Bord. Sie befasst sich viel mit Sprachen.}{Töten - erwähnt, Betäubung, Hinrichtung als Thema.}

Töten
=====

\Beitext{Kanta}

Smjer hatte recht gehabt, eigentlich. Sie hätte die besten Chancen gehabt, sich
in der Situation herauszureden, in der Sindra nun steckte. Gut, es hatte
auch eine realistische Chance gegeben, dass die Kapitänin mit der
Jolle hätte entwischen können. Aber
das hatte nicht geklappt. Die Person aus dem Zarenreich, die sie auf dem
Laufenden hielt, hatte gerade eine kurze Nachricht zu ihnen gesandt, dass
Sindra gefangen genommen worden war. Und als Kapitänin konnte Sindra
nicht in die Rolle eine Spionin schlüpfen, die Kanta mal gewesen war, um
so zu tun, als wäre sie eigentlich nicht auf Seiten der Flotte. Das war in
ihrer Position einfach nicht drin, egal wie gut Sindra darstellen können
mochte.

Sie saßen auf sanften, noch vom Sommer vertrockneten Grashügeln
in Ufernähe, weit genug weg von Mizugrad um nicht so viel Aufsehen
zu erregen. Hin und wieder spazierte eine Person vorbei. Janasz
beobachtete sie, um sie im Zweifel mit Betäubungspfeilen lahm zu
legen, falls sie sich als Schlimmeres als Passierende entlarven
würden. Die Schattenmuräne als Tauchboot lag startbereit, getarnt
als kleine ufernahe Insel, wie es hier einige gab. Sie lag auf
Grund aber schaute oben aus dem Wasser, sodass sie hatten aussteigen
können. Die Nixen lagen unterhalb des Grasüberhangs am Ufer, erschöpft, einige
schliefen. Die Ziege graste. Sie hatten sie nicht angebunden, aber
die Ziege dachte nicht daran, sich weit von ihnen zu entfernen.

Kanta machte sich erneut Gedanken um die Sache mit der Ziege. Es
war das erste Mal, dass sie es konnte, ohne dass sie ein Grauen
durchfuhr. Nun verstand sie es besser. Vielleicht zumindest.

Die Ziege hätte es nicht überlebt, wenn sie sie nicht mitgenommen
hätten. Kanta dagegen hätte große Chancen gehabt zu überleben. Mit
der Erklärung konnte Kanta leben. Aber sie fühlte, dass das nicht
alles war. Smjer hatte es nicht so begründet, sondern damit, dass Aga
niemandem etwas getan hätte. Kanta kämpfte jeden Tag gegen die
Schuldgefühle an. Und dann gegen Trotzgefühle, die sie noch
weniger wollte. Und doch empfand sie es seltsam, dass ihr Leben
dem Leben einer Ziege untergeordnet werden könnte, weil sie diese
Schuld trug.

Wahrscheinlich ging es auch darum nicht. Sondern schlicht darum, dass
Smjer kein Leben höher wertete, als ein anderes, abhängig von
der Gefühlsfähigkeit oder Denkfähigkeit einer Person.

Kanta grinste, als sie sich dabei erwischte, über Aga als
Person gedacht zu haben. Das erste Mal.

Sie sollte sich um die Trotzgefühle kümmern. Wenn sie wirklich
dazugehören wollte, waren diese fehl am Platz. Bisher war
ihr Ansatz verdrängen gewesen. Das war ihr bis zu einem
Gespräch mit Kamira nicht einmal bewusst gewesen. Kamira
hatte ihr erklärt, dass Verdrängen nicht grundsätzlich
schlecht wäre, dass es durchaus ein Mechanismus war, der
eine Berechtigung hätte, aber manchmal war es eine
kontraproduktive Strategie, um das Gefühl loszuwerden. Es
gab unerwünschte Gefühle, vor allem
Ängste, aber womöglich auch diese Trotzgefühle, die
sie eher überwinden könnte, wenn sie sie zuließe, in sie
hineinfühlte, die Verknüpfung der Gefühle fände und
sie verstehen und zur Seite legen konnte. Kamira
hatte erklärt, dass auch diese Methode nicht bei jeder Psyche so funktionierte, aber
bei ihrer, so hatte sie mit ihm herausgefunden, durchaus. Also
tat sie es.

Eigentlich lag ihr Leben wieder in Scherben. Das war der Satz, der
zu dem Gefühl gehörte, und den sie nun zuließ.

Sie hatte sich in Ashnekov verliebt. Vielleicht auch mehr als
das. Ashnekov war liebevoll und fürsorglich, hörte immer zu. Er
war intelligent und interessant. Er war ihr niemals böse
gewesen. Nicht einmal, als sie alles offen gelegt hatte. Und er
war Janaszs Liebesperson. Wieso Kanta das übersehen hatte, war ihr
nicht klar. Dazu gehörte eigentlich eine Menge Ignoranz. Sie
hatte es nicht sehen wollen. Aber auf der beengten Fahrt in
der Tauchbootvariante der Schattenmuräne hatte er unverkennbar
liebend mit Janasz gekuschelt. Nun, eigentlich war es
nie möglich, aus der Art, wie Personen kuschelten, zu schließen, wie
sie ihre Beziehung miteinander nannten. Oder ob sie romantisch
oder nicht war, oder sonst etwas. Aber sie war so sehr innig
gewesen, dass Kanta sich nicht damit wohl fühlte, Achnekov in
gleicher Weise auch zu beanspruchen. Ihr war sehr wohl
bewusst, dass es auf der Flotte der Maare anders zuging, dass
einige Personen Beziehungen mit mehreren zugleich führten. Dass
es hier keine strikten Regeln gab, dass Liebesbeziehungen immer
nur aus zwei Personen bestehen müssten, sondern stattdessen
jede solche Regel unter sich für die jeweilige Beziehungskonstellation
abgesprochen wurde. Aber selbst mit dieser Freiheit fühlte
sich Kanta nicht in Liebesbeziehungskonstellationen wohl, in
denen sie eine Person auf diese Art teilen müsste. Kamira hatte
ihr gesagt, dass das in Ordnung war und keinesfalls Bedingung
für sie wäre, dass sie sich ändern müsste, um als Crewmitglied
akzeptiert zu werden.

Sie hatte sich in Ashnekov verliebt, und mit dem Stand der
Dinge, zerbrach ihr Traum, ihn eines Tages zu fragen, ob er
für sie auch in dieser Art empfand. Etwas zu entwickeln. Das
verursachte das Frustgefühl, dass ihre Welt wieder in Scherben
läge. In kleineren Scherben noch, weil es keine weitere
Auswahl gab. Kanta würde sich nicht so leicht neu verlieben. Sie
kannte die anderen alle schon. Sie fühlte sich zu keiner anderen
Person hingezogen.

Und das wiederum, zusammen mit Smjers Befehl, löste das
Trotzgefühl aus. Warum sollte sie sich um eine Gruppe
scheren, in der ihre Träume nicht erfüllbar wären, und die
ihr Leben für weniger wert hielt als das einer Ziege?

Kanta ließ sich nach diesen Gedanken -- Psycho-Hygiene, wie
Kamira es nannte -- entspannter gegen Rashs Körper sinken. Rash
hatte die Arme um sie gelegt und schloss sie nun noch
etwas fester. Nicht in einer romantischen Weise,
einfach, weil sie befreundet waren. "Ich mag dich, Rash.", sagte
Kanta.

Rash schloss die Arme noch fester um sie. "Das hast du noch
nie gesagt.", murmelte Rash leise. Ein breites Grinsen war
in der Stimme hörbar. Und dann: "Rettest du uns?"

---

Ushenka, Amira und sie, welch ein Gespann. Kanta hatte es
ziemlich beeindruckt, wie schnell Ushenka sie damals in den
Arm genommen hatte mit den Worten 'Gut, dich wieder bei uns
zu wissen.'. Damals, als Kanta alles offen gelegt hatte, und
gerade vor Ushenka Angst gehabt hatte. Weil sie Ushenka am meisten
betrogen hatte und auch mitverantwortlich für den Tod ihres Mannes
war. Ushenka hatte sie noch viel früher hochkant aus ihrem Haushalt
rausgeworfen. Aber Ushenka war so unvorstellbar wenig
nachtragend. Sie hatte Kanta in den Arm genommen, ihr erklärt, dass
sie sie vermisst hatte, die Konversationen mit ihr über Sprache
und Forschung, die sie miteinander verbunden hatten. Dass sie
gesehen hatte, wie sehr Kanta in Schwierigkeiten gewesen wäre, und
wie sehr auf einem Weg einer Entwicklung. Aber dass der Rauswurf
eben unumgänglich gewesen wäre. Und dann hatten sie gemeinsam
um Arym getrauert.

Kanta war älter geworden und das war an Ushenka nicht
vorbeigegangen. Und doch überwältigte es Kanta auch jetzt noch, dass
Ushenka nicht mit der Wimper zuckte, als sie sie fragte, ob
sie nun mit ihr käme: Die Kramelin ausforschen. Herausfinden, wie
die Lage aussähe. Amira hingegen hatte gezögert. Amira war
für Kanta verhältnismäßig undurchsichtig. War es eher ein Nachdenken
darüber, wie sie helfen könnte, oder ob sie überhaupt wollte? Sie
stimmte schließlich zu. Sie ging nicht direkt mit Ushenka und
Kanta, machte sich selbstständig.

Während ihres Erkundungsausflugs, bei dem Ushenka eine gesprächige
Touristin schauspielte und auf diese Weise sich in unbeschreiblich
kurzer Zeit große Mengen Information ertratschte, und Kanta vor
allem beobachtete und erste Ansätze für eine Reihe möglicher
Pläne entwickelte, erblickte sie Amira nur gelegentlich in einer
Menge. Sie hätte sie vermutlich gar nicht gesehen, wenn sie nicht
gezielt auf das Assassinan geachtet hätte. Ehemaliges Assassinan.
Ehemalige Kapitänin. Alles änderte sich so sehr. Kanta hoffte, dass
sie Sindra befreien werden könnten, sodass sie die Flotte der Maare
wieder kommandierte. Sie hatte ihre Zeit gebraucht, um sich an
die Kapitänin zu gewöhnen, aber nun würde sie sie ohne Zögern
zurücknehmen und sich von ihr befehligen lassen, wenn es nötig
war.

Als sie zurückkehrten, wussten sie mehr und Kanta entwickelte
einen Plan. Eigentlich gleich ein paar Pläne. Ersatzpläne
waren immer gut.

"Wir sind eine gestrandete Forschungscrew.", erklärte sie. "Eine, die
von der Schattenmuräne zuletzt zurückgelassen worden ist. Das
ist eine Geschichte, mit der wir Chancen haben, in die Kramelin
vorgelassen zu werden. Die Zarin interessiert sich brennend
für den Verbleib der Schattenmuräne."

"Gerüchte decken sich, dass sie Personen, die glaubhaft versichern
können, etwas zu wissen, tatsächlich selbst vernimmt.", ergänzte
Ushenka.

"Wir sind allerdings etwas gemischt.", hielt Kanta fest. "Ich
denke, die Geschichte, dass Zwerge und Elben gemeinsam eine
Crew gebildet haben, ist vielleicht riskant."

"Das Mandulin-Volk käme als vorgegebener Ursprung einer solchen
Crew infrage.", kommentierte Jentel.

Kanta nickte. Sie wusste um Jentels Herkunfstort. Und es bedeutete
ihr viel, dass as nun mit ihr sprach. Jentel hatte sie so lange
gemieden. "Du hast recht und den Gedanken hatte ich auch.", sagte
sie. "Aber das Mandulin-Volk steht mit dem Zarenreich als
friedliche Nachbarn" -- Kanta unterbrach sich, um sich zu
korrigieren -- "Nachbaranen, ist das richtig, Nachbaranen?"

Jentel nickte. "Das ist unsere erfundene Form, ja.", aber
korrigierte sich dann noch einmal. "Obwoh, du wolltest den
Singular. Dann: als friedliches Nachbaran."

"Es steht mit dem Zarenreich als friedliches Nachbaran in engem
Kontakt. Das Mandulin-Volk hat zwar ein Forschungsschiff losgeschickt, ohne
die Zarin zu informieren, aber das ist nun eine Weile her. Sie
hatten damals schon zu wenig Ressourcen für ein hochseetaugliches
Schiff und hatten in den vergangenen Jahren zunehmend
mit Armut zu kämpfen.", fuhr Kanta fort. "Die Zarin und ihre Beratung wüssten
wahrscheinlich davon, wenn das Mandulin-Volk eine Forschungsreise
angetreten hätte, weil es um Zuschüsse hätte bitten müssen, und
das Zarenreich ist das einzige, das derzeit das Mandulin-Volk
unterstützt."

"Das muss nicht sein.", erwiderte Ashnekov. "Ich gebe dir im
Wesentlichen recht. Das sollte eher ein Plan B oder so sein. Aber
das Mandulin-Volk hat eine etwas andere Sicht auf Leben und
Leben Lassen, als die Zarin. Das war auch damals wahrscheinlich
der Grund, weshalb es sich nicht an sie gewandt hat. Es wäre dem
Mandulin-Volk zuzutrauen, so etwas für einen zweiten Versuch sehr lange im Voraus
unter der Hand zu planen. Du klingst aber so, als hättest du einen Plan A?"

Kanta nickte. "Auch nicht wenig riskant.", leitete sie ein. "Es
hat kürzlich neue Einstellungen bei den Wachen gegeben. Das
heißt, die Wachen kennen sich noch nicht so gut. Und du, Janasz, siehst
einer der neuen Wachen einigermaßen ähnlich."

"Oh." Janasz runzelte die Stirn, aber konnte sich auch ein
Grinsen nicht verkneifen, das selbst sein Bart nicht
verbergen konnte. "Und du meinst, ich kann in die
Rolle einer Wache schlüpfen?"

"Ich trainiere dich.", sagte Kanta. "So schwierig ist das nicht. Ich
kenne dich gut genug, dass ich dich in einer halben Stunde, etwas
Pause und noch einmal einer halben Stunde in die Rolle eingelernt
haben würde."

"Hast du dabei einberechnet, in welch unmöglichen Situationen
ich plötzlich loslache oder weine?", fragte Janasz.

Das war eine jüngere Entwicklung bei ihm, aber Kanta hatte
sie sehr wohl bemerkt. "Ja, so eine knappe
halbe Stunde solltest du ohne durchhalten.", sagte
Kanta. "Aber als neu eingestellte Wache, die gerade eine Gruppe
gestrandeter Forschender, Forschanen?, eigentlich egal, in den
Raum führt, darfst du als noch frische Wache durchaus eine gewisse
Aufgeregtheit haben, die du nur mühevoll und unerfolgreich
verbirgst."

Janasz kicherte. Und nickte. Kicherte etwas mehr. "Das Kichern
ist so unpassend in dieser ernsten Lage."

"Es wird früh genug brenzlich werden.", beruhigte Ushenka. "Wo
wir beim Thema sind: Ich werde auf jeden Fall bei jedem Vorhaben
mitmachen. Sie haben auch Marah. Die Gerüchteküche ist sich über
eine lebendige, kleine Nixe in Gefangenschaft
einig. Aber diese Rettungsaktion ist keine, die wir
zuvor als unser Ziel ausgehandelt hatten. Jede Person, die
mitmacht, steht hinterher vielleicht nicht mehr der Flotte
der Maare zur Verfügung. Wir dezimieren hier potenziell unsere
Crew. Macht euch Gedanken, ob ihr das wirklich wollt."

"Werden sie uns töten, wenn es schief geht?", fragte Janasz.

"Es ist nicht ausgeschlossen.", erwiderte Kanta. "Die Zarin
ist nicht dafür bekannt, Hinrichtungen in großem Stil
durchzuführen. Aber wenn wir in die Kramelin eindringen, und
zwar nicht als Verhandlungspartneranen, sondern als
Verbrecheranen, dann wird sie uns nicht so leicht wieder
freilassen wollen, sollte es uns nicht gelingen, auch
wieder zu entkommen. Und lebenslanges Gefangennehmen
ist so eine Sache. Zarin Katjenka hat nicht genügend
Zellen und arbeitet dann schon über Auslieferung und
manchmal dann doch über Hinrichtung, statt weitere zu
bauen."

Janasz nickte. "Ich habe kein sonderliches Interesse
zu sterben.", sagte er. "Aber ich bin dabei. Komme
was wolle."

"Was konntest du eigentlich herausfinden, Amira?", fragte
Kanta.

"Haben wir Zugriff auf Stift und Papier?", fragte diesan.

Und was dann geschah, überwältigte Kanta unerwartet. Amira
zeichnete aus dem Kopf Pläne, wo häufig
viele Wachen waren, wo weniger viele, und aus welchen
Verhaltensweisen und Beobachtungen sie das abschätzte. Sie
erklärte, wie Orte möglichst
unauffällig gewechselt werden konnten. Sie war bis in die
Kramelin hineingelangt, aber nicht lange geblieben, weil
sie es für wichtiger gehalten hatte, mit weniger
aber dafür sicherer Information zurückzukehren, als
eventuell gar nicht, im Fall, das sie doch erwischt worden
wäre. Kanta nickte anerkennend. Und dann planten sie alles
ausführlich durch.
