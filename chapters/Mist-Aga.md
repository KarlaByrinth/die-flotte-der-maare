\BefehlVorKapitelDFDM{Aga-n.png}{}{Thema gegessen werden, Fäkalsprache.}

Mist
====

\Beitext{Aga}

Ich war also mal eine Gebirgsziege. Und nun bin ich es
nicht mehr. Na gut. Oder doch? Maare, das sind doch
eigentlich Vulkanseen. Oder Vulkanmeere? Jedenfalls Gewässer
in irgendwas Bergartigem. Diese Leute hier heißen
Maare. Sind sie Vulkane? Oder Berge? Das Leut, das den
Mund am weitesten aufmacht, ist sehr groß. Das kann natürlich
zusammenhängen. Körpergröße und große Münder. Und
Berge. Ein Bergleut. Also, ein Leut aus Berg. Ein sehr großer
Mund, jedenfalls. Mit einer Stimme wie Eis. Ich mag Eis. Eis ist schön
kalt. Feuer machen mir Angst. Eine Stimme wie Feuer wäre
schlecht.

Ich weiß sehr wohl, dass mich Leute kochen wollten. Ich
glaube, nicht bei lebendigem Leib. Dann hätte ich das
gekocht werden wohl nicht mehr so sehr mitgekriegt. Trotzem
scheiße. Entschuldige den Ausdruck. Mist. Ich bin eine
Ziege, mein Dreck heißt Mist und nicht Scheiße. Oder?

Jedenfalls bin ich hier eigentlich ganz froh. Gebirge wäre
besser. Aber Schiff ohne gegessen zu werden ist besser,
als Schiff mit gegessen zu werden.

Sie haben mich Aga genannt. Rash, um genau zu sein. Ich
glaube, ich bleibe viel bei Rash. Wenn Rash nicht
gerade unter Deck ist. Ich bin oder war eine Gebirgsziege: Ich
bleibe oben. Vielleicht ist Rash viel unter Deck. Ich
habe kein gutes Zeitgefühl. Aber diese große Person ist viel
an Deck. Bei der bin ich auch gern. Sie will mich nicht
kochen. Vielleicht mag sie mich sogar.

Ich muss dringend an etwas anderes als kochen denken.

Jedenfalls ist der Nachthimmel der gleiche wie im Gebirge. Und
manchmal, wenn ich nach oben gucke, in Nächten, in denen der
Boden nicht so schwankt, in denen die nassen Leute aus dem
Unterdeck sich nach oben legen und singen, ist das Heimweh ein bisschen
besser. Oder wenn Rash bei mir ist. Ich mag Rash.
